<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:red;">
			<div class="nav-wrapper">
				<font size="5"><b>Bodeguero: Recibir Insumos</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="bodeguero.php"><b>Volver</b></a></li>
					<li><a href="bodeguero.php"><b>Menu Principal</b></a></li>
					<li><a href="cerrar-sesion.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color: red;">
		<li><a href="bodeguero.php" style="color:white;">Volver</a></li>
		<li><a href="bodeguero.php" style="color:white;">Menu Principal</a></li>
		<li><a href="cerrar-sesion.php" style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="con" align="center" vertical-align="bottom">
		<div class="container">
			<div align="center" style="width:100%;">
				<br>
				<!--Tipo y tamaño de letra-->
				<h6 align="center"><font face="arial"><b style="background-color:white; color:black;"> - "Para recibir el insumo debe dar clic en revisado"</b></font></h6>
				<br>
				<!-- Formulario para recibir los insumos-->
				<form method="post" action="" style="background:red; margin:auto; width:100%; max-width:560px; padding: 30px; border: 2px solid white; border-radius: 10px;" align="center">
					<div>
						<code><b style="color:white;">Numero de Solicitud:</b></code>
						<input type="text" name="id_ordenCompra" style="width: 60%; color: white; text-align: center;">
						<code><b style="color:white;">Nombre Insumo a Solicitar:</b></code>
						<input type="text" name="nombreInsumo" style="width: 50%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Cantidad:</b></code>
						<input type="text" name="cantidad" style="width: 79%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Nombre Proveedor:</b></code>
						<input type="text" name="nombreProveedor" style="width: 65%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Fecha Pedido:</b></code>
						<input type="date" name="fechaPedido" style="width: 72%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Fecha Llegada:</b></code>
						<input type="date" name="fechaPedido" style="width: 70%; color: white; text-align: center;">
						<br>
						<br>
						<button class="waves-effect waves-light btn-small" type="submit" style="background-color:white; color:black;" href=""><b>Revisado</b></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html>