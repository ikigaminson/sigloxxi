<?php


//Rescatando los datos
$id_insumo="1";
$nombre=$_POST['nombre'];
$stock=$_POST['stock'];
$id_proveedor_original=$_POST['nombre_proveedor'];
$id_proveedor=strval($id_proveedor_original);
$id_medida_original=$_POST['unidad_medida'];
$id_medida=strval($id_medida_original);

//Declaración de Variables de Mensajes
$mensajeIdInsumo="";
$mensajeNombre="";
$mensajeStock="";
$mensajeIdProveedor="";
$mensajeIdMedida="";

//Valida que los campos no esten vacios
if (empty($id_insumo)) {	
	$mensajeIdInsumo=" *Id Insumo*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre Insumo*";
}
if (empty($stock)) {
	$mensajeStock=" *Stock Insumo*";
}
if (empty($id_proveedor)|| $id_proveedor=="0") {
	$mensajeIdProveedor=" *Id Proveedor*";
}
if (empty($id_medida)|| $id_medida=="0") {
	$mensajeIdMedida=" *Id Medida*";
}

//Válidando campos vacios
if (empty($id_insumo) || empty($nombre)|| empty($stock)|| empty($id_proveedor)|| empty($id_medida)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdInsumo.$mensajeNombre.$mensajeStock.$mensajeIdProveedor.$mensajeIdMedida.'");</script>';
	echo '<script>window.location.href="bodeguero-agregar-insumo.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/insumo/insert';

	// Datos de consultas hechos en un array
	$data = array(
		'id_insumo'         => $id_insumo,
		'nombre'         => $nombre,
		'stock'         => $stock,
		'id_proveedor'         => $id_proveedor,
		'id_medida'         => $id_medida,
	);

	//var_dump($data);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si el insumo se modifico
	if ($validado==1) {
		//Muestra mensaje al insumo que se agrego exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Insumo agregado exitosamente");</script>';
		echo '<script>window.location.href="bodeguero-gestion-insumo.php";</script>';
		//Redirecciona al modificar insumo
		//header("Location:admin-gestion-insumo.php");
	}else{
		//Muestra mensaje al usuario que no se agrego el Insumo*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo agregar el insumo !");</script>';
		echo '<script>window.location.href="bodeguero-gestion-insumo.php";</script>';
	}		
}

?>