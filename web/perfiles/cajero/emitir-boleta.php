<?php
//Rescatando los datos
$id_comensal=$_POST['idComensal'];
$id_boleta='1';
$numero_mesa=$_POST['numeroMesa'];
$fecha_boleta_orginal=$_POST['fechaBoleta'];
$fecha_boleta_modificada= date('d-m-Y',strtotime($fecha_inicio_original));
$fecha_boleta_replace=strval($fecha_inicio_modificada);
$fecha_boleta=str_replace("-", "/", $fecha_inicio_replace);
$total_consumo=$_POST['totalConsumo'];
$propina=$_POST['propina'];
$total=$_POST['total'];
$metodo_pago=$_POST['metodoPago'];

$efectivo='Efectivo';
$pago_rut='PagoRUT';
$cheque='Cheque';
$credito='Crédito';
$debito='Débito';

if ($metodo_pago==$efectivo) {
	$id_tipo_pago='4';
}
if ($metodo_pago==$pago_rut) {
	$id_tipo_pago='5';
}
if ($metodo_pago==$cheque) {
	$id_tipo_pago='6';
}
if ($metodo_pago==$credito) {
	$id_tipo_pago='7';
}
if ($metodo_pago==$debito) {
	$id_tipo_pago='8';
}

//Url al cual le hacemos una consulta
$url = 'localhost:4567/boleta/insert';

//Datos de consultas hechos en un array
$data = array(
	'id_boleta'         => $id_boleta,
	'fecha'         => $fecha_boleta,
	'propina'         => $propina,
	'totalconsumo'         => $total_consumo,
	'total'         => $total,
	'id_comensal'         => $id_comensal,
	'id_tipo_pago'         => $id_tipo_pago
);

//var_dump($data);

//Transformacion del array a un archivo json 
$fields_string = json_encode($data);

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Parsear la data a array
$parse_result = json_decode($result, true);

//var_dump($parse_result);

$validado=$parse_result["result"];

if ($validado==1) {

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/comensal/finalizar';

	//Datos de consultas hechos en un array
	$data = array(
		'id_comensal'         => $id_comensal
	);

	//var_dump($data);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result["result"];

	if ($validado==1) {
		//Muestra mensaje al usuario que se agrego exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Boleta agregada exitosamente");</script>';
		echo '<script>window.location.href="cajero-emitir-boleta.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se agrego el Usuario*****
		echo '<script language="javascript" style="color: red;">alert("¡No se pudo finalizar la estancia!");</script>';
		echo '<script>window.location.href="cajero-emitir-boleta.php";</script>';
	}
}else{
	//Muestra mensaje al usuario que no se agrego el Usuario*****
	echo '<script language="javascript" style="color: red;">alert("¡Error al agregar la boleta!");</script>';
	echo '<script>window.location.href="cajero-emitir-boleta.php";</script>';
}
?>