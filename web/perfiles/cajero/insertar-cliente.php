<?php
//Funcion Validar Rut
function ValidaDVRut($rut) {

	$tur = strrev($rut);
	$mult = 2;
	$suma = 0;

	for ($i = 0; $i <= strlen($tur); $i++) { 
		if ($mult > 7) $mult = 2; 

		$suma = $mult * (int)substr($tur, $i, 1) + $suma;
		$mult = $mult + 1;
	}

	$valor = 11 - ($suma % 11);

	if ($valor == 11) { 
		$codigo_veri = "0";
	} elseif ($valor == 10) {
		$codigo_veri = "k";
	} else { 
		$codigo_veri = $valor;
	}
	return $codigo_veri;
}

//Rescatar los datos de los campos
$checkRut=$_POST['rut'];
$nombre=$_POST['nombre']; 
$apelPat=$_POST['apelPat']; 
$apelMat=$_POST['apelMat']; 
$email=$_POST['email']; 
$cargo='7';

//Declaración de Variables de Mensajes
$mensajeRut="";
$mensajeNombre="";
$mensajeApelPat="";
$mensajeApelMat="";
$mensajeEmail="";
$mensajeCargo="";

//Valida que los campos no esten vacios
if (empty($checkRut)) {	
	$mensajeRut=" *Rut*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre*";
}
if (empty($apelPat)) {
	$mensajeApelPat=" *Apellido Paterno*";
}
if (empty($apelMat)) {
	$mensajeApelMat=" *Apellido Materno*";
}
if (empty($email)) {
	$mensajeEmail=" *Email*";
}
if (empty($cargo)) {
	$mensajeCargo=" *Cargo*";
}
if (empty($checkRut) || empty($nombre)|| empty($apelPat)|| empty($apelMat)|| empty($email)|| empty($cargo)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeRut.$mensajeNombre.$mensajeApelPat.$mensajeApelMat.$mensajeEmail.$mensajeCargo.'");</script>';
	echo '<script>window.location.href="cajero-registrar-cliente.php";</script>';
}else{
	
	//Toma el valor del Rut y lo divide en dos
	$rutSinPunto=str_replace(".","",$checkRut);
	$rutSinGuion=str_replace("-","",$rutSinPunto);
	$dv=substr($checkRut, -1);
	$rut=substr($rutSinGuion, 0, -1);

	//Valida que la variable del digito verificador sea ingresada correctamente
	if ($dv<0 && $dv>9 && $dv!='K') {
		//Si el $dv esta incorrecto manda error*****
		echo '<script language="javascript" style="color: red;">alert("Digito verificador incorrecto");</script>';
		echo '<script>window.location.href="cajero-registrar-cliente.php";</script>';
	}else{
		//Si el $dv esta correcto ingresa*****
		//Valida que el rut exista
		if (ValidaDVRut($rut)!=$dv){
			echo '<script language="javascript" style="color: red;">alert("Rut inválido");</script>';
			echo '<script>window.location.href="cajero-registrar-cliente.php";</script>';
		}else{
			//Si el Rut existe ingresa*****
			//Url al cual le hacemos una consulta
			$url = 'localhost:4567/usuario/insert';

			// Datos de consultas hechos en un array
			$data = array(
				'rut'         => $rut,
				'dv'         => $dv,
				'nombre'         => $nombre,
				'paterno'         => $apelPat,
				'materno'         => $apelMat,
				'mail'         => $email,
				'idTipo'         => $cargo
			);

			//var_dump($data);

			//Transformacion del array a un archivo json 
			$fields_string = json_encode($data);

			// Crear un nuevo recurso "cURL" 
			$ch = curl_init($url);

			//Establecer número de variables POST, datos POST
			curl_setopt($ch,CURLOPT_POST, true);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

			//Establecer el tipo de contenido en application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

			//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

			//Ejecuta el posteo
			$result = curl_exec($ch);

			//Transforma el resultado json en array
			$array = json_decode($result, true);

			//Accede al dato del array
			$last_id = $array['last_id'];

			//var_dump($last_id);

			//Valida si la persona se agrego o no el cliente
			if ($last_id == -1) {
				//Muestra mensaje que no se agrego el cliente*****
				echo '<script language="javascript" style="color: red;">alert("¡Error! Cliente No Ingresado");</script>';
				echo '<script>window.location.href="cajero-registrar-cliente.php";</script>';
			}elseif(is_numeric($last_id)){
				//Muestra mensaje al usuario se agrego el cliente*****
				echo '<script language="javascript" style="color: red;">alert("Cliente Ingresado");</script>';
				echo '<script>window.location.href="cajero-registrar-cliente.php";</script>';
			}else{
				//Muestra mensaje al usuario que no se agrego el cliente*****
				echo '<script language="javascript" style="color: red;">alert("¡Error! Retorno no válido");</script>';
				echo '<script>window.location.href="cajero-registrar-cliente.php";</script>';
			}	
		}
	}
}
?>