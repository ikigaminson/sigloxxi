<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:#607d8b;">
			<div class="nav-wrapper">
				<font size="5"><b>Cajero: Calcular Ganancias</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="cajero.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color:#607d8b;">
		<li><a href="cajero.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-session.php" style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<?php
	function ListarGanancias(){
		include('calcular-ganancia.php');
	}
	?>
	<!--Inicio Contenedor1-->
	<div class="contene" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<!--Tipo y tamaño de letra-->
			<h5 align="center"><font face="arial"><b style="background-color:white; color:black;">"En esta sección se podrán visualizar las ganancias"</b></font></h5>
			<div align="left" style="width:100%;">
				<br>
				<br>
				<form method="post" action="" style="background:#607d8b; margin:auto; width:100%; max-width:560px; padding: 30px; border: 4px solid rgba(0,0,0,0.6); border-radius: 10px;" align="center">
					<div align="center">
						<code style="color:white;" align="center"><h5><u>Ingrese Fechas</u></h5></code>
						<code><b style="color:white;">Fecha Inicio:</b></code>
						<input type="date" name="fechaInicio" style="width: 70%; color: white; text-align: center;">
						<code><b style="color:white;">Fecha Fin:</b></code>
						<input type="date" name="fechaFin" style="width: 70%; color: white; text-align: center;">
					</br>
					<button name="boton_calcular" class="waves-effect waves-light btn-small" type="submit" style="background-color:white; color:black;"  onclick="ListarGanancias();"><b>Calcular</b></button>
				</div>
			</form>
			<!--Fieldset: Recuadro que contiene el listado ganancias-->
			<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
				<!--Legend: Titulo de fieldset-->
				<legend><h6><font face="arial"><b style="background-color: white;color: black;">Listado de Ganancias</b></font></h6>
				</legend>
				<?php ListarGanancias();?>
			</fieldset>
		</div>
	</div>
</div>
<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html>