<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:#607d8b;">
			<div class="nav-wrapper">
				<a class="brand-logo"><i>Cajero:</i> Emitir Boleta</a>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="cajero.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo">
		<li><a href="cajero.php">Menu Principal</a></li>
		<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
		<li><i class="large material-icons">people_outline</i></li>
		<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
		<li><p>&nbsp;&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	
	<!--Inicio Contenedor1-->
	<div class="contene" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<br>
			<div align="left" style="width:100%;">
				<!--Tipo y tamaño de letra-->
				<h5 align="center"><font face="arial"><b style="background-color:white; color:black;">"En esta sección se podrán visualizar las boletas agregadas"</b></font></h5>
				<!--Fieldset: Recuadro que contiene el listado de boletas-->
				<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
					<!--Legend: Titulo de fieldset-->
					<legend><h6><font face="arial"><b style="background-color: white;color: black;">Lista de Boletas</b></font></h6></legend>
					<?php listar();?>
				</fieldset>
				<br>
				<!--Formulario para emitir boletas-->
				<form method="post" action="emitir-boleta.php" style="background:#607d8b; margin:auto; width:100%; max-width:560px; padding: 30px; border: 4px solid rgba(0,0,0,0.6); border-radius: 10px;" align="center">
					<code style="color:white;"><h5><u>Crear Boleta</u></h5></code>
					<div>
						<input type="hidden" name="idComensal" value=<?php echo $id_comensal; ?>>
						<code><b style="color:white;">Numero Mesa:</b></code>
						<input type="text" name="numeroMesa" style="width: 72%; color: white; text-align: center;">
						<code><b style="color:white;">Fecha Boleta:</b></code>
						<input type="date" name="fechaBoleta" style="width: 70%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Total Consumo:</b></code>
						<input type="text" name="totalConsumo" style="width: 68%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Propina:</b></code>
						<input type="text" name="propina" style="width: 78%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Total:</b></code>
						<input type="text" name="total" style="width: 81%; color: white; text-align: center;">
						<code style="text-align: left;"><b style="color:white;">Metodo Pago:</b></code>
						<input type="text" name="metodoPago" style="width: 72%; color: white; text-align: center;">
						<br>
						<br>
						<button class="waves-effect waves-light btn-small" type="submit" style="background-color:white; color:black;"><b>Emitir Boleta</b></button>
					</div>
				</form>			
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>
<?php
function listar(){
	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/comensales/activos';

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Transforma el resultado json en array
	$datos = json_decode($result, true);

	//var_dump($datos);

	//Creando las tablas de encabezado
	echo "<table border=1 style='width:100%;'>
	<tr><th>Numero Mesa</th>
	<th>Id Comensal</th>
	<th>Total</th>";
	for ($i=0; $i < count($datos); $i++) {
		$total = $datos[$i]['total'];
		$numero_mesa = $datos[$i]['numero_mesa'];
		$id_comensal = $datos[$i]['id_comensal'];
		$id_mesa = $datos[$i]['id_mesa'];

		if (!empty($total) && $total != null && !empty($numero_mesa) && $numero_mesa != null && !empty($id_comensal) && $id_comensal != null && !empty($id_mesa) && $id_mesa != null) {
			echo "<tr>";
			echo "<td><div align='center'>".$numero_mesa."</div></td>";
			echo "<td><div align='center'>".$id_comensal."</div></td>";
			echo "<td><div align='center'>".$total."</div></td></tr>";
		}
	}
	echo "</table>";
}
?>



