<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<?php 

//Valida el ingreso de fechas
if (!empty($_POST['fechaInicio']) && !empty($_POST['fechaFin'])) {
	$fecha_inicio_original=$_POST['fechaInicio'];
	$fecha_fin_original=$_POST['fechaFin'];
	$fecha_inicio_modificada= date('d-m-Y',strtotime($fecha_inicio_original));
	$fecha_fin_modificada= date('d-m-Y',strtotime($fecha_fin_original));
	$fecha_inicio_replace=strval($fecha_inicio_modificada);
	$fecha_fin_replace=strval($fecha_fin_modificada);
	$fecha_inicio=str_replace("-", "/", $fecha_inicio_replace);
	$fecha_fin=str_replace("-", "/", $fecha_fin_replace);
}

$efectivo='Efectivo';
$pago_rut='Pago Rut';
$cheque='Cheque';
$credito='Credito';
$debito='Debito';

$id_efectivo='4';
$id_pago_rut='5';
$id_cheque='6';
$id_credito='7';
$id_debito='8';

$metodo_pago='No Definido';
$ganancia="";

if (!empty($fecha_inicio) && !empty($fecha_fin)) {

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/calcularGanancia/generar';

	// Datos de consultas hechos en un array
	$data = array(
		'fecha_inicio'         => $fecha_inicio,
		'fecha_fin'         => $fecha_fin
	);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$resultado = curl_exec($ch);

	//Transforma el resultado json en array
	$parse_result = json_decode($resultado, true);

	$ganancia = $parse_result['result'];

	//var_dump($parse_result);	
}


//Url al cual le hacemos una consulta de boletas
$url = 'localhost:4567/boletas/list';

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Transforma el resultado json en array
$datos = json_decode($result, true);

//var_dump($datos);

//Creando las tablas de encabezado
echo "<table border=1 style='width:100%;'>
<tr><th>Id Boleta</th>
<th>Fecha</th>
<th>Metodo de Pago</th>
<th>Consumo</th>
<th>Propina</th>
<th>Total</th></tr>";
for ($i=0; $i < count($datos); $i++) { 
	//Rescatando los datos del array
	$id_boleta = $datos[$i]['id_boleta'];
	$fecha = $datos[$i]['fecha'];
	$id_tipo_pago = $datos[$i]['tipo_pago']['id_tipo_pago'];
	if ($id_tipo_pago==$id_efectivo) {
		$metodo_pago=$efectivo;
	}
	if ($id_tipo_pago==$id_pago_rut) {
		$metodo_pago=$pago_rut;
	}
	if ($id_tipo_pago==$id_cheque) {
		$metodo_pago=$cheque;
	}
	if ($id_tipo_pago==$id_credito) {
		$metodo_pago=$credito;
	}
	if ($id_tipo_pago==$id_debito) {
		$metodo_pago=$debito;
	}
	$propina = $datos[$i]['propina'];
	$consumo = $datos[$i]['totalconsumo'];
	$total = $datos[$i]['total'];
	echo "<tr>";
	echo "<td><div align='center'>".$id_boleta."</div></td>";
	echo "<td><div align='center'>".$fecha."</div></td>";
	echo "<td><div align='center'>".$metodo_pago."</div></td>";
	echo "<td><div align='center'>".$consumo."</div></td>";
	echo "<td><div align='center'>".$propina."</div></td>";
	echo "<td><div align='center'>".$total."</div></td>";
	echo "</tr>";
}
if (!empty($fecha_inicio) && !empty($fecha_fin)) {
	if (!empty($_POST['fechaInicio']) && !empty($_POST['fechaFin'])) {
		echo "<tr><th colspan='6'>".$ganancia."</th></tr>";
	}
}
echo "</table>";

?>