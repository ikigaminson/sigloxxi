<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}

//Listar activos
function listarActivos(){
	//Url al cual le hacemos una consulta de boletas
	$url = 'localhost:4567/comensales/activos';

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Transforma el resultado json en array
	$datos = json_decode($result, true);

	//Creando las tablas de encabezado
	echo "<table border=1 style='width:100%;'><tr>
	<th>Id Personas</th>
	<th>Id Mesa</th>
	<th>Mesa</th>
	<th>Total</th>
	<th>Detalle</th>
	<th>Validar</th>
	<th>Cancelar</th>";
	for ($i=0; $i < count($datos); $i++) { 
		//Rescatando los datos del array
		$total_activos = $datos[$i]['total'];
		$numero_mesa_activos = $datos[$i]['numero_mesa'];
		$id_comensal_activos = $datos[$i]['id_comensal'];
		$id_mesa_activos = $datos[$i]['id_mesa'];

		echo "<tr>";
		echo "<td><div align='center'>".$id_comensal_activos."</div></td>";
		echo "<td><div align='center'>".$id_mesa_activos."</div></td>";
		echo "<td><div align='center'>".$numero_mesa_activos."</div></td>";
		echo "<td><div align='center'>".$total_activos."</div></td>";
		echo "<td>";
		?>
		<div align="center"><button name="boton_ver" class="large material-icons" type="submit" style="width:30px;" onclick="verDetalle();">remove_red_eye</button></div>
		<?php
		echo "</td>";
		echo "<td>";
		?>
		<div align="center"><button name="boton_validar" class="large material-icons" type="submit" style="width:30px;" onclick="validarPedido();">check</button></div>
		<?php
		echo "</td>";
		echo "<td>";
		?>
		<div align="center"><button name="boton_cancelar" class="large material-icons" type="submit" style="width:30px;" onclick="cancelarPedido();">clear</button></div>
		<?php
		echo "</td>";
	}
	echo "</tr>";
	echo "</table>";
}

//Ver detalle
function verDetalle(){
	//Creando las tablas de encabezado
	echo "<table border=1 style='width:100%;'><tr>
	<th>Personas</th>
	<th>Mesa</th>
	<th>Receta</th>
	<th>Cantidad</th>
	<th>Precio</th>
	<th>Validado</th></tr>";
	for ($i=0; $i < count($datos); $i++) { 
		$id_comensal_activos = $datos[$i]['id_comensal'];

		//Url al cual le hacemos una consulta
		$url = 'localhost:4567/comensales/detallesOrden';

		// Datos de consultas hechos en un array
		$data = array(
			'id_comensal'         => $id_comensal_activos
		);

		//Transformacion del array a un archivo json 
		$fields_string = json_encode($data);

		// Crear un nuevo recurso "cURL" 
		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

		//Establecer el tipo de contenido en application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
		$resultado = curl_exec($ch);

		//Transforma el resultado json en array
		$parse_result = json_decode($resultado, true);

		//var_dump($parse_result);

		for ($i=0; $i < count($parse_result); $i++) { 
			if (!empty($parse_result)) {
				$precio = $parse_result[$i]['precio'];
				$total = $parse_result[$i]['total'];
				$numero_mesa = $parse_result[$i]['numero_mesa'];
				$id_comensal = $parse_result[$i]['ID_COMENSAL'];
				$id_mesa = $parse_result[$i]['id_mesa'];
				$cantidad = $parse_result[$i]['cantidad'];
				$validado = $parse_result[$i]['validado'];
				$id_pedido = $parse_result[$i]['id_pedido'];
				$nombre_receta = $parse_result[$i]['nombre_receta'];

				if ($id_comensal_activos==$id_comensal) {
					if ($validado=="0") {
						echo "<tr>";
						echo "<td><div align='center'>".$id_comensal."</div></td>";
						echo "<td><div align='center'>".$numero_mesa."</div></td>";
						echo "<td><div align='center'>".$nombre_receta."</div></td>";
						echo "<td><div align='center'>".$cantidad."</div></td>";
						echo "<td><div align='center'>".$precio."</div></td>";
						echo "<td><div align='center'>".$validado."</div></td>";
						echo "</tr>";
					}
				}
			}
		}
	}
	echo "</table>";
}


//validar pedido
function validarPedido(){
	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/pedido/validar';

		// Datos de consultas hechos en un array
	$data = array(
		'id_comensal'         => $id_comensal_activos
	);

		//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

		// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

		//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
	$resultado = curl_exec($ch);

		//Transforma el resultado json en array
	$parse_result = json_decode($resultado, true);

}

//Eliminar pedido
function cancelarPedido(){
	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/pedido/eliminar';

	// Datos de consultas hechos en un array
	$data = array(
		'id_comensal'         => $id_comensal_activos
	);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$resultado = curl_exec($ch);

	//Transforma el resultado json en array
	$parse_result = json_decode($resultado, true);

}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:#ffd600;">
			<div class="nav-wrapper">
				<font size="5"><b>Garzón: Validar Pedidos</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="garzon.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color:#ffd600;">
		<li><a href="garzon.php" style="color:white;"><b>Menu Principal</b></a></li>
		<li><a href="../cerrar-session.php" style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="conte" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<br>
			<br>
			<!--Tipo y tamaño de letra-->
			<h5><font face="arial"><b style="background-color:white; color:black;">"En esta sección se listaran los pedidos para su validación"</b></font></h5>
			<br>
			<div align="left" style="width:100%;">
				<br>
				<!--Fieldset: Recuadro que contiene el listado de pedidos para validar-->
				<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
					<!--Legend: Titulo de fieldset-->
					<legend><h6><font face="arial"><b style="background-color: white;color: black;">Validar Pedido</b></font></h6></legend>
					<?php listarActivos();?>
				</fieldset>
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!--JavaScript de dropdown-->
	<!-- Inicializando los componentes de materialize -->
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function(){
	    var elems = document.querySelectorAll('.sidenav');
	    var instances = M.Sidenav.init(elems);
	  	});
		// Or with jQuery
		$( document ).ready(function() {
			$(".dropdown-trigger").dropdown();
		});
	</script>
</body>
</html>