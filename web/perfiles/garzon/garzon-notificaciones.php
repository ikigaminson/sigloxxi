<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:#ffd600;">
			<div class="nav-wrapper">
				<font size="5"><b>Garzón: Notificaciones</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="garzon.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color:#ffd600;">
		<li><a href="garzon.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-session.php" style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="conte" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<br>
			<!--Tipo y tamaño de letra-->
			<h5><font face="arial"><b style="background-color:white; color:black;">"En esta sección se visualizaran las notificaciones"</b></font></h5>
			<br>
			<div align="left" style="width:100%;">
				<br>
				<!--Fieldset: Recuadro que contiene el listado de notificación de garzon de llamada-->
				<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
					<!--Legend: Titulo de fieldset-->
					<legend><h6><font face="arial"><b style="background-color: white;color: black;">Llamada a Garzon</b></font></h6></legend>
					<table border="1" style="width:100%;">
						<tr>
							<th>Numero Mesa</th>
							<th>Ir a Mesa</th>
						</tr>
						<tr>
							<td></td>
							<td><div align="center"><button class="large material-icons" type="button" style="width:50%;" onclick="location.href=''">offline_pin</button></div></td>
						</tr>
					</table>	
				</fieldset>
				<br>
				<br>
				<!--Fieldset: Recuadro que contiene el listado de notificación de garzon de pago cuenta-->
				<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
					<!--Legend: Titulo de fieldset-->
					<legend><h6><font face="arial"><b style="background-color: white;color: black;">Llamada a Garzon por Pago Cuentas</b></font></h6></legend>
					<table border="1" style="width:100%;">
						<tr>
							<th>Numero Mesa</th>
							<th>Ir a Mesa</th>
						</tr>
						<tr>
							<td></td>
							<td><div align="center"><button class="large material-icons" type="button" style="width:50%;" onclick="location.href=''">offline_pin</button></div></td>
						</tr>
					</table>	
				</fieldset>
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!--JavaScript de dropdown-->
	<!-- Inicializando los componentes de materialize -->
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function(){
	    var elems = document.querySelectorAll('.sidenav');
	    var instances = M.Sidenav.init(elems);
	  	});
		// Or with jQuery
		$( document ).ready(function() {
			$(".dropdown-trigger").dropdown();
		});
	</script>
</body>
</html>