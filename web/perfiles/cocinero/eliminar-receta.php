<?php

//Rescatando los datos
$verIdReceta=$_POST['verIdReceta'];
$verNombre=$_POST['verNombre'];
$verDescripcion=$_POST['verDescripcion'];
$verPrecio=$_POST['verPrecio'];
$verDisponibilidad=$_POST['verDisponibilidad'];
$verIdOrigen=$_POST['verIdOrigen'];
$verDescripcionOrigen=$_POST['verDescripcionOrigen'];
$verEliminado=$_POST['verEliminado'];

//Url al cual le pedimos el delete
$url = 'localhost:4567/receta/delete';

// Datos enviados en un array
$data = array(
	'id_receta'         => $verIdReceta,
);

//Transformacion del array a un archivo json 
$fields_string = json_encode($data);

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Le introducimos la data a la consulta
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Parsear la data a array
$parse_result = json_decode($result, true);

//var_dump($parse_result["result"]);

//Imprimiendo el array
$datos=$parse_result["result"];

//Verifica si se ejecuto bien el cambio
if ($datos==1) {
	//Muestra operacion exitosa
	echo '<script language="javascript" style="color: red;">alert("Receta Eliminada");</script>';
	echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
}elseif($datos == -1){
	//Manda error en caso que no se pudiese eliminar el receta
	echo '<script language="javascript" style="color: red;">alert("No existe este receta");</script>';
	echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
}else{
	//Manda error en caso de que suceda algo en la base de datos
	echo '<script language="javascript" style="color: red;">alert("Error en la Base de Datos al borrar receta");</script>';
	echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
}
?>