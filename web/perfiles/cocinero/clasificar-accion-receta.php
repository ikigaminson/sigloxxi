<?php 
$modificarPagina="<script language='javascript'>window.location.replace('cocinero-modificar-coctel.php');</script>";
$eliminarPagina="<script language='javascript'>window.location.replace('eliminar-receta.php');</script>";

//Rescatando los datos
$verIdReceta=$_POST['verIdReceta'];
$verNombre=$_POST['verNombre'];
$verDescripcion=$_POST['verDescripcion'];
$verPrecio=$_POST['verPrecio'];
$verDisponibilidad=$_POST['verDisponibilidad'];
$verUrlReceta=$_POST['verUrlReceta'];
$verIdOrigen=$_POST['verIdOrigen'];
$verDescripcionOrigen=$_POST['verDescripcionOrigen'];
$verEliminado=$_POST['verEliminado'];

if (isset($_POST['boton_modificar'])) {
	?>
	<form name='enviar' method='post' action='cocinero-modificar-receta.php'>
		<input type="hidden" name="verIdReceta" value=<?php echo $verIdReceta; ?>>
		<input type="hidden" name="verNombre" value=<?php echo $verNombre; ?>>
		<input type="hidden" name="verDescripcion" value=<?php echo $verDescripcion; ?>>
		<input type="hidden" name="verPrecio" value=<?php echo $verPrecio; ?>>
		<input type="hidden" name="verDisponibilidad" value=<?php echo $verDisponibilidad; ?>>
		<input type="hidden" name="verUrlReceta" value=<?php echo $verUrlReceta; ?>>
		<input type="hidden" name="verIdOrigen" value=<?php echo $verIdOrigen; ?>>
		<input type="hidden" name="verDescripcionOrigen" value=<?php echo $verDescripcionOrigen; ?>>
		<input type="hidden" name="verEliminado" value=<?php echo $verEliminado; ?>>
	</form>
	<script language='JavaScript'>
		document.enviar.submit();
	</script>
	<?php
	echo $modificarPagina;
}elseif(isset($_POST['boton_eliminar'])){
	?>
	<form name='enviar2' method='post' action='eliminar-receta.php'>
		<input type="hidden" name="verIdReceta" value=<?php echo $verIdReceta; ?>>
		<input type="hidden" name="verNombre" value=<?php echo $verNombre; ?>>
		<input type="hidden" name="verDescripcion" value=<?php echo $verDescripcion; ?>>
		<input type="hidden" name="verPrecio" value=<?php echo $verPrecio; ?>>
		<input type="hidden" name="verDisponibilidad" value=<?php echo $verDisponibilidad; ?>>
		<input type="hidden" name="verUrlReceta" value=<?php echo $verUrlReceta; ?>>
		<input type="hidden" name="verIdOrigen" value=<?php echo $verIdOrigen; ?>>
		<input type="hidden" name="verDescripcionOrigen" value=<?php echo $verDescripcionOrigen; ?>>
		<input type="hidden" name="verEliminado" value=<?php echo $verEliminado; ?>>
	</form>
	
	<script language='JavaScript'>
		document.enviar2.submit();
	</script>
	<?php
	echo $eliminarPagina;
}else{
	echo '<script language="javascript" style="color: red;">alert("¡ Error al traspasar datos !");</script>';
	echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
}
?>