<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<?php 
$existe = false;
//Valida que el Filtro haya sido activado
if (isset($_POST['busquedaReceta'])) {
	if (empty($_POST['busquedaReceta'])) {
		$filtro = false;
	}else{
		$receta_buscado = $_POST['busquedaReceta'];
		$busqueda_de_receta=strtolower($receta_buscado);
		$filtro = true;
	}
}else{
	$filtro = false;
}

$disponible='D';
$no_disponible='N';

//Url al cual le hacemos una consulta
$url = 'localhost:4567/recetas/list';

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Transforma el resultado json en array
$datos = json_decode($result, true);

//var_dump($datos);

//Creando las tablas de encabezado
echo "<table border=1 style='width:100%;'>
<tr><th>Nombre</th>
<th>Descripción</th>
<th>Precio</th>
<th>Disponibilidad</th>
<th>Imagen</th>
<th>Origen</th>
<th colspan='2'>Opciones</th></tr>";
for ($i=0; $i < count($datos); $i++) { 

	//Rescatando los datos del array
	$id_receta = $datos[$i]['id_receta'];
	$nombre_receta_original = $datos[$i]['nombre'];
	$nombre_receta=strtolower($nombre_receta_original);
	$nombre=str_replace(" ", "-", $nombre_receta_original);
	$descripcion_receta=$datos[$i]['descripcion'];
	$descripcion=str_replace(" ", "-", $descripcion_receta);
	$precio = $datos[$i]['precio'];
	$disponibilidad_receta = $datos[$i]['disponibilidad'];
	$disponibilidad=str_replace(" ", "-", $disponibilidad_receta);
	if ($disponibilidad_receta==$disponible) {
		$disponiblidad_visual='Disponible';
	}elseif($disponibilidad_receta==$no_disponible){
		$disponiblidad_visual='No Disponible';
	}else{
		$disponiblidad_visual='No Disponible';
	}
	$url_receta=$datos[$i]['ImageUrl'];
	$id_origen = $datos[$i]['origen']['id_origen'];
	$descripcion_origen = $datos[$i]['origen']['descripcion'];
	$eliminado = $datos[$i]['eleminado'];
	//Verifica si hay filtro aplicado
	if ($filtro==true) {
		//Verifica si el nombre ingresado es igual al nombre rescatado
		if($nombre_receta==$busqueda_de_receta) {
			//Filtra los recetas que vienen desde la cocina
			if ($descripcion_origen=='Cocina') {
				$existe=true;
				?>
				<form method="post" action="clasificar-accion-receta.php" name= "formulario-clasificar-receta">
					<?php
					//Listado de todas las receta
					echo "<tr>";
					echo "<td><div align='center'>".$nombre_receta_original."</div></td>";
					echo "<td><div align='center'>".$descripcion_receta."</div></td>";
					echo "<td><div align='center'>".$precio."</div></td>";
					echo "<td><div align='center'>".$disponiblidad_visual."</div></td>";
					echo "<td><div align='center'><img src=".$url_receta." width='70px' height='70px'></div></td>";
					echo "<td><div align='center'>".$descripcion_origen."</div></td>";
					echo "<td>";
					?>
					<!--Inputs invisibles que se envían a clasificar accion mesa-->
					<input type="hidden" name="verIdReceta" value=<?php echo $id_receta; ?>>
					<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
					<input type="hidden" name="verDescripcion" value=<?php echo $descripcion; ?>>
					<input type="hidden" name="verPrecio" value=<?php echo $precio; ?>>
					<input type="hidden" name="verDisponibilidad" value=<?php echo $disponibilidad; ?>>
					<input type="hidden" name="verUrlReceta" value=<?php echo $url_receta; ?>>
					<input type="hidden" name="verIdOrigen" value=<?php echo $id_origen; ?>>
					<input type="hidden" name="verDescripcionOrigen" value=<?php echo $descripcion_origen; ?>>
					<input type="hidden" name="verEliminado" value=<?php echo $eliminado; ?>>
					<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
					<?php
					echo "</td>";
					echo "<td>";
					?>
					<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
					<?php
					echo "</td>";
					echo "</tr>";
					?>
				</form>
				<?php
			}
		}
	}elseif ($filtro==false){
		if ($descripcion_origen=='Cocina') {
			$existe=true;
			?>
			<form method="post" action="clasificar-accion-receta.php" name= "formulario-clasificar-receta">
				<?php
				//Listado de todas las receta
				echo "<tr>";
				echo "<td><div align='center'>".$nombre_receta_original."</div></td>";
				echo "<td><div align='center'>".$descripcion_receta."</div></td>";
				echo "<td><div align='center'>".$precio."</div></td>";
				echo "<td><div align='center'>".$disponiblidad_visual."</div></td>";
				echo "<td><div align='center'><img src=".$url_receta." width='70px' height='70px'></div></td>";
				echo "<td><div align='center'>".$descripcion_origen."</div></td>";
				echo "<td><div align='center'>";
				?>
				<!--Inputs invisibles que se envían a clasificar accion mesa-->
				<input type="hidden" name="verIdReceta" value=<?php echo $id_receta; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verDescripcion" value=<?php echo $descripcion; ?>>
				<input type="hidden" name="verPrecio" value=<?php echo $precio; ?>>
				<input type="hidden" name="verDisponibilidad" value=<?php echo $disponibilidad; ?>>
				<input type="hidden" name="verUrlReceta" value=<?php echo $url_receta; ?>>
				<input type="hidden" name="verIdOrigen" value=<?php echo $id_origen; ?>>
				<input type="hidden" name="verDescripcionOrigen" value=<?php echo $descripcion_origen; ?>>
				<input type="hidden" name="verEliminado" value=<?php echo $eliminado; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}else{
		echo '<script language="javascript" style="color: red;">alert("Error de Sistema");</script>';
		echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
	}
}
echo "</table>";
if ($existe == false) {
	echo '<script language="javascript" style="color: red;">alert("Coctel no Encontrado");</script>';
	echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
}
?>