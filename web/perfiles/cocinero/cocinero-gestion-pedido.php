<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
function listar(){
	//Url al cual le hacemos una consulta de boletas
	$url = 'localhost:4567/cocina/ordenes';

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Transforma el resultado json en array
	$datos = json_decode($result, true);

	//var_dump($datos);
	//Creando las tablas de encabezado
	echo "<table border=1 style='width:100%;'>
	<tr><th>Id Comensal</th>
	<th>Numero Mesa</th>
	<th>Descripcion</th>
	<th>Cantidad</th>
	<th>Receta</th>
	<th>Realizado</th></tr>";
	for ($i=0; $i < count($datos); $i++) { 
	//Rescatando los datos del array
		$numero_mesa = $datos[$i]['numero_mesa'];
		$id_origen = $datos[$i]['id_origen'];
		$id_comensal = $datos[$i]['id_comensal'];
		$nombre_descripcion = $datos[$i]['nombre_descripcion'];
		$id_mesa = $datos[$i]['id_mesa'];
		$cantidad = $datos[$i]['cantidad'];
		$id_pedido = $datos[$i]['id_pedido'];
		$nombre_receta = $datos[$i]['nombre_receta'];
		if ($nombre_descripcion=='Cocina') {
			echo "<tr>";
			echo "<td><div align='center'>".$id_comensal."</div></td>";
			echo "<td><div align='center'>".$numero_mesa."</div></td>";
			echo "<td><div align='center'>".$nombre_descripcion."</div></td>";
			echo "<td><div align='center'>".$cantidad."</div></td>";
			echo "<td><div align='center'>".$nombre_receta."</div></td>";
			echo "<td>";
			?>
			<div align="center"><button name="boton_validar" class="large material-icons" type="submit" style="width:30px;" onclick="enviarPedido();">check</button></div>
			<?php
			echo "</td>";
			echo "</tr>";
		}	
	}
	
	echo "</table>";
}
function enviarPedido()
{
	//Url al cual le hacemos una consulta de boletas
	$url = 'localhost:4567/cocina/terminarPedido';

	// Datos de consultas hechos en un array
	$data = array(
		'id_pedido'         => $id_pedido
	);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);


	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Transforma el resultado json en array
	$parse_result = json_decode($result, true);

	//var_dump($datos);

	$data = $parse_result['result'];

	if ($data==1) {
		echo '<script language="javascript" style="color: red;">alert("Pedido enviado");</script>';
		echo '<script>window.location.href="cocinero-gestion-pedido.php";</script>';
	}elseif($data==-1){
		echo '<script language="javascript" style="color: red;">alert("No se pudo enviar el pedido");</script>';
		echo '<script>window.location.href="cocinero-gestion-pedido.php";</script>';
	}else{
		echo '<script language="javascript" style="color: red;">alert("Error de Sistema");</script>';
		echo '<script>window.location.href="cocinero-gestion-pedido.php";</script>';
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:blue;">
			<div class="nav-wrapper">
				<font size="5"><b>Cocinero: Pedidos</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="cocinero.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color:blue;">
		<li><a href="cocinero.php" style="color:white;"><b>Volver</b></a></li>
		<li><a href="cocinero.php" style="color:white;"><b>Menu Principal</b></a></li>
		<li><a href="../cerrar-session.php"  style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="contenedores" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<br>
			<!--Tipo y tamaño de letra-->
			<h5><font face="arial"><b style="background-color:white; color:black;">"En esta sección se podrán visualizar los pedidos"</b></font></h5>
			<br>
			<div align="left" style="width: 100%">
				<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
					<legend><h6><font face="arial"><b style="background-color: white;color: black;">Lista de Registro Pedidos</b></font></h6></legend>
					<?php listar();?>
				</fieldset>
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!--JavaScript de dropdown-->
	<!-- Inicializando los componentes de materialize -->
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function(){
	    var elems = document.querySelectorAll('.sidenav');
	    var instances = M.Sidenav.init(elems);
	  	});
		// Or with jQuery
		$( document ).ready(function() {
			$(".dropdown-trigger").dropdown();
		});
	</script>
</body>
</html>