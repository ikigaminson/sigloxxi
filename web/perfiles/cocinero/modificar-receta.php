<?php

//Rescatando los datos
$id_receta=$_POST['id_receta'];
$nombre=$_POST['nombre'];
$descripcion=$_POST['descripcion'];
$precio=$_POST['precio'];
$disponibilidad=$_POST['disponibilidad'];
$url_receta=$_POST['url_receta'];
$id_origen=$_POST['id_origen'];
$descripcion_origen=$_POST['descripcion'];
$eliminado=$_POST['eliminado'];
//Variables para comprobar
$comprobar_nombre=$_POST['comprobar_nombre'];
$comprobar_descripcion=$_POST['comprobar_descripcion'];
$comprobar_descripcion_origen=$_POST['comprobar_descripcion_origen'];

//
if ($nombre == $comprobar_nombre) {
	$nombre_final = str_replace("-", " ", $nombre);
}else{
	$nombre_final = $nombre;
}
if ($descripcion == $comprobar_descripcion) {
	$descripcion_final = str_replace("-", " ", $descripcion);
}else{
	$descripcion_final = $descripcion;
}
if ($descripcion_origen == $comprobar_descripcion_origen) {
	$descripcion_origen_final = str_replace("-", " ", $descripcion_origen);
}else{
	$descripcion_origen_final = $descripcion_origen;
}

//Declaración de Variables de Mensajes
$mensajeIdReceta="";
$mensajeNombre="";
$mensajeDescripcion="";
$mensajePrecio="";
$mensajeDisponibilidad="";
$mensajeUrl="";
$mensajeIdOrigen="";
$mensajeDescripcionOrigen="";

//Valida que los campos no esten vacios
if (empty($id_receta)) {	
	$mensajeIdReceta=" *Id Receta*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre*";
}
if (empty($descripcion)) {
	$mensajeDescripcion=" *Descripcion*";
}
if (empty($precio)) {
	$mensajePrecio=" *Precio*";
}
if (empty($disponibilidad)) {
	$mensajeDisponibilidad=" *Disponibilidad*";
}
if (empty($url_receta)) {
	$mensajeUrl=" *Url*";
}
if (empty($id_origen)) {
	$mensajeIdOrigen=" *Id Origen*";
}
if (empty($descripcion_origen)) {
	$mensajeDescripcionOrigen=" *Descripcion Origen*";
}

//Válidando campos vacios
if (empty($id_receta) || empty($nombre)|| empty($descripcion)|| empty($precio)|| empty($disponibilidad) || empty($url_receta)|| empty($id_origen) || empty($descripcion_origen)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdReceta.$mensajeNombre.$mensajeDescripcion.$mensajePrecio.$mensajeDisponibilidad.$mensajeUrl.$mensajeIdOrigen.$mensajeDescripcionOrigen.'");</script>';
	echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/receta/update';

	// Datos de consultas hechos en un array
	$data = array(
		'id_receta'         => $id_receta,
		'nombre'         => $nombre_final,
		'descripcion'         => $descripcion_final,
		'precio'         => $precio,
		'disponibilidad'         => $disponibilidad,
		'url'         => $url_receta,
		'id_origen'         => $id_origen,
		'eliminado'         => $eliminado
	);

	//var_dump($data);
	
	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si la receta se modifico
	if ($validado==1) {
		//Muestra mensaje al usuario que se modifico exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Receta modificada exitosamente");</script>';
		echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se modifico la receta*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo modificar el receta !");</script>';
		echo '<script>window.location.href="cocinero-gestion-receta.php";</script>';
	}
}
?>