<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>
<?php
function buscarImagen($capacidad){
	if ($capacidad==2) {
		$urlImagen="../../img/mesa2.png";
	}
	if ($capacidad==4) {
		$urlImagen="../../img/mesa4.png";
	}
	if ($capacidad==6) {
		$urlImagen="../../img/mesa6.png";
	}
	return $urlImagen;
}
//Url al cual le hacemos una consulta
$url = 'localhost:4567/mesas/list';

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Transforma el resultado json en array
$datos = json_decode($result, true);

//var_dump($datos);

echo"<table border=1 style='width:100%;'>
<tr><th>Numero</th>
<th>Capacidad</th>
<th>Disponibilidad</th>
<th>Imagen Mesa</th>
<th colspan='2'>Opciones</th></tr>";
for ($i=0; $i < count($datos); $i++) { 
	$id_mesa=$datos[$i]['id_mesa'];
	$numero=$datos[$i]['numero'];
	$capacidad=$datos[$i]['capacidad'];
	$disponibilidad_original=$datos[$i]['disponibilidad'];
	$disponibilidad=str_replace(" ", "-", $disponibilidad_original);
	$ruta=buscarImagen($capacidad);
	?>
	<form method="post" action="clasificar-accion-mesa.php" name= "formulario-clasificar-mesa">
		<?php
		echo "<tr>";
		echo "<td><div align='center'>".$numero."</div></td>";
		echo "<td><div align='center'>".$capacidad."</div></td>";
		echo "<td><div align='center'>".$disponibilidad_original."</div></td>";
		echo "<td>";
		?>
		<!--Inputs invisibles que se envían a clasificar accion mesa-->
		<input type="hidden" name="verIdMesa" value=<?php echo $id_mesa; ?>>
		<input type="hidden" name="verNumero" value=<?php echo $numero; ?>>
		<input type="hidden" name="verCapacidad" value=<?php echo $capacidad; ?>>
		<input type="hidden" name="verDisponibilidad" value=<?php echo $disponibilidad; ?>>
		<input type="hidden" name="verRuta" value=<?php echo $ruta; ?>>

		<div align="center"><img width="70px" src=<?php echo $ruta; ?>></div>
		<?php
		echo "</td>";
		echo "<td>";
		?>
		<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
		<?php
		echo "</td>";
		echo "<td>";
		?>
		<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
		<?php
		echo "</td>";
		echo "</tr>";
		?>
	</form>
	<?php
}
echo "</table>";

?>