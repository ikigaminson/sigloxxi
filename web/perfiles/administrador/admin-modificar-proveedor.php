<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
if (isset($_POST['verIdProveedor'])) {
	//Rescatando los datos enviados desde la lista
	$verIdProveedor=$_POST['verIdProveedor'];
	$verNombre=$_POST['verNombre'];
	$verNombreOriginal=str_replace("-", " ", $verNombre);
	$verFono=$_POST['verFono'];
	$verEmail=$_POST['verEmail'];
	$verDireccion=$_POST['verDireccion'];
	$verDireccionOriginal=str_replace("-", " ", $verDireccion);	
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import estiloadmin.css-->
	<link rel="stylesheet" href="../../css/estiloadmin.css">
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:black;">
			<div class="nav-wrapper">
				<font size="5"><b>Administrador: Modificar Proveedor</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="admin-gestion-proveedor.php"><b>Volver</b></a></li>
					<li><a href="admin.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-sesion.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color: black;">
		<li><a href="admin-gestion-proveedor.php" style="color:white;">Volver</a></li>
		<li><a href="admin.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-sesion.php" style="color:white;">Cerrar Sessión</a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="contenedor" align="center" vertical-align="bottom">
		<div class="container">
			<div align="center" style="width:100%;">
				<br>
				<!--Tipo y tamaño de letra-->
				<h6 align="center"><font face="arial"><b> - "Podrá modificar el dato que necesite del proveedor"</b></font></h6>
				<h6 align="center"><font face="arial"><b> - "El uso de guion (-) en la dirección del proveedor no influye en nada al momento de almacenar la información"</b></font></h6>
				<br>
				<!-- Formulario de modificar proveedor-->
				<form method="post" action="modificar-proveedor.php" style="background-color: black; border-color: black; border-radius: 20px; border: 2px solid white; width:100%;" align="center">
					<div>
						<input type="hidden" name="id_proveedor" value=<?php echo $verIdProveedor; ?>>
						<input type="hidden" name="comprobar_nombre" value=<?php echo $verNombre; ?>>
						<input type="text" name="nombre" placeholder="Nombre Proveedor" style= "color: #ffffff;" value=<?php if (isset($_POST['verNombre'])) { echo $verNombre;}else{ echo '';} ?>>
						<input type="text" name="fono" placeholder="Fono" style= "color: #ffffff;" value=<?php if (isset($_POST['verFono'])) { echo $verFono;}else{ echo '';} ?>>
						<input type="text" name="email" placeholder="Email" style= "color: #ffffff;" value=<?php if (isset($_POST['verEmail'])) { echo $verEmail;}else{ echo '';}?>>
						<input type="hidden" name="comprobar_direccion" value=<?php echo $verDireccion; ?>>
						<input type="text" name="direccion" placeholder="Direccion" style= "color: #ffffff;" value=<?php if (isset($_POST['verDireccion'])) { echo $verDireccion;}else{ echo '';}?>>

						<button class="waves-effect waves-light btn-small" type="submit" style="background-color:white; color:black;" href=""><b>Modificar</b></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html>