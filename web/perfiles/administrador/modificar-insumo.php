<?php

//Rescatando los datos
$id_insumo=$_POST['id_insumo'];
$nombre=$_POST['nombre'];
$stock=$_POST['stock'];
$id_proveedor=$_POST['id_proveedor'];
$nombre_proveedor=$_POST['nombre_proveedor'];
$fono_proveedor=$_POST['fono_proveedor'];
$id_medida=$_POST['id_medida'];
$unidad_medida=$_POST['unidad_medida'];

//Declaración de Variables de Mensajes
$mensajeIdInsumo="";
$mensajeNombre="";
$mensajeStock="";
$mensajeIdProveedor="";
$mensajeNombreProveedor="";
$mensajeFonoProveedor="";
$mensajeIdMedida="";
$mensajeUnidadMedida="";

//Valida que los campos no esten vacios
if (empty($id_insumo)) {	
	$mensajeIdInsumo=" *Id Insumo*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre Insumo*";
}
if (empty($stock)) {
	$mensajeStock=" *Stock Insumo*";
}
if (empty($id_proveedor)) {
	$mensajeIdProveedor=" *Id Proveedor*";
}
if (empty($nombre_proveedor)) {
	$mensajeNombreProveedor=" *Nombre Proveedor*";
}
if (empty($fono_proveedor)) {
	$mensajeFonoProveedor=" *Fono Proveedor*";
}
if (empty($id_medida)) {
	$mensajeIdMedida=" *Id Medida*";
}
if (empty($unidad_medida)) {
	$mensajeUnidadMedida=" *Unidad Medida*";
}

//Válidando campos vacios
if (empty($id_insumo) || empty($nombre)|| empty($stock)|| empty($id_proveedor)|| empty($nombre_proveedor)|| empty($fono_proveedor)|| empty($id_medida)|| empty($unidad_medida)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdInsumo.$mensajeNombre.$mensajeStock.$mensajeIdProveedor.$mensajeNombreProveedor.$mensajeFonoProveedor.$mensajeIdMedida.$mensajeUnidadMedida.'");</script>';
	echo '<script>window.location.href="admin-gestion-insumo.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/insumo/update';

	$nombre_original = str_replace("-", " ", $nombre);

	// Datos de consultas hechos en un array
	$data = array(
		'id_insumo'         => $id_insumo,
		'nombre'         => $nombre_original,
		'stock'         => $stock,
		'id_proveedor'         => $id_proveedor,
		'id_medida'         => $id_medida
	);

	//var_dump($data);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si el insumo se modifico
	if ($validado==1) {
		//Muestra mensaje al usuario que se modifico exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Insumo modificado exitosamente");</script>';
		echo '<script>window.location.href="admin-gestion-insumo.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se modifico el Insumo*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo modificar el insumo !");</script>';
		echo '<script>window.location.href="admin-gestion-insumo.php";</script>';
	}		
}
?>