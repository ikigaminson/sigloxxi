<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<?php

//Url al cual le hacemos una consulta
$url = 'localhost:4567/cliente/list';

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Transforma el resultado json en array
$datos = json_decode($result, true);

//var_dump($datos);

echo"<table border=1 style='width:100%;'>
<tr><th>Nombre</th>
<th>Apellido Paterno</th>
<th>Apellido Materno</th>
<th>Rut</th>
<th>Dv</th>
<th>Correo</th>
<th colspan='2'>Opciones</th></tr>";
for ($i=0; $i < count($datos); $i++) { 
	//Rescatando los Datos
	$id_persona=$datos[$i]['id_persona'];
	$nombre = $datos[$i]['nombre'];
	$paterno = $datos[$i]['paterno'];
	$materno = $datos[$i]['materno'];
	$rut=$datos[$i]['rut'];
	$dv=$datos[$i]['dv'];
	$email = $datos[$i]['email'];
	$id_tipo_persona = $datos[$i]['tipoPersona']['id_tipo_persona'];
	$descripcion = $datos[$i]['tipoPersona']['descripcion'];
	?>
	<form method="post" action="clasificar-accion-cliente.php" name= "formulario-clasificar-cliente">
		<?php
		echo "<tr>";
		echo "<td><div align='center'>".$nombre."</div></td>";
		echo "<td><div align='center'>".$paterno."</div></td>";
		echo "<td><div align='center'>".$materno."</div></td>"; 
		echo "<td><div align='center'>".$rut."</div></td>";
		echo "<td><div align='center'>".$dv."</div></td>"; 
		echo "<td><div align='center'>".$email."</div></td>";
		echo "<td>";
		?>
		<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
		<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
		<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
		<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
		<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
		<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
		<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
		<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
		<input type="hidden" name="verDescripcion" value=<?php echo $descripcion; ?>>
		<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
		<?php
		echo "</td>";
		echo "<td>";
		?>
		<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
		<?php
		echo "</td>";
		echo "</tr>";
		?>
	</form>
	<?php
}
echo "</table>";

?>