<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import estiloadmin.css-->
	<link rel="stylesheet" href="../../css/estiloadmin.css">
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:black;">
			<div class="nav-wrapper">
				<font size="5"><b>Administrador: Agregar Usuario</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="admin-gestion-usuario.php"><b>Volver</b></a></li>
					<li><a href="admin.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>
	<ul class="sidenav" id="mobile-demo" style="background-color: black;">
		<li><a href="admin-gestion-usuario.php" style="color:white;">Volver</a></li>
		<li><a href="admin.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-session.php" style="color:white;">Cerrar Sessión</a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="contenedor" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<!--Tipo y tamaño de letra-->
			<h6 align="center"><font face="arial"><b> - "Para agregar un trabajador, tendrá que completar el siguiente formulario"</b></font></h6>
			<br>
			<!-- Formulario de registro usuario-->
			<form method="post" action="insertar-usuario.php" style="background-color: black; border-color: black; border-radius: 20px; border: 2px solid white;">
				<div>
					<input type="text" name="nombre" placeholder="Nombre" style= "color: #ffffff;">
					<input type="text" name="apelPat" placeholder="Apellido Paterno" style= "color: #ffffff;">
					<input type="text" name="apelMat" placeholder="Apellido Materno" style= "color: #ffffff;">
				    <input type="text" name="rut" placeholder="Rut" style= "color: #ffffff;"> 
					<input type="email" name="email" placeholder="Correo" style= "color: #ffffff;">
					<div>
						<select class="browser-default" name="seleccionCargo" placeholder="cargo" style= "background-color: black; color:white;">	
							<option value="0">Seleccione Cargo</option>
							<option value="1">Administrador</option>
							<option value="2">Bodeguero</option>
							<option value="3">Cajero</option>
							<option value="4">Cocinero</option>
							<option value="5">Bartender</option>
							<option value="6">Garzon</option>		
						</select>
					</div>
					<input type="text" placeholder="Usuario" name="usuario" style= "color: #ffffff;">
					<input type="password" placeholder="Contraseña" name="clave" style= "color: #ffffff;">
					<button class="waves-effect waves-light btn-small" type="submit" style="background-color: white; color:black;" href=""><b>Agregar</b></button>
				</div>
			</form>		
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html>