<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<?php

//Valida que el Filtro haya sido activado
if (isset($_POST['busquedaInsumo'])) {
	
	if (empty($_POST['busquedaInsumo'])) {
		$filtro = false;
	}else{
		$insumo_buscado = $_POST['busquedaInsumo'];
		$busquedaInsumo = strtolower($insumo_buscado);
		$filtro = true;
	}
}else{
	$filtro = false;
}
//var_dump($filtro);

//Url al cual le hacemos una consulta
$url = 'localhost:4567/insumos/list';

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Transforma el resultado json en array
$datos = json_decode($result, true);

//var_dump($datos);

echo "<table border=1 style='width:100%;'>
<tr><th>Nombre</th>
<th>Stock</th>
<th>Unidad</th>
<th>Proveedor</th>
<th>Fono</th>
<th colspan='2'>Opciones</th></tr>";

if ($datos!=null) {
	for ($i=0; $i < count($datos); $i++) { 

		$id_insumo = $datos[$i]['id_insumo'];
		$nombre_insumo_original = $datos[$i]['nombre'];
		$nombre_insumo_minimizado = strtolower($nombre_insumo_original);
		$nombre_insumo = str_replace(" ", "-", $nombre_insumo_original);
		$stock_insumo=$datos[$i]['stock'];
		$id_proveedor = $datos[$i]['proveedor']['id_proveedor'];
		$nombre_proveedor = $datos[$i]['proveedor']['nombre'];
		$nombre_proveedor_arreglado = str_replace(" ", "-", $nombre_proveedor);
		$fono_proveedor = $datos[$i]['proveedor']['fono'];
		$id_medida = $datos[$i]['medida']['id_medida'];
		$unidad_medida_insumo = $datos[$i]['medida']['unidad'];

		if ($filtro==true) {
			if ($nombre_insumo_minimizado==$busquedaInsumo) {
				?>
				<form method="post" action="clasificar-accion-insumo.php" name= "formulario-clasificar-insumo">
					<?php 
					//Listado de todos los insumos
					echo "<tr>";
					echo "<td>".$nombre_insumo_original."</td>";
					echo "<td>".$stock_insumo."</td>";
					echo "<td>".$unidad_medida_insumo."</td>";
					echo "<td>".$nombre_proveedor."</td>";
					echo "<td>".$fono_proveedor."</td>";
					echo "<td>";
					?>
					<!--Inputs invisibles que se envían a clasificar accion insumo-->
					<input type="hidden" name="verIdInsumo" value=<?php echo $id_insumo; ?>>
					<input type="hidden" name="verNombre" value=<?php echo $nombre_insumo; ?>>
					<input type="hidden" name="verStock" value=<?php echo $stock_insumo; ?>>
					<input type="hidden" name="verIdProveedor" value=<?php echo $id_proveedor; ?>>
					<input type="hidden" name="verNombreProveedor" value=<?php echo $nombre_proveedor_arreglado; ?>>
					<input type="hidden" name="verFonoProveedor" value=<?php echo $fono_proveedor; ?>>
					<input type="hidden" name="verIdMedida" value=<?php echo $id_medida; ?>>
					<input type="hidden" name="verUnidadMedida" value=<?php echo $unidad_medida_insumo; ?>>

					<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
					<?php
					echo "</td>";
					echo "<td>";
					?>
					<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
					<?php
					echo "</td>";
					echo "</tr>";
					?>
				</form>
				<?php
			}
		}
		if ($filtro==false){
			?>
			<form method="post" action="clasificar-accion-insumo.php" name= "formulario-clasificar-insumo">
				<?php 
				//Listado de todos los insumos
				echo "<tr>";
				echo "<td>".$nombre_insumo_original."</td>";
				echo "<td>".$stock_insumo."</td>";
				echo "<td>".$unidad_medida_insumo."</td>";
				echo "<td>".$nombre_proveedor."</td>";
				echo "<td>".$fono_proveedor."</td>";
				echo "<td>";
				?>
				<!--Inputs invisibles que se envían a clasificar accion insumo-->
				<input type="hidden" name="verIdInsumo" value=<?php echo $id_insumo; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre_insumo; ?>>
				<input type="hidden" name="verStock" value=<?php echo $stock_insumo; ?>>
				<input type="hidden" name="verIdProveedor" value=<?php echo $id_proveedor; ?>>
				<input type="hidden" name="verNombreProveedor" value=<?php echo $nombre_proveedor_arreglado; ?>>
				<input type="hidden" name="verFonoProveedor" value=<?php echo $fono_proveedor; ?>>
				<input type="hidden" name="verIdMedida" value=<?php echo $id_medida; ?>>
				<input type="hidden" name="verUnidadMedida" value=<?php echo $unidad_medida_insumo; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}
	echo "</table>";
}else{
	echo "<tr>";
	echo "<td>Información Nula</td>";
	echo "<td>Información Nula</td>";
	echo "<td>Información Nula</td>";
	echo "<td>Información Nula</td>";
	echo "<td>Información Nula</td>";
	echo "<td>Información Nula</td>";
	echo "<td>Información Nula</td>";
	echo "</tr>";
	echo "</table>";
}
?>

