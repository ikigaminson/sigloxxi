<?php

//Rescatando los datos
$id_mesa=$_POST['id_mesa'];
$numero=$_POST['numero'];
$capacidad=$_POST['capacidad'];
$disponibilidad=$_POST['disponibilidad'];
$ruta=$_POST['ruta'];

//Declaración de Variables de Mensajes
$mensajeIdMesa="";
$mensajeNumero="";
$mensajeCapacidad="";
$mensajeDisponibilidad="";
$mensajeRuta="";

//Valida que los campos no esten vacios
if (empty($id_mesa)) {	
	$mensajeIdMesa=" *Id Mesa*";
}
if (empty($numero)) {
	$mensajeNumero=" *Numero*";
}
if (empty($capacidad)) {
	$mensajeCapacidad=" *Capacidad*";
}
if (empty($disponibilidad)) {
	$mensajeDisponibilidad=" *Disponibilidad*";
}
if (empty($ruta)) {
	$mensajeRuta=" *Ruta*";
}

//Válidando campos vacios
if (empty($id_mesa) || empty($numero)|| empty($capacidad)|| empty($disponibilidad)|| empty($ruta)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdMesa.$mensajeNumero.$mensajeCapacidad.$mensajeDisponibilidad.$mensajeRuta.'");</script>';
	echo '<script>window.location.href="admin-gestion-mesa.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/mesa/update';

	// Datos de consultas hechos en un array
	$data = array(
		'id_mesa'         => $id_mesa,
		'numero'         => $numero,
		'capacidad'         => $capacidad,
		'disponibilidad'         => $disponibilidad,
		'eliminado'         => "0"
	);

	//var_dump($data);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si el mesa se modifico
	if ($validado==1) {
		//Muestra mensaje al mesa que se agrego exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Mesa modificada exitosamente");</script>';
		echo '<script>window.location.href="admin-gestion-mesa.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se agrego el mesa*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo modificar la mesa !");</script>';
		echo '<script>window.location.href="admin-gestion-mesa.php";</script>';
	}		
}
?>