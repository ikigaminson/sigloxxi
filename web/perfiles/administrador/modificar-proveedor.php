<?php

//Rescatando los datos
$id_proveedor=$_POST['id_proveedor'];
$nombre=$_POST['nombre'];
$fono=$_POST['fono'];
$email=$_POST['email'];
$direccion=$_POST['direccion'];

$comprobar_nombre=$_POST['comprobar_nombre'];
$comprobar_direccion=$_POST['comprobar_direccion'];

if ($nombre == $comprobar_nombre) {
	$nombre_final = str_replace("-", " ", $nombre);
}else{
	$nombre_final = $nombre;
}
if ($direccion == $comprobar_direccion) {
	$direccion_final = str_replace("-", " ", $direccion);
}else{
	$direccion_final = $direccion;
}

//Declaración de Variables de Mensajes
$mensajeIdProveedor="";
$mensajeNombre="";
$mensajeFono="";
$mensajeEmail="";
$mensajeDireccion="";

//Valida que los campos no esten vacios
if (empty($id_proveedor)) {	
	$mensajeIdProveedor=" *Id Proveedor*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre*";
}
if (empty($fono)) {
	$mensajeFono=" *Fono*";
}
if (empty($email)) {
	$mensajeEmail=" *Email*";
}
if (empty($direccion)) {
	$mensajeDireccion=" *Direccion*";
}

//Válidando campos vacios
if (empty($id_proveedor) || empty($nombre)|| empty($fono)|| empty($email)|| empty($direccion)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdProveedor.$mensajeNombre.$mensajeFono.$mensajeEmail.$mensajeDireccion.'");</script>';
	echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/proveedor/update';

	// Datos de consultas hechos en un array
	$data = array(
		'id_proveedor'         => $id_proveedor,
		'nombre'         => $nombre_final,
		'fono'         => $fono,
		'email'         => $email,
		'direccion'         => $direccion_final
	);

	//var_dump($data);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si el proveedor se modifico
	if ($validado==1) {
		//Muestra mensaje al usuario que se modifico exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Proveedor modificado exitosamente");</script>';
		echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se modifico el proveedor*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo modificar el proveedor !");</script>';
		echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';
	}		
}
?>