<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:black;">
			<div class="nav-wrapper">
				<font size="5"><b>Administrador: Gestión de Usuarios</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="admin.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
			<div class="nav-content">
				<ul class="tabs tabs-transparent">
					<li class="tab">
						<a class="active no-autoinit" href="admin-agregar-usuario.php"><b>Agregar Usuarios</b></a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<ul class="sidenav" id="mobile-demo" style="background-color: black;">
		<li><a href="admin.php" style="color:white;"><b>Menu Principal</b></a></li>
		<li><a href="../cerrar-session.php" style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<?php
	function Listar(){
		include('listar-usuario.php');
	}
	?>
	
	<!--Inicio Contenedor1-->
	<div class="contenedor" align="center" vertical-align="bottom">
		<div class="container" float="right">
			<br>
			<br>
			<br>
			<!--Tipo y tamaño de letra-->
			<h5><font face="arial"><b>"En esta sección podrá visualizar los datos de sus trabajadores"</b></font></h5>
			<br>
			<div align="left" style="width:100%;">
				<form action="" method="post">
					<!--Fieldset: Recuadro para la busqueda del cargo de trabajadores-->
					<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
						<!--Legend: Titulo de fieldset-->
						<legend><h6><font face="arial"><b>Busqueda de trabajadores por cargo</b></font></h6></legend>
						<Select class="browser-default" name="busquedaCargo" style= "background-color: black; color: #ffffff; width:87%; float:left;">
							<option value="0">Seleccione Cargo</option>
							<option value="1">Administrador</option>
							<option value="2">Bodeguero</option>
							<option value="3">Cajero</option>
							<option value="4">Cocinero</option>
							<option value="5">Bartender</option>
							<option value="6">Garzon</option>
						</Select>
						<p style="float:left;">&nbsp;&nbsp;</p>
						<button class="waves-effect waves-light btn-small" type="submit" style="background-color: black; float:left; height: 45px;" href="" onclick="Listar();" >&#128269; Buscar</button>
					</fieldset>
				</form>
				<br>
				<!--Fieldset: Recuadro que contiene el listado de trabajadores-->
				<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
					<!--Legend: Titulo de fieldset-->
					<legend><h6><font face="arial"><b>Lista de trabajadores</b></font></h6></legend>
					<?php Listar();?>
				</fieldset>
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->
	
	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	
	<!--JavaScript de dropdown-->
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function(){
	    var elems = document.querySelectorAll('.sidenav');
	    var instances = M.Sidenav.init(elems, {});
	  	});
		// Or with jQuery
		$( document ).ready(function() {
			$('.sidenav').sidenav();
			$(".dropdown-trigger").dropdown();
		});
	</script>
</body>
</html>