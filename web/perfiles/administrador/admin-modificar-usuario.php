<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
if (isset($_POST['verId'])) {
	//Rescatando los datos enviados desde la lista
	$verId=$_POST['verId'];
	$verRut=$_POST['verRut'];
	$verDv=$_POST['verDv'];
	$verNombre=$_POST['verNombre'];
	$verPaterno=$_POST['verPaterno'];
	$verMaterno=$_POST['verMaterno'];
	$verEmail=$_POST['verEmail'];
	$verIdTipoPersona=$_POST['verIdTipoPersona'];	
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import estiloadmin.css-->
	<link rel="stylesheet" href="../../css/estiloadmin.css">
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:black;">
			<div class="nav-wrapper">
				<font size="5"><b>Administrador: Modificar Usuario</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="admin-gestion-usuario.php"><b>Volver</b></a></li>
					<li><a href="admin.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color: black;">
		<li><a href="admin-gestion-usuario.php" style="color:white;">Volver</a></li>
		<li><a href="admin.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-session.php" style="color:white;">Cerrar Sessión</a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="contenedor" align="center" vertical-align="bottom">
		<br>
		<br>
		<!--Tipo y tamaño de letra-->
		<h6 align="center"><font face="arial"><b> - "Podrá modificar el dato que necesite del trabajador"</b></font></h6>
		<br>
		<div class="container">
			<!-- Formulario de modificar usuario-->	
			<form method="post" action="modificar-usuario.php" style="background-color: black; border-color: black; border-radius: 20px; border: 2px solid white; background-attachment: fixed;">
				<div>
					<input type="hidden" name="id" value=<?php echo $verId; ?>>
					<input type="text" name="nombre" placeholder="Nombre" style= "color: #ffffff;" value=<?php if (isset($_POST['verId'])) { echo $verNombre;}else{ echo '';} ?>>
					<input type="text" name="apelPat" placeholder="Apellido Paterno" style= "color: #ffffff;" value=<?php if (isset($_POST['verId'])) { echo $verPaterno;}else{ echo '';} ?>>
					<input type="text" name="apelMat" placeholder="Apellido Materno" style= "color: #ffffff;" value=<?php if (isset($_POST['verId'])) { echo $verMaterno;}else{ echo '';} ?>>
					<input type="text" name="rut" placeholder="Rut" style= "color: #ffffff;" value=<?php if (isset($_POST['verId'])) {echo $verRut."-".$verDv;}else{ echo '';} ?>> 
					<input type="email" name="email" placeholder="Correo" style= "color: #ffffff;" value=<?php if (isset($_POST['verId'])) { echo $verEmail;}else{ echo '';} ?>>
					<div>
						<select class="browser-default" id="selectCargo" name="seleccionCargo" placeholder="cargo" style= "background-color: black; color:white;">	
							<option value="0"<?php if($verIdTipoPersona==0){echo "selected";} ?>>Seleccione Cargo</option>
							<option value="1"<?php if($verIdTipoPersona==1){echo "selected";} ?>>Administrador</option>
							<option value="2"<?php if($verIdTipoPersona==2){echo "selected";} ?>>Bodeguero</option>
							<option value="3"<?php if($verIdTipoPersona==3){echo "selected";} ?>>Cajero</option>
							<option value="4"<?php if($verIdTipoPersona==4){echo "selected";} ?>>Cocinero</option>
							<option value="5"<?php if($verIdTipoPersona==5){echo "selected";} ?>>Bartender</option>
							<option value="6"<?php if($verIdTipoPersona==6){echo "selected";} ?>>Garzon</option>
						</select>
					</div>
					<!--<input type="text" placeholder="Usuario" name="usuario" style= "color: #ffffff;">-->
					<!--<input type="password" placeholder="Contraseña" name="clave" style= "color: #ffffff;">-->
					<br>
					<button class="waves-effect waves-light btn-small" type="submit" style="background-color: white; color:black;" href=""><b>Modificar</b></button>
				</div>
			</form>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html>