<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!-- Inicio Menú Dropdown-->
	<!--Gestión Datos-->
	<ul id="id_dropGestionDatos" class="dropdown-content" style="background-color: black">
		<li><a href="admin-gestion-usuario.php" style="color: #ffffff"><b>Gestión de Usuarios</b></a></li>
		<li><a href="admin-gestion-insumo.php" style="color: #ffffff"><b>Gestión de Insumos</b></a></li>
		<li><a href="admin-gestion-mesa.php" style="color: #ffffff"><b>Gestión de Mesas</b></a></li>
		<li><a href="admin-gestion-cliente.php" style="color: #ffffff"><b>Gestión de Clientes</b></a></li>
		<li><a href="admin-gestion-proveedor.php" style="color: #ffffff"><b>Gestión de Proveedores</b></a></li>
	</ul>
	<!--Termino Menú Dropdown-->

	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:black;">
			<div class="nav-wrapper">
				<font size="5"><b>Administrador</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
			<div class="nav-wrapper">
				<ul class="left hide-on-med-and-dow">
					<!-- Inicio Menú Dropdown-->
					<!--Gestión Datos-->
					<li>
						<a href="#" class="dropdown-trigger" data-target="id_dropGestionDatos">
							<b>Gestión Datos</b>
							<i class="material-icons right">arrow_drop_down</i>
						</a>
					</li>
					<!--Termino Menú Dropdown-->
				</ul>
			</div>
		</nav>
	</div>
	
	<ul class="sidenav" id="mobile-demo" style="background-color: black;">
		<li><p>&nbsp;&nbsp;</p></li>
		<li><a href="../cerrar-session.php" style="color:white;">Cerrar Sessión</a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="contenedor" align="center" vertical-align="bottom">
		<br><br><br>
		<img src="../../img/bienvenido.png" align="center" vertical-align="bottom" height=”1050” width=1000” style="display: block; margin: 0 auto; max-width: 100%; width: 100%;">
	</div>
	<!--Fin Contenedor-->
	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!--JavaScript de dropdown-->
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
	<script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function(){
	    var elems = document.querySelectorAll('.sidenav');
	    var instances = M.Sidenav.init(elems);
	  	});
		// Or with jQuery
		$( document ).ready(function() {
			$(".dropdown-trigger").dropdown();
		});
	</script>
	<!--<script type="text/javascript" src="../../js/materialize.js">$(".dropdown-button").dropdown();</script>-->
</body>
</html>
