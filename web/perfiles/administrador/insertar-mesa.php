<?php


//Rescatando los datos
$id_mesa="1";
$numero=$_POST['numero'];
$capacidad=$_POST['capacidad'];
$disponibilidad=$_POST['disponibilidad'];
$eliminado="0";

//Declaración de Variables de Mensajes
$mensajeIdMesa="";
$mensajeNumero="";
$mensajeCapacidad="";
$mensajeDisponibilidad="";
$mensajeEliminado="";
$mensajeErrorNumeroCapacidad="";
$mensajeErrorNumeroNumerico="";
$mensajeErrorTipoNumeroCapacidad="";
$errorNumero=false;
$errorCapacidad=false;


//Valida que los campos no esten vacios
if (empty($id_mesa)) {	
	$mensajeIdMesa=" *Id Mesa*";
}
if (is_numeric($numero)) {
	$mensajeErrorNumeroNumerico="";
	$errorNumero=false;
}else{
	$mensajeErrorNumeroNumerico=" *El campo del numero no es un valor numerico*";
	$errorNumero=true;
}
if (empty($numero)) {
	$mensajeNumero=" *Numero*";
}
if (empty($capacidad)) {
	$mensajeCapacidad=" *Capacidad*";
}
if (is_numeric($capacidad)) {
	$mensajeErrorTipoNumeroCapacidad="";
	$errorCapacidad=false;
}else{
	$mensajeErrorTipoNumeroCapacidad=" *La capacidad no es un numero*";
	$errorCapacidad=true;
}
if($capacidad<1 || $capacidad>6){
	$mensajeErrorNumeroCapacidad=" *La capacidad debe ser entre 1 y 6*";
	$errorCapacidad=true;
}
if (empty($disponibilidad)) {
	$mensajeDisponibilidad=" *Disponibilidad*";
}

//Válidando campos vacios
if (empty($id_mesa) || empty($numero)|| empty($capacidad)|| empty($disponibilidad)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdMesa.$mensajeNumero.$mensajeCapacidad.$mensajeDisponibilidad.'");</script>';
	echo '<script>window.location.href="admin-agregar-mesa.php";</script>';
}elseif($errorNumero==true || $errorCapacidad==true) {
	echo '<script language="javascript" style="color: red;">alert("Errores:'.$mensajeErrorNumeroNumerico.$mensajeErrorNumeroCapacidad.$mensajeErrorTipoNumeroCapacidad.'");</script>';
	echo '<script>window.location.href="admin-agregar-mesa.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/mesa/insert';

	// Datos de consultas hechos en un array
	$data = array(
		'id_mesa'         => $id_mesa,
		'numero'         => $numero,
		'capacidad'         => $capacidad,
		'disponibilidad'         => $disponibilidad,
		'eliminado'         => $eliminado,
	);

	//var_dump($data);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si la mesa se modifico
	if ($validado==1) {
		//Muestra mensaje la mesa que se agrego exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Mesa agregada exitosamente");</script>';
		echo '<script>window.location.href="admin-gestion-mesa.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se agrego la mesa*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo agregar la mesa !");</script>';
		echo '<script>window.location.href="admin-gestion-mesa.php";</script>';
	}		
}
?>