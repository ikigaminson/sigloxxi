<?php 
$modificarPagina="<script language='javascript'>window.location.replace('admin-modificar-mesa.php');</script>";
$eliminarPagina="<script language='javascript'>window.location.replace('eliminar-mesa.php');</script>";

//Rescatando los datos
$verIdMesa=$_POST['verIdMesa'];
$verNumero=$_POST['verNumero'];
$verCapacidad=$_POST['verCapacidad'];
$verDisponibilidad=$_POST['verDisponibilidad'];
$verRuta=$_POST['verRuta'];

if (isset($_POST['boton_modificar'])) {
	?>
	<form name='enviar' method='post' action='admin-modificar-mesa.php'>
		<input type="hidden" name="verIdMesa" value=<?php echo $verIdMesa; ?>>
		<input type="hidden" name="verNumero" value=<?php echo $verNumero; ?>>
		<input type="hidden" name="verCapacidad" value=<?php echo $verCapacidad; ?>>
		<input type="hidden" name="verDisponibilidad" value=<?php echo $verDisponibilidad; ?>>
		<input type="hidden" name="verRuta" value=<?php echo $verRuta; ?>>
	</form>
	<script language='JavaScript'>
		document.enviar.submit();
	</script>
	<?php
	echo $modificarPagina;
}elseif(isset($_POST['boton_eliminar'])){
	?>
	<form name='enviar2' method='post' action='eliminar-mesa.php'>
		<input type="hidden" name="verIdMesa" value=<?php echo $verIdMesa; ?>>
		<input type="hidden" name="verNumero" value=<?php echo $verNumero; ?>>
		<input type="hidden" name="verCapacidad" value=<?php echo $verCapacidad; ?>>
		<input type="hidden" name="verDisponibilidad" value=<?php echo $verDisponibilidad; ?>>
		<input type="hidden" name="verRuta" value=<?php echo $verRuta; ?>>
	</form>
	
	<script language='JavaScript'>
		document.enviar2.submit();
	</script>
	<?php
	echo $eliminarPagina;
}else{
	echo '<script language="javascript" style="color: red;">alert("¡ Error al traspasar datos !");</script>';
	echo '<script>window.location.href="admin-gestion-mesa.php";</script>';
}
?>