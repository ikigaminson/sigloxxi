<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
if (isset($_POST['verIdInsumo'])) {
	//Rescatando los datos
	$verIdInsumo=$_POST['verIdInsumo'];
	$verNombre=$_POST['verNombre'];
	$verNombreInsumo=str_replace("-", " ", $verNombre);
	$verStock=$_POST['verStock'];
	$verIdProveedor=$_POST['verIdProveedor'];
	$verNombreProveedor=$_POST['verNombreProveedor'];
	$verNombreProveedorArreglado=str_replace("-", " ", $verNombreProveedor);
	$verFonoProveedor=$_POST['verFonoProveedor'];
	$verIdMedida=$_POST['verIdMedida'];
	$verUnidadMedida=$_POST['verUnidadMedida'];
}

?>

<!DOCTYPE html>
<html>
<head>
	<!--Import estiloadmin.css-->
	<link rel="stylesheet" href="../../css/estiloadmin.css">
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:black;">
			<div class="nav-wrapper">
				<font size="5"><b>Administrador: Modificar Insumo</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="admin-gestion-insumo.php"><b>Volver</b></a></li>
					<li><a href="admin.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color: black;">
		<li><a href="admin-gestion-insumo.php" style="color:white;">Volver</a></li>
		<li><a href="admin.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-session.php" style="color:white;">Cerrar Sessión</a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="contenedor" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<!--Tipo y tamaño de letra-->
			<h6><font face="arial"><b> - "Solo podrá modificar el 'STOCK' de insumos"</b></font></h6>
			<br>
			<!-- Formulario de modificar registro de insumo-->
			<form method="post" action="modificar-insumo.php" style="background-color: black; border-color: black; border-radius: 20px; border: 2px solid white;">
				<div>
					<input type="hidden" name="id_insumo" value=<?php echo $verIdInsumo; ?>>
					<input type="text" name="nombre" placeholder="Nombre Insumo" style= "color: #ffffff;" value=<?php if (isset($_POST['verIdInsumo'])) { echo $verNombre;}else{ echo '';} ?> readonly="readonly">
					<input type="text" name="stock" placeholder="Stock" style= "color: #ffffff;" value=<?php if (isset($_POST['verIdInsumo'])) { echo $verStock;}else{ echo '';} ?>>
					<input type="text" name="unidad_medida" placeholder="Unidad Medida" style= "color: #ffffff;" value=<?php if (isset($_POST['verIdInsumo'])) { echo $verUnidadMedida;}else{ echo '';}?> readonly="readonly">
					<input type=hidden name=id_proveedor value=<?php echo $verIdProveedor; ?>>
					<input type="text" name="nombre_proveedor" placeholder="Nombre Proveedor" style= "color: #ffffff;" value=<?php if (isset($_POST['verIdInsumo'])) { echo $verNombreProveedor;}else{ echo '';}?> readonly="readonly">
					<input type="text" name="fono_proveedor" placeholder="Fono Proveedor" style= "color: #ffffff;" value=<?php if (isset($_POST['verIdInsumo'])) { echo $verFonoProveedor;}else{ echo '';}?> readonly="readonly">
					<input type="hidden" name=id_medida value=<?php echo $verIdMedida; ?>>

					<button class="waves-effect waves-light btn-small" type="submit" style="background-color: white; color:black" href=""><b>Modificar</b></button>
				</div>
			</form>	
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html>