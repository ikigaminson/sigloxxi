<?php

//Funcion Validar Rut
function ValidaDVRut($rut) {

	$tur = strrev($rut);
	$mult = 2;
	$suma = 0;

	for ($i = 0; $i <= strlen($tur); $i++) { 
		if ($mult > 7) $mult = 2; 

		$suma = $mult * (int)substr($tur, $i, 1) + $suma;
		$mult = $mult + 1;
	}

	$valor = 11 - ($suma % 11);

	if ($valor == 11) { 
		$codigo_veri = "0";
	} elseif ($valor == 10) {
		$codigo_veri = "k";
	} else { 
		$codigo_veri = $valor;
	}
	return $codigo_veri;
}

//Rescatando los datos
$id=$_POST['id'];
$nombre=$_POST['nombre'];
$paterno=$_POST['apelPat'];
$materno=$_POST['apelMat'];
$checkRut=$_POST['rut'];
$email=$_POST['email'];
$cargo=$_POST['id_tipo_persona'];
$descripcion=$_POST['descripcion'];

//Declaración de Variables de Mensajes
$mensajeRut="";
$mensajeNombre="";
$mensajePaterno="";
$mensajeMaterno="";
$mensajeEmail="";
$mensajeCargo="";
$mensajeDescripcion="";

//Valida que los campos no esten vacios
if (empty($checkRut)) {	
	$mensajeRut=" *Rut*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre*";
}
if (empty($paterno)) {
	$mensajePaterno=" *Apellido Paterno*";
}
if (empty($materno)) {
	$mensajeMaterno=" *Apellido Materno*";
}
if (empty($email)) {
	$mensajeEmail=" *Email*";
}
if (empty($cargo)) {
	$mensajeCargo=" *Cargo*";
}
if (empty($descripcion)) {
	$mensajeDescripcion=" *Descripcion*";
}

//Válidando campos vacios
if (empty($checkRut) || empty($nombre)|| empty($paterno)|| empty($materno)|| empty($email)|| empty($cargo) || empty($descripcion)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeRut.$mensajeNombre.$mensajePaterno.$mensajeMaterno.$mensajeEmail.$mensajeCargo.$mensajeDescripcion.'");</script>';
	echo '<script>window.location.href="admin-gestion-cliente.php";</script>';
}else{
	if ($cargo!=7) {
		//Si el $dv esta incorrecto manda error*****
		echo '<script language="javascript" style="color: red;">alert("El seleccionado no es un cliente");</script>';
		echo '<script>window.location.href="admin-gestion-cliente.php";</script>';	
	}else{
		//Toma el valor del Rut y lo divide en dos
		$rutSinPunto=str_replace(".","",$checkRut);
		$rutSinGuion=str_replace("-","",$rutSinPunto);
		$dv=substr($checkRut, -1);
		$rut=substr($rutSinGuion, 0, -1);

		//Valida que la variable del digito verificador sea ingresada correctamente
		if ($dv<0 && $dv>9 && $dv!='K') {
			//Si el $dv esta incorrecto manda error*****
			echo '<script language="javascript" style="color: red;">alert("Digito verificador incorrecto");</script>';
			echo '<script>window.location.href="admin-gestion-cliente.php";</script>';
		}else{
			//Si el $dv esta correcto ingresa*****
			//Valida que el rut exista
			if (ValidaDVRut($rut)!=$dv){
				echo '<script language="javascript" style="color: red;">alert("Rut inválido");</script>';
				echo '<script>window.location.href="admin-gestion-cliente.php";</script>';
			}else{
				//Si el Rut existe actualiza*****
				
				//Url al cual le hacemos una consulta
				$url = 'localhost:4567/usuario/update';

				// Datos de consultas hechos en un array
				$data = array(
					'id_persona'         => $id,
					'rut'         => $rut,
					'dv'         => $dv,
					'nombre'         => $nombre,
					'paterno'         => $paterno,
					'materno'         => $materno,
					'mail'         => $email,
					'idTipo'         => $cargo
				);

				//var_dump($data);

				
				//Transformacion del array a un archivo json 
				$fields_string = json_encode($data);

				// Crear un nuevo recurso "cURL" 
				$ch = curl_init($url);

				//Establecer número de variables POST, datos POST
				curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

				//Establecer el tipo de contenido en application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

				//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

				//Ejecuta el posteo
				$result = curl_exec($ch);

				//Parsear la data a array
				$parse_result = json_decode($result, true);
				
				//var_dump($parse_result);

				$validado=$parse_result['result'];
				

				//Válida que si el cliente se modifico
				if ($validado==1) {
					//Muestra mensaje al usuario que se agrego exitosamente*****
					echo '<script language="javascript" style="color: red;">alert("Cliente modificado exitosamente");</script>';
					echo '<script>window.location.href="admin-gestion-cliente.php";</script>';
				}else{
					//Muestra mensaje al usuario que no se agrego el cliente*****
					echo '<script language="javascript" style="color: red;">alert("¡ No se pudo modificar !");</script>';
					echo '<script>window.location.href="admin-gestion-cliente.php";</script>';
				}
			}	
		}		
	}			
}
?>