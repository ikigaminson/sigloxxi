<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<?php

//Url al cual le hacemos una consulta
$url = 'localhost:4567/proveedor/list';

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Transforma el resultado json en array
$datos = json_decode($result, true);

//var_dump($datos);

echo"<table border=1 style='width:100%;'>
<tr><th>Nombre</th>
<th>Fono</th>
<th>Email</th>
<th>Dirección</th>
<th colspan='2'>Opciones</th></tr>";
for ($i=0; $i < count($datos); $i++) { 
	//Rescatando los Datos
	$id_proveedor=$datos[$i]['id_proveedor'];
	$nombre_original = $datos[$i]['nombre'];
	$nombre = str_replace(" ", "-", $nombre_original);
	$fono = $datos[$i]['fono'];
	$email = $datos[$i]['email'];
	$direccion_original = $datos[$i]['direccion'];
	$direccion = str_replace(" ", "-", $direccion_original);
	?>
	<form method="post" action="clasificar-accion-proveedor.php" name= "formulario-clasificar-proveedor">
		<?php
		echo "<tr>";
		echo "<td><div align='center'>".$nombre_original."</div></td>";
		echo "<td><div align='center'>".$fono."</div></td>"; 
		echo "<td><div align='center'>".$email."</div></td>";
		echo "<td><div align='center'>".$direccion_original."</div></td>"; 
		echo "<td>";
		?>
		<!--Inputs invisibles que se envían a clasificar accion mesa-->
		<input type="hidden" name="verIdProveedor" value=<?php echo $id_proveedor; ?>>
		<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
		<input type="hidden" name="verFono" value=<?php echo $fono; ?>>
		<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
		<input type="hidden" name="verDireccion" value=<?php echo $direccion; ?>>
		<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
		<?php
		echo "</td>";
		echo "<td>";
		?>
		<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
		<?php
		echo "</td>";
		echo "</tr>";
		?>
	</form>
	<?php
}
echo "</table>";

?>