<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
    background-color:#E8EAEB;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color:#6B859D;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}	
</style>
<?php 
//Valida que el Filtro haya sido activado
if (isset($_POST['busquedaCargo'])) {
	$busquedaCargo=$_POST['busquedaCargo'];
	if ($busquedaCargo==0) {
		//Mostrar Todos
		$filtro=0;
	}
	if ($busquedaCargo==1) {
		//Mostrar Administradores
		$filtro=1;
	}
	if ($busquedaCargo==2) {
		//Mostrar Bodegueros
		$filtro=2;
	}
	if ($busquedaCargo==3) {
		//Mostrar Cajeros
		$filtro=3;
	}
	if ($busquedaCargo==4) {
		//Mostrar Cocineros
		$filtro=4;
	}
	if ($busquedaCargo==5) {
		//Mostrar Bartenders
		$filtro=5;
	}
	if ($busquedaCargo==6) {
		//Mostrar Garzón
		$filtro=6;
	}
}else{
	$filtro=0;
}

//Url al cual le hacemos una consulta
$url = 'localhost:4567/trabajador/list';

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Transforma el resultado json en array
$datos = json_decode($result, true);

//var_dump($result);

echo"<table border=1 width:100%;'>
<tr><th>Nombre</th>
<th>Apellido Paterno</th>
<th>Apellido Materno</th>
<th>Rut</th>
<th>Digito Verificador</th>
<th>Email</th>
<th>Cargo</th>
<th colspan='2'>Opciones</th></tr>";
for ($i=0; $i < count($datos); $i++) {
	$id_trabajador=$datos[$i]['id_trabajador'];
	$id_persona=$datos[$i]['persona']['id_persona'];
	$rut=$datos[$i]['persona']['rut'];
	$dv=$datos[$i]['persona']['dv'];
	$nombre = $datos[$i]['persona']['nombre'];
	$paterno = $datos[$i]['persona']['paterno'];
	$materno = $datos[$i]['persona']['materno'];
	$email = $datos[$i]['persona']['email'];
	$id_tipo_persona = $datos[$i]['persona']['tipoPersona']['id_tipo_persona'];
	$descripcion = $datos[$i]['persona']['tipoPersona']['descripcion'];

	if ($filtro==0) {
		//Listado de todos los trabajadores
		if ($id_tipo_persona>0 && $id_tipo_persona<7) {
			?>
			<form method="post" action="clasificar-accion.php" name= "formulario-clasificar">
				<?php
				echo "<tr>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$paterno."</td>";
				echo "<td>".$materno."</td>";
				echo "<td>".$rut."</td>";
				echo "<td>".$dv."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$descripcion."</td>";
				echo "<td>";
				?>
				<input type="hidden" name="verIdTrabajador" value=<?php echo $id_trabajador; ?>>
				<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
				<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
				<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
				<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
				<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
				<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}elseif ($filtro==1) {
		//Filtrar Administradores
		if ($id_tipo_persona==1) {
			?>
			<form method="post" action="clasificar-accion.php" name= "formulario-clasificar">
				<?php
				echo "<tr>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$paterno."</td>";
				echo "<td>".$materno."</td>";
				echo "<td>".$rut."</td>";
				echo "<td>".$dv."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$descripcion."</td>";
				echo "<td>";
				?>
				<input type="hidden" name="verIdTrabajador" value=<?php echo $id_trabajador; ?>>
				<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
				<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
				<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
				<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
				<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
				<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}elseif ($filtro==2) {
		//Filtrar Bodegueros
		if ($id_tipo_persona==2) {
			?>
			<form method="post" action="clasificar-accion.php" name= "formulario-clasificar">
				<?php
				echo "<tr>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$paterno."</td>";
				echo "<td>".$materno."</td>";
				echo "<td>".$rut."</td>";
				echo "<td>".$dv."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$descripcion."</td>";
				echo "<td>";
				?>
				<input type="hidden" name="verIdTrabajador" value=<?php echo $id_trabajador; ?>>
				<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
				<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
				<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
				<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
				<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
				<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}elseif ($filtro==3) {
		//Filtrar Cajeros
		if ($id_tipo_persona==3) {
			?>
			<form method="post" action="clasificar-accion.php" name= "formulario-clasificar">
				<?php
				echo "<tr>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$paterno."</td>";
				echo "<td>".$materno."</td>";
				echo "<td>".$rut."</td>";
				echo "<td>".$dv."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$descripcion."</td>";
				echo "<td>";
				?>
				<input type="hidden" name="verIdTrabajador" value=<?php echo $id_trabajador; ?>>
				<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
				<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
				<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
				<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
				<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
				<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}elseif ($filtro==4) {
		//Filtrar Cocineros
		if ($id_tipo_persona==4) {
			?>
			<form method="post" action="clasificar-accion.php" name= "formulario-clasificar">
				<?php
				echo "<tr>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$paterno."</td>";
				echo "<td>".$materno."</td>";
				echo "<td>".$rut."</td>";
				echo "<td>".$dv."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$descripcion."</td>";
				echo "<td>";
				?>
				<input type="hidden" name="verIdTrabajador" value=<?php echo $id_trabajador; ?>>
				<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
				<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
				<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
				<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
				<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
				<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}elseif ($filtro==5) {
		//Filtrar Bartenders
		if ($id_tipo_persona==5) {
			?>
			<form method="post" action="clasificar-accion.php" name= "formulario-clasificar">
				<?php
				echo "<tr>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$paterno."</td>";
				echo "<td>".$materno."</td>";
				echo "<td>".$rut."</td>";
				echo "<td>".$dv."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$descripcion."</td>";
				echo "<td>";
				?>
				<input type="hidden" name="verIdTrabajador" value=<?php echo $id_trabajador; ?>>
				<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
				<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
				<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
				<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
				<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
				<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}elseif ($filtro==6) {
		//Filtrar Garzón
		if ($id_tipo_persona==6) {
			?>
			<form method="post" action="clasificar-accion.php" name= "formulario-clasificar">
				<?php
				echo "<tr>";
				echo "<td>".$nombre."</td>";
				echo "<td>".$paterno."</td>";
				echo "<td>".$materno."</td>";
				echo "<td>".$rut."</td>";
				echo "<td>".$dv."</td>";
				echo "<td>".$email."</td>";
				echo "<td>".$descripcion."</td>";
				echo "<td>";
				?>
				<input type="hidden" name="verIdTrabajador" value=<?php echo $id_trabajador; ?>>
				<input type="hidden" name="verId" value=<?php echo $id_persona; ?>>
				<input type="hidden" name="verRut" value=<?php echo $rut; ?>>
				<input type="hidden" name="verDv" value=<?php echo $dv; ?>>
				<input type="hidden" name="verNombre" value=<?php echo $nombre; ?>>
				<input type="hidden" name="verPaterno" value=<?php echo $paterno; ?>>
				<input type="hidden" name="verMaterno" value=<?php echo $materno; ?>>
				<input type="hidden" name="verEmail" value=<?php echo $email; ?>>
				<input type="hidden" name="verIdTipoPersona" value=<?php echo $id_tipo_persona; ?>>
				<div align="center"><button name="boton_modificar" class="large material-icons" type="submit" style="width:30px;">editar</button></div>
				<?php
				echo "</td>";
				echo "<td>";
				?>
				<div align="center"><button name="boton_eliminar" class="large material-icons" type="submit" style="width:30px;">delete_sweep</button></div>
				<?php
				echo "</td>";
				echo "</tr>";
				?>
			</form>
			<?php
		}
	}
}
echo "</table>";

?>







