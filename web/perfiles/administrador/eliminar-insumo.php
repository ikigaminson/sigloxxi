<?php

//Rescatando los datos
$verIdInsumo=$_POST['verIdInsumo'];
$verNombre=$_POST['verNombre'];
$verStock=$_POST['verStock'];
$verIdProveedor=$_POST['verIdProveedor'];
$verNombreProveedor=$_POST['verNombreProveedor'];
$verFonoProveedor=$_POST['verFonoProveedor'];
$verIdMedida=$_POST['verIdMedida'];
$verUnidadMedida=$_POST['verUnidadMedida'];

//Url al cual le pedimos el delete
$url = 'localhost:4567/insumo/delete';

// Datos enviados en un array
$data = array(
	'id_insumo'         => $verIdInsumo,
);

//Transformacion del array a un archivo json 
$fields_string = json_encode($data);

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Le introducimos la data a la consulta
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Parsear la data a array
$parse_result = json_decode($result, true);

//var_dump($parse_result["result"]);

//Imprimiendo el array
$datos=$parse_result["result"];

//Verifica si se ejecuto bien el cambio
if ($datos==1) {
	//Muestra operacion exitosa
	echo '<script language="javascript" style="color: red;">alert("Insumo Eliminado");</script>';
	echo '<script>window.location.href="admin-gestion-insumo.php";</script>';
}elseif($datos == -1){
	//Manda error en caso que no se pudiese eliminar el insumo
	echo '<script language="javascript" style="color: red;">alert("No existe este insumo");</script>';
	echo '<script>window.location.href="admin-gestion-insumo.php";</script>';
}else{
	//Manda error en caso de que suceda algo en la base de datos
	echo '<script language="javascript" style="color: red;">alert("Error en la Base de Datos al borrar insumo");</script>';
	echo '<script>window.location.href="admin-gestion-insumo.php";</script>';
}

//Redirecciona al eliminar usuario
//header("Location:admin-eliminar-usuario.php");
?>