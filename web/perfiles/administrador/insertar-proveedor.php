<?php


//Rescatando los datos
$id_proveedor="1";
$nombre=$_POST['nombre'];
$telefono=$_POST['telefono'];
$email=$_POST['email'];
$direccion=$_POST['direccion'];
$eliminado="0";

//Declaración de Variables de Mensajes
$mensajeIdProveedor="";
$mensajeNombre="";
$mensajeTelefono="";
$mensajeEmail="";
$mensajeDireccion="";

//Valida que los campos no esten vacios
if (empty($id_proveedor)) {	
	$mensajeIdProveedor=" *Id Proveedor*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre*";
}
if (empty($telefono)) {
	$mensajeTelefono=" *Telefono*";
}
if (empty($email)) {
	$mensajeEmail=" *Email*";
}
if (empty($direccion)) {
	$mensajeDireccion=" *Direccion*";
}

//Válidando campos vacios
if (empty($id_proveedor) || empty($nombre)|| empty($telefono)|| empty($email)|| empty($direccion)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdProveedor.$mensajeNombre.$mensajeTelefono.$mensajeEmail.$mensajeDireccion.'");</script>';
	echo '<script>window.location.href="admin-agregar-proveedor.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/proveedor/insert';

	// Datos de consultas hechos en un array
	$data = array(
		'id_proveedor'         => $id_proveedor,
		'nombre'         => $nombre,
		'fono'         => $telefono,
		'email'         => $email,
		'direccion'         => $direccion,
	);

	//var_dump($data);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si el proveedor se modifico
	if ($validado==1) {
		//Muestra mensaje el proveedor que se agrego exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Proveedor agregado exitosamente");</script>';
		echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se agrego el proveedor*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo agregar el proveedor !");</script>';
		echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';
	}		
}
?>