<?php

//Rescatando los datos
$verIdProveedor=$_POST['verIdProveedor'];
$verNombre=$_POST['verNombre'];
$verFono=$_POST['verFono'];
$verEmail=$_POST['verEmail'];
$verDireccion=$_POST['verDireccion'];

//Url al cual le pedimos el delete
$url = 'localhost:4567/proveedor/delete';

// Datos enviados en un array
$data = array(
	'id_proveedor'         => $verIdProveedor,
);

//Transformacion del array a un archivo json 
$fields_string = json_encode($data);

// Crear un nuevo recurso "cURL" 
$ch = curl_init($url);

//Establecer número de variables POST, datos POST
curl_setopt($ch,CURLOPT_POST, true);

//Le introducimos la data a la consulta
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//Establecer el tipo de contenido en application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

//Ejecuta el posteo
$result = curl_exec($ch);

//Parsear la data a array
$parse_result = json_decode($result, true);

//var_dump($parse_result["result"]);

//Imprimiendo el array
$validado=$parse_result["result"];

if ($validado == "1") {
	//Muestra operacion exitosa
	echo '<script language="javascript" style="color: red;">alert("Proveedor Eliminado");</script>';
	echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';
}elseif($validado == "-1"){
	//Manda error en caso que no se pudiese eliminar el proveedor
	echo '<script language="javascript" style="color: red;">alert("Error al eliminar proveedor");</script>';
	echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';
}else{
	//Manda error en caso de que suceda algo en la base de datos
	echo '<script language="javascript" style="color: red;">alert("Error en la Base de Datos al borrar proveedor");</script>';
	echo '<script>window.location.href="admin-gestion-proveedor.php";</script>';	
}
?>