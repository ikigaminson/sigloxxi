<?php
//Funcion Validar Rut
function ValidaDVRut($rut) {

	$tur = strrev($rut);
	$mult = 2;
	$suma = 0;

	for ($i = 0; $i <= strlen($tur); $i++) { 
		if ($mult > 7) $mult = 2; 

		$suma = $mult * (int)substr($tur, $i, 1) + $suma;
		$mult = $mult + 1;
	}

	$valor = 11 - ($suma % 11);

	if ($valor == 11) { 
		$codigo_veri = "0";
	} elseif ($valor == 10) {
		$codigo_veri = "k";
	} else { 
		$codigo_veri = $valor;
	}
	return $codigo_veri;
}

//Rescatar los datos de los campos
$checkRut=$_POST['rut'];
$nombre=$_POST['nombre']; 
$apelPat=$_POST['apelPat']; 
$apelMat=$_POST['apelMat']; 
$email=$_POST['email']; 
$cargo=$_POST['seleccionCargo'];
$usuario=$_POST['usuario'];
$pass=$_POST['clave']; 
$clave=hash("sha256",$pass);

//Declaración de Variables de Mensajes
$mensajeRut="";
$mensajeNombre="";
$mensajeApelPat="";
$mensajeApelMat="";
$mensajeEmail="";
$mensajeCargo="";
$mensajeUsuario="";
$mensajeClave="";

//Valida que los campos no esten vacios
if (empty($checkRut)) {	
	$mensajeRut=" *Rut*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre*";
}
if (empty($apelPat)) {
	$mensajeApelPat=" *Apellido Paterno*";
}
if (empty($apelMat)) {
	$mensajeApelMat=" *Apellido Materno*";
}
if (empty($email)) {
	$mensajeEmail=" *Email*";
}
if (empty($cargo)) {
	$mensajeCargo=" *Cargo*";
}
if (empty($usuario)) {
	$mensajeUsuario=" *Usuario*";
}
if (empty($clave)) {
	$mensajeClave=" *Clave*";
}
if (empty($checkRut) || empty($nombre)|| empty($apelPat)|| empty($apelMat)|| empty($email)|| empty($cargo)|| empty($usuario)|| empty($clave)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeRut.$mensajeNombre.$mensajeApelPat.$mensajeApelMat.$mensajeEmail.$mensajeCargo.$mensajeUsuario.$mensajeClave.'");</script>';
	echo '<script>window.location.href="admin-agregar-usuario.php";</script>';
}else{
	if ($cargo<1 && $cargo>6) {
		//Si el $dv esta incorrecto manda error*****
		echo '<script language="javascript" style="color: red;">alert("Seleccione un Cargo Válido");</script>';
		echo '<script>window.location.href="admin-agregar-usuario.php";</script>';	
	}else{
		//Toma el valor del Rut y lo divide en dos
		$rutSinPunto=str_replace(".","",$checkRut);
		$rutSinGuion=str_replace("-","",$rutSinPunto);
		$dv=substr($checkRut, -1);
		$rut=substr($rutSinGuion, 0, -1);

		//Valida que la variable del digito verificador sea ingresada correctamente
		if ($dv<0 && $dv>9 && $dv!='K') {
			//Si el $dv esta incorrecto manda error*****
			echo '<script language="javascript" style="color: red;">alert("Digito verificador incorrecto");</script>';
			echo '<script>window.location.href="admin-agregar-usuario.php";</script>';
		}else{
			//Si el $dv esta correcto ingresa*****
			//Valida que el rut exista
			if (ValidaDVRut($rut)!=$dv){
				echo '<script language="javascript" style="color: red;">alert("Rut inválido");</script>';
				echo '<script>window.location.href="admin-agregar-usuario.php";</script>';
			}else{
				//Si el Rut existe ingresa*****
				//Url al cual le hacemos una consulta
				$url = 'localhost:4567/usuario/insert';

				// Datos de consultas hechos en un array
				$data = array(
					'rut'         => $rut,
					'dv'         => $dv,
					'nombre'         => $nombre,
					'paterno'         => $apelPat,
					'materno'         => $apelMat,
					'mail'         => $email,
					'idTipo'         => $cargo
				);

				//var_dump($data);

				//Transformacion del array a un archivo json 
				$fields_string = json_encode($data);

				// Crear un nuevo recurso "cURL" 
				$ch = curl_init($url);

				//Establecer número de variables POST, datos POST
				curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

				//Establecer el tipo de contenido en application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

				//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

				//Ejecuta el posteo
				$result = curl_exec($ch);

				//Transforma el resultado json en array
				$array = json_decode($result, true);

				//Accede al dato del array
				$last_id = $array['last_id'];

				//var_dump($array);

				//Valida si la persona se agrego o no
				if ($last_id == -1) {
					//Muestra mensaje que no se agrego el usuario*****
					echo '<script language="javascript" style="color: red;">alert("¡Error! Persona No Ingresada");</script>';
					echo '<script>window.location.href="admin-agregar-usuario.php";</script>';
				}else{
					//Si la persona se agrega, se pasa a agregar el Usuario*****
					$id_trabajador="0";
					$eliminado="0";
					$id_persona=strval($last_id);
					//Url al cual le hacemos una consulta
					$url = 'localhost:4567/trabajador/insert';

					// Datos de consultas hechos en un array
					$data = array(
						'id_trabajador'         => $id_trabajador,
						'user'         => $usuario,
						'pass'         => $clave,
						'id_persona'         => $id_persona,
						'eliminado'         => $eliminado
					);

					//Transformacion del array a un archivo json 
					$fields_string = json_encode($data);

					// Crear un nuevo recurso "cURL" 
					$ch = curl_init($url);

					//Establecer número de variables POST, datos POST
					curl_setopt($ch,CURLOPT_POST, true);
					curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

					//Establecer el tipo de contenido en application/json
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

					//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

					//Ejecuta el posteo
					$result = curl_exec($ch);

					//Transforma el resultado json en array
					$array = json_decode($result, true);

					//Accede al dato del array
					$result = $array['result'];

					//Valida que el Usuario haya sido ingresado
					if ($result == "1") {
						//Muestra mensaje al usuario que se agrego exitosamente*****
						echo '<script language="javascript" style="color: red;">alert("Usuario Agregado Exitosamente");</script>';
						echo '<script>window.location.href="admin-agregar-usuario.php";</script>';
					}elseif($result == "-1"){
						//Muestra mensaje al usuario que no se agrego el Usuario*****
						echo '<script language="javascript" style="color: red;">alert("¡Error! no se pudo procesar en la Base de Datos");</script>';
						echo '<script>window.location.href="admin-agregar-usuario.php";</script>';
					}else{
						//Muestra mensaje al usuario que no se agrego el Usuario*****
						echo '<script language="javascript" style="color: red;">alert("¡Ejecución Fallida! Verificar Datos");</script>';
						echo '<script>window.location.href="admin-agregar-usuario.php";</script>';
					}	
				}
			}
		}
	}
}

?>