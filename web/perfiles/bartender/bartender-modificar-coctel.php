<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
if (isset($_POST['verIdCoctel'])) {
	//Rescatando los datos
	$verIdCoctel=$_POST['verIdCoctel'];
	$verNombre=$_POST['verNombre'];
	$verDescripcion=$_POST['verDescripcion'];
	$verPrecio=$_POST['verPrecio'];
	$verDisponibilidad=$_POST['verDisponibilidad'];
	$verUrlCoctel=$_POST['verUrlCoctel'];
	$verIdOrigen=$_POST['verIdOrigen'];
	$verDescripcionOrigen=$_POST['verDescripcionOrigen'];
	$verEliminado=$_POST['verEliminado'];
	$disponible='D';
	$no_disponible='N';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:green;">
			<div class="nav-wrapper">
				<font size="5"><b>Bartender: Modificar Cócteles</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="bartender-gestion-coctel.php"><b>Volver</b></a></li>
					<li><a href="bartender.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color: green;">
		<li><a href="bartender-gestion-coctel.php" style="color:white;">Volver</a></li>
		<li><a href="bartender.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-session.php" style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="conten" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<!--Tipo y tamaño de letra-->
			<h6><font face="arial"><b style="background-color:white; color:black;"> - "Ingrese los datos que necesita modificar del coctel"</b></font></h6>
			<br>
			<!--Formulario para modificar coctel-->
			<form method="post" action="modificar-coctel.php" style="background:green; margin: auto; width: 50%; max-width: 560px; padding: 30px; border: 2px solid white; border-radius: 10px;" align="center">
				<h4><b style="color:white;">Actualice su Coctel</b></h4>
				<input type="hidden" name="id_coctel" value=<?php echo $verIdCoctel; ?>>
				<input type="text" name="nombre" placeholder="Nombre Coctel" style="color: #ffffff;" value=<?php echo $verNombre; ?>>
				<input type="text" name="descripcion" placeholder="Descripción" style="color: #ffffff;" value=<?php echo $verDescripcion; ?>>
				<input type="text" name="precio" placeholder="Precio" style="color: #ffffff;" value=<?php echo $verPrecio; ?>>

				<select class="browser-default" name="disponibilidad" placeholder="Disponibilidad Coctel" style= "background-color:green; color: #ffffff;">
					<option value="D"<?php if($verDisponibilidad==$disponible){echo "selected='selected'";} ?>>Disponible</option>
					<option value="N"<?php if($verDisponibilidad==$no_disponible){echo "selected='selected'";} ?>>No Disponible</option>
				</select>
				<input type="hidden" name="url_coctel" value=<?php echo $verUrlCoctel; ?>>
				<input type="hidden" name="id_origen" value=<?php echo $verIdOrigen; ?>>
				<input type="hidden" name="descripcion_origen" value=<?php echo $verDescripcionOrigen; ?>>
				<input type="hidden" name="eliminado" value=<?php echo $verEliminado; ?>>

				<input type="hidden" name="comprobar_nombre" value=<?php echo $verNombre; ?>>
				<input type="hidden" name="comprobar_descripcion" value=<?php echo $verDescripcion; ?>>
				<input type="hidden" name="comprobar_descripcion_origen" value=<?php echo $verDescripcionOrigen; ?>>
				<br>
				<button class="waves-effect waves-light btn-small" type="submit" style="background-color:white; color:black;" href=""><b>Modificar</b></button>
			</form>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html>