<?php
session_start();
$varsession = $_SESSION['log'];
if ($varsession == null || $varsession == '') {
	echo '<script language="javascript" style="color: red;">alert("¡ Acceso no autorizado !");</script>';
	echo '<script>window.location.href="../../index.php";</script>';
}
?>

<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
	<!--Import estilo.css-->
	<link rel="stylesheet" href="../../css/estilo.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<!--Estilo para el diseño de la tabla-->
<style type="text/css">
table{
	background-color:#c8e6c9;
}

table, th, td{
	border:1px solid black;
	border-collapse:collapse;
}

th, td{
	padding:15px;
}

th{
	background-color: green;
	border-bottom: solid 5px #132230;
	color:white; 
	text-align:center;
}
</style>

<body>
	<!--Barra de Navegación-->
	<div class="navbar-fixed"> <!--Fijar menú mientras la despliegas-->
		<nav class="nav-extended" style="background-color:green;">
			<div class="nav-wrapper">
				<font size="5"><b>Bartender: Visualizar Pedido</b></font>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="bartender-gestion-pedido.php"><b>Volver</b></a></li>
					<li><a href="bartender.php"><b>Menu Principal</b></a></li>
					<li><a href="../cerrar-session.php"><b>Cerrar Sessión</b></a></li>
					<li><p>&nbsp;&nbsp;</p></li>
					<li><i class="large material-icons">people_outline</i></li>
					<li><i style="font-size:16px;"><?php echo $varsession;?></i></li>
					<li><p>&nbsp;&nbsp;&nbsp;</p></li>
				</ul>
			</div>
		</nav>
	</div>

	<ul class="sidenav" id="mobile-demo" style="background-color: green;">
		<li><a href="bartender-gestion-pedido.php" style="color:white;">Volver</a></li>
		<li><a href="bartender.php" style="color:white;">Menu Principal</a></li>
		<li><a href="../cerrar-session.php" style="color:white;"><b>Cerrar Sessión</b></a></li>
		<li><p>&nbsp;&nbsp;</p></li>
	</ul>
	<!--Fin Barra de Navegación-->
	<!--Inicio Contenedor1-->
	<div class="conten" align="center" vertical-align="bottom">
		<div class="container">
			<br>
			<!--Tipo y tamaño de letra-->
			<h5><font face="arial"><b style="background-color:white; color:black;">"En esta sección se podrá visualizar el detalle de cada pedido seleccionado, para luego dar clic cuando el pedido este listo"</b></font></h5>
			<br>
			<div align="left" style="width: 100%">
				<fieldset style="border-color: black; border-radius:10px 10px 10px 10px;">
					<legend><h6><font face="arial"><b style="background-color: white;color: black;">Detalle registro de pedido</b></font></h6></legend>
					<table border='1' style='width:100%;'>
						<tr>
							<th style='width:30%;'>Nombre Coctel</th>
							<td></td>
						</tr>
						<tr>
							<th>Descripción Coctel</th>
							<td></td>
						</tr>
						<tr>
							<th>Cantidad</th>
							<td></td>
						</tr>
						<tr>
							<th>Estado</th>
							<td></td>
						</tr>
						<tr>
							<th>Fecha</th>
							<td></td>
						</tr>
						<tr>
							<th>Número Mesa</th>
							<td></td>
						</tr>
						<tr>
							<td colspan="2"><div align="center"><button class="large material-icons" type="button" style="width:50%; background-color:green; border-color: black;" onclick="location.href=''">playlist_add_check</button></div></td>
						</tr>
					</table>
				</fieldset>
			</div>
		</div>
	</div>
	<!--Fin Contenedor-->

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../js/materialize.min.js"></script>
	<!-- Inicializando los componentes de materialize -->
	<script>
		document.addEventListener('DOMContentLoaded', function(){
			M.AutoInit();
		})
	</script>
</body>
</html> 