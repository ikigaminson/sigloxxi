<<?php

//Rescatando los datos
$id_coctel="1";
$nombre=$_POST['nombre'];
$descripcion=$_POST['descripcion'];
$precio=$_POST['precio'];
$disponibilidad=$_POST['disponibilidad'];
$url_coctel=$_POST['url_coctel'];
$id_origen="2";
$descripcion_origen=$_POST['descripcion'];
$eliminado="0";

//Declaración de Variables de Mensajes
$mensajeIdCoctel="";
$mensajeNombre="";
$mensajeDescripcion="";
$mensajePrecio="";
$mensajeDisponibilidad="";
$mensajeUrl="";
$mensajeIdOrigen="";
$mensajeDescripcionOrigen="";

//Valida que los campos no esten vacios
if (empty($id_coctel)) {	
	$mensajeIdCoctel=" *Id Coctel*";
}
if (empty($nombre)) {
	$mensajeNombre=" *Nombre*";
}
if (empty($descripcion)) {
	$mensajeDescripcion=" *Descripcion*";
}
if (empty($precio)) {
	$mensajePrecio=" *Precio*";
}
if (empty($disponibilidad)) {
	$mensajeDisponibilidad=" *Disponibilidad*";
}
if (empty($url_coctel)) {
	$mensajeUrl=" *Url*";
}
if (empty($id_origen)) {
	$mensajeIdOrigen=" *Id Origen*";
}
if (empty($descripcion_origen)) {
	$mensajeDescripcionOrigen=" *Descripcion Origen*";
}

//Válidando campos vacios
if (empty($id_coctel) || empty($nombre)|| empty($descripcion)|| empty($precio)|| empty($disponibilidad) || empty($url_coctel)|| empty($id_origen) || empty($descripcion_origen)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar'.$mensajeIdCoctel.$mensajeNombre.$mensajeDescripcion.$mensajePrecio.$mensajeDisponibilidad.$mensajeUrl.$mensajeIdOrigen.$mensajeDescripcionOrigen.'");</script>';
	echo '<script>window.location.href="bartender-gestion-coctel.php";</script>';
}else{

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/receta/insert';

	// Datos de consultas hechos en un array
	$data = array(
		'id_receta'         => $id_coctel,
		'nombre'         => $nombre,
		'descripcion'         => $descripcion,
		'precio'         => $precio,
		'disponibilidad'         => $disponibilidad,
		'url'         => $url_coctel,
		'id_origen'         => $id_origen,
		'eliminado'         => $eliminado
	);

	//var_dump($data);
	
	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);

	//var_dump($parse_result);

	$validado=$parse_result['result'];

	//Válida que si el coctel se modifico
	if ($validado==1) {
		//Muestra mensaje el coctel que se agrego exitosamente*****
		echo '<script language="javascript" style="color: red;">alert("Coctel agregado exitosamente");</script>';
		echo '<script>window.location.href="bartender-gestion-coctel.php";</script>';
	}else{
		//Muestra mensaje al usuario que no se agrego el coctel*****
		echo '<script language="javascript" style="color: red;">alert("¡ No se pudo agregar el coctel !");</script>';
		echo '<script>window.location.href="bartender-gestion-coctel.php";</script>';
	}	
}
?>