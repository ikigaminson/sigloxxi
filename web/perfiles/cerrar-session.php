<?php
//Inicia Session
session_start();
//Destruye la session existente
session_destroy();
//Redirecciona al index
header("Location:../index.php");
?>