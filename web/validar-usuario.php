<?php

//Rescatar los datos de las variables
$usuario = $_POST["usuario"];
$pass = $_POST["clave"];

//Inicia una sesion
session_start();
//Define la session usuario con la variable recibida del formulario
$_SESSION['log'] = $usuario;

//Valida que los campos no esten vacios
if (empty($usuario) && empty($pass)) {	
	echo '<script language="javascript" style="color: red;">alert("Debe agregar su usuario y contraseña");</script>';
	echo '<script>window.location.href="index.php";</script>';
}elseif (empty($usuario)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar su usuario");</script>';
	echo '<script>window.location.href="index.php";</script>';
}elseif (empty($pass)) {
	echo '<script language="javascript" style="color: red;">alert("Debe agregar su contraseña");</script>';
	echo '<script>window.location.href="index.php";</script>';
}else{

	$clave = hash("sha256",$pass);

	//Url al cual le hacemos una consulta
	$url = 'localhost:4567/usuario/login';

	// Datos de consultas hechos en un array
	$data = array(
		'user'         => $usuario,
		'pass'         => $clave
	);

	//Transformacion del array a un archivo json 
	$fields_string = json_encode($data);

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Parsear la data a array
	$parse_result = json_decode($result, true);
	$id= $parse_result['id'];

	//var_dump($result);

	//Valida que el usuario 
	if ($id>0) 
	{
		//The url you wish to send the POST request to
		$url = 'localhost:4567/usuario/get?id='.$id;

		//Abrir conexión
		$ch = curl_init($url);

		//Editar post
		curl_setopt($ch,CURLOPT_POST, true);

		//Establecer el tipo de contenido en application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el post
		$result = curl_exec($ch);

		//Parsea la Data a Array
		$user = json_decode($result, true);

		//Convierte en variables
		$id_tipo_persona = $user['persona']['tipoPersona']['id_tipo_persona'];

		//Valido los perfiles
		if ($id_tipo_persona == 1) {
			echo ('</br>');
			?>
			<script>window.location="perfiles/administrador/admin.php"</script><?php
		}elseif ($id_tipo_persona == 2) {
			echo ('</br>');
			?>
			<script>window.location="perfiles/bodeguero/bodeguero.php"</script><?php
		}elseif ($id_tipo_persona == 3) {
			echo ('</br>');
			?>
			<script>window.location="perfiles/cajero/cajero.php"</script><?php
		}elseif ($id_tipo_persona == 4) {
			echo ('</br>');
			?>
			<script>window.location="perfiles/cocinero/cocinero.php"</script><?php
		}elseif ($id_tipo_persona == 5) {
			echo ('</br>');
			?>
			<script>window.location="perfiles/bartender/bartender.php"</script><?php
		}elseif ($id_tipo_persona == 6) {
			echo ('</br>');
			?>
			<script>window.location="perfiles/garzon/garzon.php"</script><?php
		}else{
			echo ('</br>');
			echo '<script language="javascript" style="color: red;">alert("Usuario sin Perfil Administrativo");</script>';
			echo '<script>window.location.href="index.php";</script>';
		}
	}
	elseif ($id<0) 
	{
		echo '<script language="javascript" style="color: red;">alert("Su usuario y/o contraseña son incorrectas");</script>';
		echo '<script>window.location.href="index.php";</script>';
	}
}

?>

