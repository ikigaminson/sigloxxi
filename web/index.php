<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
  <link rel="stylesheet" href="css/estiloindex.css">
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="background-image: url(img/fondoparaindex.jpg); background-size: cover; background-repeat: no-repeat; margin: 0; min-height:100vh; max-width: 100%;">
  <form method="post" action="validar-usuario.php" name="login_formulario">
    <img src="img/logo.jpg" align="center" size="100px" style="margin: 30px; width: 90%; max-width: 560px;">
    <h4 style="color: white;">Ingrese su Usuario</h4>
    <input type="text" placeholder="&#128272; Usuario" name="usuario" id="txt_usuario" style="color:white;">
    <input type="password" placeholder="&#128272; Contraseña" name="clave" id="txt_clave" style="color:white;">
    <br>
    <input type="submit" value="Iniciar Sesión">
  </form> 
  <!--JavaScript at end of body for optimized loading-->
  <script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>
