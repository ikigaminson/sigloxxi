<?php

 use PHPUnit\Framework\TestCase;

 final class Pruebalistar extends TestCase {

 		/** @test **/

 		 public function listaOrdenCompraInsumo(){

    	//Url al cual le hacemos una consulta

		$url = 'localhost:4567/ordenCompraInsumo/list';

		// Crear un nuevo recurso "cURL" 

		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST

		curl_setopt($ch,CURLOPT_POST, true);

		//Establecer el tipo de contenido en application/json

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él

		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo

		$result = curl_exec($ch);

		//Transforma el resultado json en array

		$array = json_decode($result, true);

		//var_dump($array);	
        
        //Verifica que el listado no este vacio
		$this->assertNotEmpty($array);


    }


    	/** @test **/
    	public function listarUsuario(){

    	//Url al cual le hacemos una consulta
		$url = 'localhost:4567/trabajador/list';

		// Crear un nuevo recurso "cURL" 
		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
		curl_setopt($ch,CURLOPT_POST, true);

		//Establecer el tipo de contenido en application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
		$result = curl_exec($ch);

		//Transforma el resultado json en array
		$datos = json_decode($result, true);

		//var_dump($result);

		//Verifica que el listado no este vacio
		$this->assertNotEmpty($datos);
    }

        /** @test **/  
        public function listarProveedor(){

        //Url al cual le hacemos una consulta
		$url = 'localhost:4567/proveedor/list';

		// Crear un nuevo recurso "cURL" 
		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
		curl_setopt($ch,CURLOPT_POST, true);

		//Establecer el tipo de contenido en application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
		$result = curl_exec($ch);

		//Transforma el resultado json en array
		$datos = json_decode($result, true);


		//Verifica que el listado no este vacio
		$this->assertNotEmpty($datos);

        }




    /** @test **/

    public function listarClientes(){

    	//Url al cual le hacemos una consulta
		$url = 'localhost:4567/cliente/list';

		// Crear un nuevo recurso "cURL" 
		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
		curl_setopt($ch,CURLOPT_POST, true);

		//Establecer el tipo de contenido en application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
		$result = curl_exec($ch);

		//Transforma el resultado json en array
		$datos = json_decode($result, true);

		//Verifica que el listado no este vacio
		$this->assertNotEmpty($datos);

    }

    /** @test **/

    public function listarInsumos(){
    
    //Url al cual le hacemos una consulta
	$url = 'localhost:4567/insumos/list';

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//0Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Transforma el resultado json en array
	$datos = json_decode($result, true);
    
    //Verifica que el listado no este vacio
	$this->assertNotEmpty($datos);

    }

    /** @test **/

    public function listarMesas(){
    
    //Url al cual le hacemos una consulta
	$url = 'localhost:4567/mesas/list';

	// Crear un nuevo recurso "cURL" 
	$ch = curl_init($url);

	//Establecer número de variables POST, datos POST
	curl_setopt($ch,CURLOPT_POST, true);

	//Establecer el tipo de contenido en application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

	//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

	//Ejecuta el posteo
	$result = curl_exec($ch);

	//Transforma el resultado json en array
	$datos = json_decode($result, true);

	//Verifica que el listado no este vacio
	$this->assertNotEmpty($datos);

    }


              /** @test **/

            public function listarRecetas(){
            	
             //Url al cual le hacemos una consulta
			$url = 'localhost:4567/recetas/list';

			// Crear un nuevo recurso "cURL" 
			$ch = curl_init($url);

			//Establecer número de variables POST, datos POST
			curl_setopt($ch,CURLOPT_POST, true);

			//Establecer el tipo de contenido en application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

			//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

			//Ejecuta el posteo
			$result = curl_exec($ch);

			//Transforma el resultado json en array
			$datos = json_decode($result, true);

			//Verifica que el listado no este vacio
			$this->assertNotEmpty($datos);
		}


		/** @tesst **/

        public function tipoPersona(){
        
         //Url al cual le hacemos una consulta
        $url = 'localhost:4567/usuario/getTipos';

        // Crear un nuevo recurso "cURL" 
		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
		curl_setopt($ch,CURLOPT_POST, true);

		//Establecer el tipo de contenido en application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
		$result = curl_exec($ch);

		//Transforma el resultado json en array
		$datos = json_decode($result, true);

		//Verifica que el listado no este vacio
		$this->assertNotEmpty($datos);

        }


        /** @test **/

        public function lisTrabajador(){
         //Url al cual le hacemos una consulta
        $url = 'localhost:4567/trabajador/list';

        // Crear un nuevo recurso "cURL" 
		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
		curl_setopt($ch,CURLOPT_POST, true);

		//Establecer el tipo de contenido en application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
		$result = curl_exec($ch);

		//Transforma el resultado json en array
		$datos = json_decode($result, true);

		//Verifica que el listado no este vacio
		$this->assertNotEmpty($datos);

        }

}