<?php

 use PHPUnit\Framework\TestCase;

 final class PruebaTest extends TestCase {

	/** @test **/
	
    public function recetaInsumo(){


    //Url al cual le hacemos una consulta
				$url = 'localhost:4567/recetaInsumo/insert';

				// Datos de consultas hechos en un array
				$data = array(
					"cantidad" => 4,
    				"id_insumo" => 8,
    				"id_receta" => 1,
   					"id_medida" => 5
				);

				//var_dump($data);

				
				//Transformacion del array a un archivo json 
				$fields_string = json_encode($data);

				// Crear un nuevo recurso "cURL" 
				$ch = curl_init($url);

				//Establecer número de variables POST, datos POST
				curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

				//Establecer el tipo de contenido en application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

				//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

				//Ejecuta el posteo
				$result = curl_exec($ch);

				//Parsear la data a array
				$parse_result = json_decode($result, true);
				
				//var_dump($parse_result);

				$validado=print ($parse_result);

				//Verifica que el listado no este vacio
				$this->assertNotEmpty($data);
			}



   				 /** @test **/

   				public function modificarUsuario(){

   				 //Url al cual le hacemos una consulta
				$url = 'localhost:4567/usuario/update';

				// Datos de consultas hechos en un array
				$data = array(
					'id_persona'         => 4,
					'rut'         => 14459449,
					'dv'         => "5",
					'nombre'         => "felix",
					'paterno'         => "denver",
					'materno'         => "atrabis",
					'mail'         => "felix@gmail.com",
					'idTipo'         => 4
				);

				//var_dump($data);

				
				//Transformacion del array a un archivo json 
				$fields_string = json_encode($data);

				// Crear un nuevo recurso "cURL" 
				$ch = curl_init($url);

				//Establecer número de variables POST, datos POST
				curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

				//Establecer el tipo de contenido en application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

				//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

				//Ejecuta el posteo
				$result = curl_exec($ch);

				//Parsear la data a array
				$parse_result = json_decode($result, true);
				
				//var_dump($parse_result);

				$validado=print ($parse_result);

				//Verifica que el listado no este vacio
				$this->assertNotEmpty($data);

    }         

              /** @test **/

              public function usuarioLogin(){

              
			//Url al cual le hacemos una consulta
			$url = 'localhost:4567/usuario/login';

			// Datos de consultas hechos en un array
			$data = array(
			'user'         => 'id',
			'pass'         => 'pass123'
			);

			//Transformacion del array a un archivo json 
			$fields_string = json_encode($data);

			// Crear un nuevo recurso "cURL" 
			$ch = curl_init($url);

			//Establecer número de variables POST, datos POST
			curl_setopt($ch,CURLOPT_POST, true);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

			//Establecer el tipo de contenido en application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

			//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

			//Ejecuta el posteo
			$result = curl_exec($ch);

			//Verifica que el listado no este vacio
			$this->assertNotEmpty($data);

          }
          

          /** @test **/

 		public function recetaDelete(){


 		//Url al cual le pedimos el delete

		$url = 'localhost:4567/receta/delete';

		// Datos enviados en un array
		$data = array(
		'id_receta'        => '4'
	);

		//Transformacion del array a un archivo json 
		$fields_string = json_encode($data);

		// Crear un nuevo recurso "cURL" 
		$ch = curl_init($url);

		//Establecer número de variables POST, datos POST
		curl_setopt($ch,CURLOPT_POST, true);

		//Le introducimos la data a la consulta
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

		//Establecer el tipo de contenido en application/json

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		//Para que curl_exec devuelva el contenido de la cURL; en lugar de hacerse eco de él
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

		//Ejecuta el posteo
		$result = curl_exec($ch);

		//Parsear la data a array
		$parse_result = json_decode($result, true);


		$this->assertSame('1', $parse_result["result"]);
		
        }

   

}