package Utils;

import oracle.jdbc.internal.OracleTypes;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.*;
import java.util.ArrayList;

public class DB {
    private static Connection con = null;

    // Query to get data from DB (SELECT, FUNCTIONS)
    public static CachedRowSet query(String query){
        try{
            //init db connection
            init();

            //create the statement object
            Statement stmt = con.createStatement();;

            //execute query
            ResultSet rs = stmt.executeQuery(query);

            //Create a disconnected object to handle results through components
            //ResultSet need a open connection, while CachedRowSet no
            RowSetFactory factory = RowSetProvider.newFactory();
            CachedRowSet crs = factory.createCachedRowSet();
            crs.populate(rs);

            //close the connection and ResultSet object
            con.close();
            rs.close();

            return crs;

        }catch (Exception e){
            System.out.println("DB Query Error: " + e);
            return null;
        }
    }

    // Query to get modificated rows (INSERT, UPDATE, DELETE)
    public boolean rowQuery(String query, ArrayList data){
        try{
            // Init db connection
            init();

            // To get rows numbers, need use this statement
            // The query should have a '?' in the values you want to edit/insert/whatever
            PreparedStatement preparedStatement = con.prepareStatement(query);
            //System.out.println(query);

            // Add params to the statement, the long depends of the '?' at query
            // If it's more of less than the required, throw a exception at executeUpdate()
            for (int i = 0; i < data.size(); i++ ){
                preparedStatement.setString(i+1, data.get(i).toString());
            }
            //System.out.printf(preparedStatement.toString());

            /* Here the query it's send to DB and been executed. Returns the number of
             * updated rows.
             * Check if modificated rows are > 0. If you want, can get int */
            boolean result = preparedStatement.executeUpdate() > 0;

            // Close connection
            con.close();

            return  result;
        }catch (Exception e){
            System.out.println("Error at modicated query: " + e);
            return false;
        }
    }

    // This method handle the int from database like string 'cuz I like that way.
    // You can always change this :D
    // Maybe later I will regret this xD
    public String getProceduresWithInt(String query, ArrayList data){
        try{
            init();

            CallableStatement cs = con.prepareCall(query);
            int arraySize = data.size();

            for (int i = 0; i < arraySize; i++ ){
                cs.setString(i+1, data.get(i).toString());
            }

            // The next value after the array size, always will be the output parameter
            // this 'cuz, this method handle the int
            // Even if you had a 0 size array, the first values is a int
            cs.registerOutParameter(arraySize+1, OracleTypes.NUMBER);

            // Execute the sql command
            cs.execute();

            String result = cs.getString(arraySize + 1 );

            // Close connection
            con.close();

            return result;
        }catch (Exception e){
            System.out.println("Exception at Procedure int method: " + e.getMessage());
            return null;
        }
    }

    public CachedRowSet getProceduresWithCursor(String query, ArrayList data){
        try{
            //Init database connection
            init();

            // Prepare the statement to call a procedure
            CallableStatement cs = con.prepareCall(query);
            int arraySize = data.size(); // If 0, no extra parameters

            //If data > 0, fills parameters in the sql statement
            for (int i = 0; i < arraySize; i++ ){
                cs.setString(i+1, data.get(i).toString());
            }

            // The next value after the array size, always will be the output parameter
            // this 'cuz, this method handle the cursor
            // Even if you had a 0 size array, the first values is a cursor
            cs.registerOutParameter(arraySize+1, OracleTypes.CURSOR);

            // Execute the sql command
            cs.execute();

            // I'm not sure how works, 'cuz I don't know if I should expect 1 as the
            // return or, the las element of the procedure. If something go wrong with
            // more parameters than 1, this it's the problem.
            ResultSet rs = (ResultSet) cs.getObject(arraySize + 1);

            // Create a CacheRowSet to handle data without the connection
            RowSetFactory factory = RowSetProvider.newFactory();
            CachedRowSet crs = factory.createCachedRowSet();

            // Fills the CacheRowSet with the ResultSet Data
            crs.populate(rs);

            // Close connection
            con.close();

            return crs;
        }catch (Exception e){
            System.out.println("Exception at Procedure row method: " + e.getMessage());
            return null;
        }
    }

    // Crea multiples inserts
    public static String multipleInsertPreorden(String query, String id, ArrayList list1, ArrayList list2){

        try{
            init();

            PreparedStatement ps = con.prepareStatement(query);

            for (int i = 0; i < list1.size(); i++ ){
                ps.setString(1, list2.get(i).toString());
                ps.setString(2, id);
                ps.setString(3, list1.get(i).toString());
                ps.addBatch();
            }

            // Execute the sql command
            int[] inserted = ps.executeBatch();
            String result = "1";

            if(result.equals("-1")){
                System.out.println("Nos fuimos a la verga wey :'v");
            }

            // Close connection
            con.close();

            return result;
        }catch (Exception e){
            System.out.println("Exception at multiple insert preorden: " + e.getMessage());
            return "-1";
        }
    }

    // For debug purposes
    public static StringBuilder debugTableSelect(String query){
        CachedRowSet rs = query(query);
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("<table>\n<tr>\n");
            //Get columns name
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                sb.append("\t<th>" + rs.getMetaData().getColumnName(i) + "</th>\n");
            }
            sb.append("</tr>\n");

            //Get the row
            System.out.println(rs.getRow());
            while (rs.next()) {
                //Get columns data
                sb.append("<tr>\n");
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    sb.append("\t<td>" + rs.getString(i) + "</td>\n");
                }
                sb.append("</tr>\n");
            }
            sb.append("</table>\n");

            rs.close();

            // Close connection
            con.close();

            ///System.out.println(sb.toString());
            return sb;
        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }

    // Initialize the connection, but for the class use
   private static boolean init(){
            try{
            //step1 load the driver class
            Class.forName("oracle.jdbc.driver.OracleDriver");

            //step2 create  the connection object
            //jdbc:oracle:thin:@192.168.47.130:1521:orcl
            con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@sigloxxi.cl7zln5rik06.us-east-1.rds.amazonaws.com:1521:orcl","sigloxxi_admin","pass123");
            return true;

        }catch (Exception e){
            System.out.println("DB Init error: " + e);
            return false;
        }
    }
}
