import Services.BusinessService;
import Services.MovilService;
import Services.TrabajadorService;
import Services.UsuarioService;

import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
        get("/", (req, res) -> "Server running... ");

        UsuarioService us = new UsuarioService();
        TrabajadorService ts = new TrabajadorService();
        BusinessService bs = new BusinessService();
        MovilService ms = new MovilService();

        us.init();
        ts.init();
        bs.init();
        ms.init();

        /*get("/", (request, res) ->
                Utils.DB.debugTableSelect("select * from proveedor")
        );*/



    }
}
