package Controllers.Personas;

import Models.Personas.TipoPersona;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class TipoPersonaController {
    private TipoPersona tipoPersona;
    private static DB db = new DB();

    //Tipo persona
    @SneakyThrows
    public static String getTipos(){
        String json = "";
        String sql = "SELECT id_tipo_persona id, descripcion tipo FROM tipo_persona";
        ArrayList<TipoPersona> personaList = new ArrayList<>();

        CachedRowSet rs = db.query(sql);

        try {
            while (rs.next()){
                int id = Integer.parseInt(rs.getString("ID"));
                String tipo = rs.getString("tipo");
                TipoPersona tp = new TipoPersona();

                tp.setId_tipo_persona(id);
                tp.setDescripcion(tipo);

                personaList.add(tp);
            }
        }catch (Exception e){
            System.out.println("Error at Login: " + e);
        }
        json = new Gson().toJson(personaList);

        return json;
    }
}
