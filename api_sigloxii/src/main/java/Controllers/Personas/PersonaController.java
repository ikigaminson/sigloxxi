package Controllers.Personas;

import Models.Personas.Persona;
import Utils.DB;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;


public class PersonaController {
    private Persona persona;
    private static DB db = new DB();

    @SneakyThrows
    public static int insertPersona(String rut, String dv, String nombre, String paterno, String materno, String mail, String idTipo){
        ArrayList<String> data = new ArrayList<>();
        String sql = "INSERT INTO persona(rut, digitoVerificador, nombre, apelPat, apelMat, email, TIPO_PERSONA_id_tipo_persona, eliminado) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, 0)";

        data.add(rut);
        data.add(dv);
        data.add(nombre);
        data.add(paterno);
        data.add(materno);
        data.add(mail);
        data.add(idTipo);

        if(db.rowQuery(sql, data)){
            sql = "select max(id_persona) last_id from persona";
            CachedRowSet rs = db.query(sql);

            while (rs.next()){
                int last_id = Integer.parseInt(rs.getString("last_id"));
                return last_id;
            }
        }
        System.out.println("Error ingreso Persona");
        return -1;
    }

    @SneakyThrows
    public static String updatePersona(String id_persona, String rut, String dv, String nombre, String paterno, String materno, String mail, String idTipo){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_updatePersona(?,?,?,?,?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_persona);
        data.add(rut);
        data.add(dv);
        data.add(nombre);
        data.add(paterno);
        data.add(materno);
        data.add(mail);
        data.add(idTipo);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete persona
    public static String deletePersona(String id_persona){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETEPERSONA(?, ?)}";
        data.add(id_persona);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }
}
