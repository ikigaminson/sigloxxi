package Controllers.Personas;

import Models.Personas.Persona;
import Models.Personas.TipoPersona;
import Models.Personas.Trabajador;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class TrabajadorController {
    private Trabajador trabajador;
    private static DB db = new DB();

	//Login
    @SneakyThrows
    public static int login(String user, String pass){
        // Obtenemos el id trabajador para usarlo con el /usuario/get
        String sql = "SELECT id_trabajador id FROM trabajador " +
                "WHERE usuario = '" + user + "' "+
                "AND clave = '" + pass + "'";

        //System.out.println(sql);
        CachedRowSet rs = db.query(sql);

        try {
            while (rs.next()){
                String id = rs.getString("ID");
                return Integer.parseInt(id);
            }
        }catch (Exception e){
            System.out.println("Error at Login: " + e);
        }

        return -1;
    }

    // Get all transversal data about the trabajador
	@SneakyThrows
    public static String getTrabajador(String id){
        String sql = "SELECT " +
                "t.id_trabajador id_trabajador," +
                "p.nombre nombre," +
                "p.id_persona id_persona," +
                "p.apelpat paterno," +
                "p.apelmat materno," +
                "p.email email, " +
                "p.tipo_persona_id_tipo_persona id_cargo " +
                "FROM persona p " +
                "JOIN trabajador t " +
                "ON (t.persona_id_persona = p.id_persona) " +
                "WHERE t.id_trabajador = " + id;

        //System.out.println(sql);
        CachedRowSet rs = db.query(sql);
        Trabajador trabajador = new Trabajador();
        trabajador.setPersona(new Persona());
        trabajador.getPersona().setTipoPersona(new TipoPersona());
        String json = "";

        try{
            while (rs.next()){
                int id_trabajador = Integer.parseInt(rs.getString("id_trabajador"));
                int id_persona = Integer.parseInt(rs.getString("id_persona"));
                String nombre = rs.getString("nombre");
                String paterno = rs.getString("paterno");
                String materno = rs.getString("materno");
                String email = rs.getString("email");
                int id_cargo = Integer.parseInt(rs.getString("id_cargo"));

                trabajador.setId_trabajador(id_trabajador);
                trabajador.getPersona().setId_persona(id_persona);
                trabajador.getPersona().setNombre(nombre);
                trabajador.getPersona().setPaterno(paterno);
                trabajador.getPersona().setMaterno(materno);
                trabajador.getPersona().setEmail(email);
                trabajador.getPersona().getTipoPersona().setId_tipo_persona(id_cargo);

                Gson gson = new Gson();
                json = gson.toJson(trabajador);

                break;
            }
        }catch (Exception e){
            System.out.println(e);
        }
        return json;
    }

	//Insert trabajador
    @SneakyThrows
    public static String insertTrabajador(String id_trabajador,String usuario, String clave, String persona_id_persona, String eliminado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_TRABAJADOR(?,?,?,?,?,?)}";

        data.add(id_trabajador);
        data.add(usuario);
        data.add(clave);
        data.add(persona_id_persona);
        data.add(eliminado);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

	//Update password
    @SneakyThrows
    public static boolean updatePassword(int id_trabajador, String password){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_updatePassword(?, ?, ?)}";
        return false;

        //TODO: In process
    }

    //Listar trabajador
    @SneakyThrows
    public static String listTrabajadores(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Trabajador> list = new ArrayList<>();
        String sql = "{call sp_getTrabajadores(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        Trabajador trabajador;
        String json = "";

        try{
            while (rs.next()){
                trabajador = new Trabajador();
                trabajador.setPersona(new Persona());
                trabajador.getPersona().setTipoPersona(new TipoPersona());

                int id_trabajador = Integer.parseInt(rs.getString("id_trabajador"));
                int id_persona = Integer.parseInt(rs.getString("id_persona"));
                int rut = Integer.parseInt(rs.getString("rut"));
                String dv = rs.getString("dv");;
                String nombre = rs.getString("name");
                String paterno = rs.getString("paterno");
                String materno = rs.getString("materno");
                String email = rs.getString("email");
                int id_cargo = Integer.parseInt(rs.getString("id_tipoPersona"));
                String descripcionCargo = rs.getString("tipoNombre");

                trabajador.setId_trabajador(id_trabajador);
                trabajador.getPersona().setId_persona(id_persona);
                trabajador.getPersona().setRut(rut);
                trabajador.getPersona().setDv(dv);
                trabajador.getPersona().setNombre(nombre);
                trabajador.getPersona().setPaterno(paterno);
                trabajador.getPersona().setMaterno(materno);
                trabajador.getPersona().setEmail(email);
                trabajador.getPersona().getTipoPersona().setId_tipo_persona(id_cargo);
                trabajador.getPersona().getTipoPersona().setDescripcion(descripcionCargo);

                list.add(trabajador);
             }

            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            System.out.println(e);
        }
        return json;
    }

    @SneakyThrows
    //Delete trabajador
    public static String deleteTrabajador(String id_trabajador){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_TRABAJADOR(?, ?)}";
        data.add(id_trabajador);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }


}


