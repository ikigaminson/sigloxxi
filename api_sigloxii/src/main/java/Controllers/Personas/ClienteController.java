package Controllers.Personas;

import Models.Negocio.Origen;
import Models.Negocio.Receta;
import Models.Personas.Persona;
import Models.Personas.TipoPersona;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class ClienteController {
    private static Persona cliente;
    private static DB db = new DB();

    // List all clients
	@SneakyThrows
    public static String getClients(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Persona> list = new ArrayList<>();
        String sql = "{call sp_getClients(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                int id_cliente = Integer.parseInt(rs.getString("id_persona"));
                String nombre = rs.getString("name");
                String paterno = rs.getString("paterno");
                String materno = rs.getString("materno");
                int rut = Integer.parseInt(rs.getString("rut"));
                String dv = rs.getString("dv");
                String email = rs.getString("email");
                int id_tipoPersona = Integer.parseInt(rs.getString("id_tipoPersona"));
                String descripcion = rs.getString("tipoNombre");

                TipoPersona tipoPersona = new TipoPersona(id_tipoPersona, descripcion);
                // FIX: Todavía no cargo la actualización a la db, así que queda pendiente
                cliente = new Persona(id_cliente, rut, dv, nombre, paterno, materno, email, tipoPersona);

                list.add(cliente);
            }
            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            System.out.println(e);
        }
        return json;
    }
}
