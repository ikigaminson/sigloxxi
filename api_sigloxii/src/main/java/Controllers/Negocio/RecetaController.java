package Controllers.Negocio;

import Models.Negocio.*;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class RecetaController {
    private static Receta receta;
    private static DB db = new DB();

    //List all recetas
	@SneakyThrows
    public static String getRecetas(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Receta> list = new ArrayList<>();
        String sql = "{call sp_getRecetas(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                Origen o = new Origen();

                int id_receta = Integer.parseInt(rs.getString("id_receta"));
                String nombre = rs.getString("nombre");
                String descripcion = rs.getString("descripcion");
                int precio = Integer.parseInt(rs.getString("precio"));
                String url = rs.getString("url");
                String disponibilidad = rs.getString("disponibilidad");
                String origen = rs.getString("origen");
                int id_origen = Integer.parseInt(rs.getString("id_origen"));

                o.setId_origen(id_origen);
                o.setDescripcion(origen);
                receta = new Receta(id_receta, nombre, descripcion, precio, disponibilidad, url, o, 0);

                list.add(receta);
            }
            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            System.out.println(e);
        }
        return json;
    }

    //Insertar receta
    @SneakyThrows
    public static String insertReceta(String id_receta, String nombre, String descripcion, String precio, String disponibilidad, String url, String origen_id_origen, String eliminado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_RECETA(?,?,?,?,?,?,?,?,?)}";

        data.add(id_receta);
        data.add(nombre);
        data.add(descripcion);
        data.add(precio);
        data.add(disponibilidad);
        data.add(url);
        data.add(origen_id_origen);
        data.add(eliminado);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Update receta
    @SneakyThrows
    public static String updateReceta(String id_receta, String nombre, String descripcion, String precio,
                                      String disponibilidad, String url, String origen_id_origen, String eliminado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Receta(?,?,?,?,?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_receta);
        data.add(nombre);
        data.add(descripcion);
        data.add(precio);
        data.add(disponibilidad);
        data.add(url);
        data.add(origen_id_origen);
        data.add(eliminado);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete receta
    @SneakyThrows
    public static String deleteReceta(String id_receta){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_RECETA(?, ?)}";
        data.add(id_receta);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Insertar receta insumo
    @SneakyThrows
    public static String insertRecetaInsumo(String cantidad, String insumo_id_insumo, String receta_id_receta,
                                            String medida_id_medida){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_RECETA_INSUMO(?,?,?,?,?)}";

        data.add(cantidad);
        data.add(insumo_id_insumo);
        data.add(receta_id_receta);
        data.add(medida_id_medida);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Listar receta insumo
    @SneakyThrows
    public static String getListRecetasInsumos(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Receta_insumo> list = new ArrayList<>();
        String sql = "{call sp_get_Receta_Insumo(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                float cantidad = rs.getFloat("cantidad");
                int insumo_id_insumo = Integer.parseInt(rs.getString("insumo_id_insumo"));
                int receta_id_receta = Integer.parseInt(rs.getString("receta_id_receta"));
                int medida_id_medida = Integer.parseInt(rs.getString("medida_id_medida"));

                Insumo insumo = new Insumo();
                Receta receta = new Receta();
                Medida medida = new Medida();

                insumo.setId_insumo(insumo_id_insumo);
                receta.setId_receta(receta_id_receta);
                medida.setId_medida(medida_id_medida);
                Receta_insumo recetaInsumos = new Receta_insumo(cantidad,insumo,receta,medida);

                list.add(recetaInsumos);
            }
            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            System.out.println(e);
        }
        return json;
    }

    //Update receta insumo
    @SneakyThrows
    public static String updateRecetaInsumo(String cantidad, String insumo_id_insumo, String receta_id_receta,
                                            String medida_id_medida){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Receta_Insumo(?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(cantidad);
        data.add(insumo_id_insumo);
        data.add(receta_id_receta);
        data.add(medida_id_medida);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete receta insumo
    @SneakyThrows
    public static String deleteRecetaInsumo(String insumo_id_insumo,String receta_id_receta,String medida_id_medida){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_RECETA_INSUMO(?,?,?,?)}";
        data.add(insumo_id_insumo);
        data.add(receta_id_receta);
        data.add(medida_id_medida);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

}
