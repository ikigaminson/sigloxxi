package Controllers.Negocio;

import Models.Negocio.Comensal;
import Models.Negocio.Pedido;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PedidoController {
    private Pedido pedido;
    private static DB db = new DB();

    //Insertar pedido
    @SneakyThrows
    public static String insertPedido(String id_pedido, String estado, String comensal_id_comensal, String validado) {
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_PEDIDO(?,?,?,?,?)}";

        data.add(id_pedido);
        data.add(estado);
        data.add(comensal_id_comensal);
        data.add(validado);

        String result = db.getProceduresWithInt(sql, data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Listar pedido
    @SneakyThrows
    public static String getListPedido() {
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Pedido> list = new ArrayList<>();
        String sql = "{call sp_getPedido(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";

        try {
            while (rs.next()) {
                int id_pedido = Integer.parseInt(rs.getString("id_pedido"));
                String estado = rs.getString("estado");
                int comensal_id_comensal = Integer.parseInt(rs.getString("comensal_id_comensal"));
                String validado = rs.getString("validado");

                Comensal comensal = new Comensal();

                comensal.setId_comensal(comensal_id_comensal);
                Pedido pedidos = new Pedido(id_pedido, estado, comensal, validado);

                list.add(pedidos);
            }

            Gson gson = new Gson();
            json = gson.toJson(list);
        } catch (Exception e) {
            json = e.getMessage();
            //System.out.println(e);
        }

        return json;
    }

    //Update pedido
    @SneakyThrows
    public static String updatePedido(String id_pedido, String estado, String comensal_id_comensal, String validado) {
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Pedido(?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_pedido);
        data.add(estado);
        data.add(comensal_id_comensal);
        data.add(validado);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result", result);

        return resp.toString();
    }

    //Delete pedido
    @SneakyThrows
    public static String deletePedido(String id_pedido) {
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_DELETE_PEDIDO(?, ?)}";
        data.add(id_pedido);
        String result = db.getProceduresWithInt(sql, data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    // Genera preorden
    @SneakyThrows
    public static String createPreOrden(String id_comensal, ArrayList platos, ArrayList cantidad) {
        String id_pedido = createPedido(id_comensal);
        String result = "-1";

        if (!id_pedido.equals("")){
            String sql = "INSERT INTO SIGLOXXI_ADMIN.ORDEN (CANTIDAD, PEDIDO_ID_PEDIDO, RECETA_ID_RECETA) " +
                    "VALUES(?, ?, ?)";

            result = db.multipleInsertPreorden(sql, id_pedido, platos, cantidad);
        }

        JSONObject resp = new JSONObject();
        resp.put("result", result);

        return resp.toString();
    }

    @SneakyThrows
    private static String createPedido(String id_comensal){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_PEDIDO(?,?,?,?,?)}";

        data.add("0");
        data.add("0");
        data.add(id_comensal);
        data.add("0");

        if(!db.getProceduresWithInt(sql,data).equals("-1")){
            sql = "select max(ID_PEDIDO) last_id from PEDIDO";
            CachedRowSet rs = db.query(sql);

            while (rs.next()){
                String last_id = rs.getString("last_id");
                return last_id;
            }
        }

        return "";
    }

    // Obtiene los comensales activos con el monto gastado
    @SneakyThrows
    public static String getComensalesActivos() {
        ArrayList<String> data = new ArrayList<>();
        List<Map<String, String>> listaActivos = new ArrayList<>();

        String sql = "{call SP_GET_COMENSALES_ACTIVOS(?)}";

        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        while (rs.next()){
            Map<String, String> activo = new HashMap<>();

            activo.put("id_comensal",rs.getString("ID_COMENSAL" ));
            activo.put("id_mesa",rs.getString("id_mesa" ));
            activo.put("numero_mesa",rs.getString("numero_mesa" ));
            activo.put("total",rs.getString("total" ));

            listaActivos.add(activo);
        }

        String json = new Gson().toJson(listaActivos);
        return json;
    }

    // Obtiene el detalle de los comensales
    @SneakyThrows
    public static String getComensalesDetalles(String id_comensal) {
        ArrayList<String> data = new ArrayList<>();
        List<Map<String, String>> listaDetalles = new ArrayList<>();
        String sql = "{call SP_GET_PEDIDOS(?, ?)}";

        data.add(id_comensal);

        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        while (rs.next()){
            Map<String, String> activo = new HashMap<>();

            activo.put("ID_COMENSAL",rs.getString("ID_COMENSAL"));
            activo.put("id_mesa",rs.getString("id_mesa"));
            activo.put("numero_mesa",rs.getString("numero_mesa"));
            activo.put("id_pedido",rs.getString("id_pedido"));
            activo.put("validado",rs.getString("validado"));
            activo.put("nombre_receta",rs.getString("nombre_receta"));
            activo.put("cantidad",rs.getString("cantidad"));
            activo.put("precio",rs.getString("precio"));
            activo.put("total",rs.getString("total"));

            listaDetalles.add(activo);
        }

        String json = new Gson().toJson(listaDetalles);
        return json;
    }

    // Obtiene los comensales activos con el monto gastado
    @SneakyThrows
    public static String getPedidosValidados() {
        ArrayList<String> data = new ArrayList<>();
        List<Map<String, String>> listaActivos = new ArrayList<>();

        String sql = "{call SP_GET_PEDIDOS_VALIDADOS(?)}";

        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        while (rs.next()){
            Map<String, String> activo = new HashMap<>();

            activo.put("id_comensal",rs.getString("ID_COMENSAL"));
            activo.put("id_mesa",rs.getString("id_mesa"));
            activo.put("numero_mesa",rs.getString("numero_mesa"));
            activo.put("id_pedido",rs.getString("id_pedido"));
            activo.put("nombre_receta",rs.getString("nombre_receta"));
            activo.put("cantidad",rs.getString("cantidad"));
            activo.put("id_origen",rs.getString("id_origen"));
            activo.put("nombre_descripcion",rs.getString("nombre_descripcion"));

            listaActivos.add(activo);
        }

        String json = new Gson().toJson(listaActivos);
        return json;
    }

    // Validar pedido (id_pedido)
    @SneakyThrows
    public static String validarPedido(String id_pedido) {
        ArrayList<String> data = new ArrayList<>();
        String result;
        data.add(id_pedido);

        String sql = "UPDATE SIGLOXXI_ADMIN.PEDIDO SET VALIDADO='1' WHERE ID_PEDIDO = ?";
        result = (db.rowQuery(sql, data)) ? "1": "-1";

        JSONObject resp = new JSONObject();
        resp.put("result", result);

        return resp.toString();
    }

    // ELiminar pedido (id_pedido)
    @SneakyThrows
    public static String eliminarPedido(String id_pedido) {
        ArrayList<String> data = new ArrayList<>();
        String result;
        data.add(id_pedido);

        String sql = "DELETE FROM SIGLOXXI_ADMIN.PEDIDO WHERE ID_PEDIDO=?";
        result = (db.rowQuery(sql, data)) ? "1": "-1";

        JSONObject resp = new JSONObject();
        resp.put("result", result);

        return resp.toString();
    }
    //  Terminar pedido Cocina (id_pedido)
    @SneakyThrows
    public static String terminarPedido(String id_pedido) {
        ArrayList<String> data = new ArrayList<>();
        String result;
        data.add(id_pedido);

        String sql = "UPDATE SIGLOXXI_ADMIN.PEDIDO SET ESTADO='1' WHERE ID_PEDIDO=?";
        result = (db.rowQuery(sql, data)) ? "1": "-1";

        JSONObject resp = new JSONObject();
        resp.put("result", result);

        return resp.toString();
    }

}
