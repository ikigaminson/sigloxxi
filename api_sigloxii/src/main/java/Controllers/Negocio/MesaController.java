package Controllers.Negocio;

import Models.Negocio.Mesa;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class MesaController {
    private static DB db = new DB();

	//Lista mesas
    @SneakyThrows
    public static String getList(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Mesa> list = new ArrayList<>();
        String sql = "{call sp_getMesas(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        Mesa mesa;
        String json = "";

        try{
            while (rs.next()){

                int id_mesa = Integer.parseInt(rs.getString("ID_MESA"));
                int numero  = Integer.parseInt(rs.getString("NUMERO"));
                int cantidad  = Integer.parseInt(rs.getString("CAPACIDAD"));
                String disponibilad = rs.getString("DISPONIBILIDAD");

                mesa = new Mesa(id_mesa, numero, cantidad, disponibilad);

                list.add(mesa);
            }

            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            System.out.println(e);
        }

        return json;
    }

    public static String cambiarDisponibilidad(String id){
        ArrayList<String> data = new ArrayList<>();
        String sql = "UPDATE SIGLOXXI_ADMIN.MESA " +
                "SET DISPONIBILIDAD='No Disponible'" +
                "WHERE ID_MESA= ? ";

        data.add(id);
        String result = db.rowQuery(sql, data) ? "1" : "-1";

        return result;
    }


    //Insertar mesa
    @SneakyThrows
    public static String insertMesa(String id_mesa,String numero,String capacidad,String disponibilidad,String eliminado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_MESA(?,?,?,?,?,?)}";

        data.add(id_mesa);
        data.add(numero);
        data.add(capacidad);
        data.add(disponibilidad);
        data.add(eliminado);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Update mesa
    @SneakyThrows
    public static String updateMesa(String id_mesa, String numero, String capacidad, String disponibilidad, String eliminado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_updateMesa(?,?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_mesa);
        data.add(numero);
        data.add(capacidad);
        data.add(disponibilidad);
        data.add(eliminado);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete mesa
    @SneakyThrows
    public static String deleteMesa(String id_mesa){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_MESA(?, ?)}";
        data.add(id_mesa);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }
}
