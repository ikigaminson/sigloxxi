package Controllers.Negocio;

import Models.Negocio.Insumo;
import Models.Negocio.Medida;
import Models.Negocio.Proveedor;
import Models.Personas.Persona;
import Models.Personas.TipoPersona;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class InsumoController {
    private static Insumo insumo;
    private static DB db = new DB();

    //Listar insumo
    @SneakyThrows
    public static String getInsumos(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Insumo> list = new ArrayList<>();
        String sql = "{call SP_GET_INSUMOS(?)}";
        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                int id_insumo = Integer.parseInt(rs.getString("id_insumo"));
                String nombre = rs.getString("nombre");
                int stock = Integer.parseInt(rs.getString("stock"));
                int id_proveedor = Integer.parseInt(rs.getString("proveedor_id_proveedor"));
                String p_nombre = rs.getString("nombre_proveedor");
                Long fono = rs.getLong("fono_proveedor");
                int id_medida = Integer.parseInt(rs.getString("medida_id_medida"));
                String unidad = rs.getString("unidad");

                Proveedor proveedor = new Proveedor(id_proveedor, p_nombre,fono);
                Medida medida = new Medida(id_medida, unidad);
                insumo = new Insumo(id_insumo, nombre, stock, proveedor, medida);

                list.add(insumo);
            }
            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            System.out.println(e);
        }
        return json;
    }

    //Insertar insumo
    @SneakyThrows
    public static String insertInsumo(String id_insumo, String nombre, String stock, String proveedor_id_proveedor, String medida_id_medida){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_INSUMO(?,?,?,?,?,?)}";

        data.add(id_insumo);
        data.add(nombre);
        data.add(stock);
        data.add(proveedor_id_proveedor);
        data.add(medida_id_medida);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Update insumo
    @SneakyThrows
    public static String updateInsumo(String id_insumo, String nombre, String stock, String proveedor_id_proveedor, String medida_id_medida){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_updateInsumo(?,?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_insumo);
        data.add(nombre);
        data.add(stock);
        data.add(proveedor_id_proveedor);
        data.add(medida_id_medida);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete Insumo
    @SneakyThrows
    public static String deleteInsumo(String id_insumo){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_INSUMO(?, ?)}";
        data.add(id_insumo);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }
}
