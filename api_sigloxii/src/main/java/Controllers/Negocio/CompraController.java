package Controllers.Negocio;

import Models.Negocio.DetalleOrdenCompra;
import Models.Negocio.Insumo;
import Models.Negocio.OrdenCompra;
import Models.Negocio.Proveedor;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.sql.Date;
import java.util.ArrayList;

/*
*   Controlador con maneja las ordenes de compras y sus detalles :D
* */
public class CompraController {
    private static OrdenCompra ordenCompra;
    private static DetalleOrdenCompra detalleOrdenCompra;
    private static DB db = new DB();

    // List Ordenes de Compra
	@SneakyThrows
    public static String getOrdenesCompras(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<OrdenCompra> list = new ArrayList<>();
        String sql = "{call sp_getOrdenCompras(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                int id_orden_compra = Integer.parseInt(rs.getString("id_orden_compra"));
                Date fechapedido = rs.getDate("fechapedido");
                Date fechallegada = rs.getDate("fechallegada");
                int eliminado = Integer.parseInt(rs.getString("eliminado"));

                // FIX: Handle eliminado column from db
                OrdenCompra ordenCompras = new OrdenCompra(id_orden_compra, fechapedido, fechallegada, eliminado);

                list.add(ordenCompras);
            }
        }catch (Exception e){
            System.out.println(e);
        }

        Gson gson = new Gson();
        json = gson.toJson(list);
        return json;
    }


    //Insert orden compra
    @SneakyThrows
    public static String insertOrdenCompra(String id_orden_compra,String fechapedido,String fechallegada,String eliminado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_ORDEN_COMPRA(?,?,?,?,?)}";

        data.add(id_orden_compra);
        data.add(fechapedido);
        data.add(fechallegada);
        data.add(eliminado);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Update orden compra
    @SneakyThrows
    public static String updateOrdenCompra(String id_orden_compra,String fechapedido,String fechallegada,String eliminado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Orden_Compra(?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_orden_compra);
        data.add(fechapedido);
        data.add(fechallegada);
        data.add(eliminado);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete orden compra
    @SneakyThrows
    public static String deleteOrdenCompra(String id_orden_compra){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_ORDEN_COMPRA(?, ?)}";
        data.add(id_orden_compra);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }
    // TODO: CREATE, UPDATE, DELETE

    //Insert orden compra insumo
    @SneakyThrows
    public static String insertOrdenCompraInsumo(String cantidad,String orden_compra_id_orden_compra,
                                                 String insumo_id_insumo){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_ORDEN_COMPRA_INSUMOS(?,?,?,?)}";

        data.add(cantidad);
        data.add(orden_compra_id_orden_compra);
        data.add(insumo_id_insumo);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    // Lista de Orden compra insumos
    @SneakyThrows
    public static String getListOrdenCompraInsumo(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<DetalleOrdenCompra> list = new ArrayList<>();
        String sql = "{call sp_get_Orden_CompraInsumos(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                int cantidad = Integer.parseInt(rs.getString("cantidad"));
                int orden_compra_id_orden_compra = Integer.parseInt(rs.getString("orden_compra_id_orden_compra"));
                int insumo_id_insumo = Integer.parseInt(rs.getString("insumo_id_insumo"));

                OrdenCompra ordenCompra = new OrdenCompra();
                Insumo insumo = new Insumo();

                ordenCompra.setId_ordenCompra(orden_compra_id_orden_compra);
                insumo.setId_insumo(insumo_id_insumo);
                DetalleOrdenCompra detalleOrdenCompras = new DetalleOrdenCompra(cantidad, ordenCompra, insumo);

                list.add(detalleOrdenCompras);
            }
        }catch (Exception e){
            System.out.println(e);
        }

        Gson gson = new Gson();
        json = gson.toJson(list);
        return json;
    }

    //Update orden compra insumo
    @SneakyThrows
    public static String updateOrdenCompraInsumo(String cantidad,String orden_compra_id_orden_compra,
                                                 String insumo_id_insumo){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Orden_CompraInsumos(?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(cantidad);
        data.add(orden_compra_id_orden_compra);
        data.add(insumo_id_insumo);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete orden compra insumo
    @SneakyThrows
    public static String deleteOrdenCompraInsumo(String orden_compra_id_orden_compra, String insumo_id_insumo){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_ORDEN_COMPRA_INSUMOS(?, ?, ?)}";
        data.add(orden_compra_id_orden_compra);
        data.add(insumo_id_insumo);
        String result = db.getProceduresWithInt(sql,data);

        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }
    //Calcula la ganancia
    @SneakyThrows
    public static String calcularGanancia(String fecha_inicio,String fecha_fin){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_calcular_ganancias(?,?,?)}";
        data.add(fecha_inicio);
        data.add(fecha_fin);
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String result = "";
        try {
            while (rs.next()) {
                result = rs.getString("totalConsumo");
            }
        }catch (Exception e){
            System.out.println(e);
        }
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

}
