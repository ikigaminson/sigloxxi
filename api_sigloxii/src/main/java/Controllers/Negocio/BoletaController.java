package Controllers.Negocio;

import Models.Negocio.Boleta;
import Models.Negocio.Comensal;
import Models.Negocio.Tipo_pago;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;
import java.sql.Date;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class BoletaController {
    private Boleta boleta;
    private static DB db = new DB();

    //Insert boleta
    @SneakyThrows
    public static String insertBoleta(String id_boleta, String fecha, String propina, String totalconsumo, String total,
                                      String comensal_id_comensal, String tipo_pago_id_tipo_pago){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_BOLETA(?,?,?,?,?,?,?,?)}";

        data.add(id_boleta);
        data.add(fecha);
        data.add(propina);
        data.add(totalconsumo);
        data.add(total);
        data.add(comensal_id_comensal);
        data.add(tipo_pago_id_tipo_pago);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Listar boletas
    @SneakyThrows
    public static String getListBoleta(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Boleta> list = new ArrayList<>();
        String sql = "{call sp_get_Boleta(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";

        try{
            while (rs.next()){
                int id_boleta = Integer.parseInt(rs.getString("id_boleta"));
                Date fecha = rs.getDate("fecha");
                int propina = Integer.parseInt(rs.getString("propina"));
                int totalconsumo = Integer.parseInt(rs.getString("totalconsumo"));
                int total  = Integer.parseInt(rs.getString("total"));
                int comensal_id_comensal  = Integer.parseInt(rs.getString("comensal_id_comensal"));
                int tipo_pago_id_tipo_pago  = Integer.parseInt(rs.getString("tipo_pago_id_tipo_pago"));

                Comensal comensal = new Comensal();
                Tipo_pago tipo_pago = new Tipo_pago();

                comensal.setId_comensal(comensal_id_comensal);
                tipo_pago.setId_tipo_pago(tipo_pago_id_tipo_pago);
                Boleta boletas = new Boleta(id_boleta, fecha, propina, totalconsumo,total, comensal, tipo_pago);

                list.add(boletas);
            }

            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            json = e.getMessage();
            //System.out.println(e);
        }

        return json;
    }

    //Update boleta
    @SneakyThrows
    public static String updateBoleta(String id_boleta, String fecha, String propina, String totalconsumo, String total,
                                      String comensal_id_comensal, String tipo_pago_id_tipo_pago){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Boleta(?,?,?,?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_boleta);
        data.add(fecha);
        data.add(propina);
        data.add(totalconsumo);
        data.add(total);
        data.add(comensal_id_comensal);
        data.add(tipo_pago_id_tipo_pago);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete boleta
    @SneakyThrows
    public static String deleteBoleta(String id_boleta){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_DELETE_BOLETA(?, ?)}";
        data.add(id_boleta);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

}
