package Controllers.Negocio;

import Models.Negocio.Insumo;
import Models.Negocio.Medida;
import Models.Negocio.Proveedor;
import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class ProveedorController {
    private static Proveedor proveedor;
    private static DB db = new DB();

    //Lista proveedores
    @SneakyThrows
    public static String getProveedores(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Proveedor> list = new ArrayList<>();
        String sql = "{call sp_getProveedores(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                int id_proveedor = Integer.parseInt(rs.getString("id_proveedor"));
                String nombre = rs.getString("nombre");
                long fono = Long.parseLong(rs.getString("fono").trim());
                String email = rs.getString("email");
                String direccion = rs.getString("direccion");

                proveedor = new Proveedor(id_proveedor, nombre, fono, email, direccion);

                list.add(proveedor);
            }
        }catch (Exception e){
            System.out.println(e);
        }

        Gson gson = new Gson();
        json = gson.toJson(list);
        return json;
    }

    //Insertar proveedor
    @SneakyThrows
    public static String insertProveedor(String id_proveedor, String nombre, String fono, String email, String direccion){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_PROVEEDOR(?,?,?,?,?,?)}";

        data.add(id_proveedor);
        data.add(nombre);
        data.add(fono);
        data.add(email);
        data.add(direccion);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Update proveedor
    @SneakyThrows
    public static String updateProveedor(String id_proveedor, String nombre, String fono, String email, String direccion){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Proveedor(?,?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_proveedor);
        data.add(nombre);
        data.add(fono);
        data.add(email);
        data.add(direccion);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete proveedor
    @SneakyThrows
    public static String deleteProveedor(String id_proveedor){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_PROVEEDOR(?, ?)}";
        data.add(id_proveedor);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }
}
