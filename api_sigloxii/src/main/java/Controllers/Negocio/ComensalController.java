package Controllers.Negocio;

import Models.Negocio.Comensal;
import Utils.DB;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class ComensalController {
    private Comensal comensal;
    private static DB db = new DB();

    @SneakyThrows
    public static String create(String cantidadPersonas, String id_mesa, String id_trabajador) {
        ArrayList<String> data = new ArrayList<>();
        String sql = "INSERT INTO SIGLOXXI_ADMIN.COMENSAL " +
                "(CANTIDADPERSONAS, MESA_ID_MESA, TRABAJADOR_ID_TRABAJADOR, ELIMINADO, FINALIZADO) " +
                "VALUES(?, ? , ?, 0, 0) ";

        data.add(cantidadPersonas);
        data.add(id_mesa);
        data.add(id_trabajador);

        if(db.rowQuery(sql, data)){

            return "1";
           /* sql = "select max(ID_COMENSAL) last_id from COMENSAL";
            CachedRowSet rs = db.query(sql);

            while (rs.next()){
                String last_id = rs.getString("last_id");
                return last_id;
            }*/
        }
        System.out.println("Error creating comensal");
        return "-1";
    }

    //Insertar comensal
    @SneakyThrows
    public static String insertComensal(String id_comensal,String cantidadpersonas,String mesa_id_mesa,String persona_id_persona,String trabajador_id_trabajador,String eliminado,String finalizado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_COMENSAL(?,?,?,?,?,?,?,?)}";

        data.add(id_comensal);
        data.add(cantidadpersonas);
        data.add(mesa_id_mesa);
        data.add(persona_id_persona);
        data.add(trabajador_id_trabajador);
        data.add(eliminado);
        data.add(finalizado);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Update comensal
    @SneakyThrows
    public static String updateComensal(String id_comensal, String cantidadpersonas, String mesa_id_mesa, String persona_id_persona, String trabajador_id_trabajador, String eliminado, String finalizado){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call sp_update_Comensal(?,?,?,?,?,?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_comensal);
        data.add(cantidadpersonas);
        data.add(mesa_id_mesa);
        data.add(persona_id_persona);
        data.add(trabajador_id_trabajador);
        data.add(eliminado);
        data.add(finalizado);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete comensal
    @SneakyThrows
    public static String deleteComensal(String id_comensal){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_COMENSAL(?, ?)}";
        data.add(id_comensal);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //get comensal from the mesa ID
    @SneakyThrows
    public static String getComesal(String id_mesa){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_GET_COMENSAL_ID(?, ?)}";

        data.add(id_mesa);

        CachedRowSet rs = db.getProceduresWithCursor(sql,data);
        String id = "-1";

        while (rs.next()){
            id = rs.getString("id");
            break;
        }

        JSONObject resp = new JSONObject();

        // Este está como result para poder reutilizar el método desde la app móvi
        resp.put("result", id);
        return resp.toString();
    }

    // Cerrar Comensal (id_comensal)
    @SneakyThrows
    public static String cerrarComensal(String id_comensal) {
        ArrayList<String> data = new ArrayList<>();
        String result;
        data.add(id_comensal);

        String sql = "UPDATE SIGLOXXI_ADMIN.COMENSAL SET FINALIZADO= '1' WHERE ID_COMENSAL=?";
        result = (db.rowQuery(sql, data)) ? "1": "-1";

        JSONObject resp = new JSONObject();
        resp.put("result", result);

        return resp.toString();
    }
}
