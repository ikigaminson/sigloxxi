package Controllers.Negocio;

import Models.Negocio.Medida;

import Utils.DB;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.json.JSONObject;

import javax.sql.rowset.CachedRowSet;
import java.util.ArrayList;

public class MedidaController {
    private static Medida medida;
    private static DB db = new DB();

    //Insertar medida
    @SneakyThrows
    public static String insertMedida(String id_medida, String unidad){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_INSERT_MEDIDA(?,?,?)}";

        data.add(id_medida);
        data.add(unidad);

        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }

    //Listar media
    @SneakyThrows
    public static String getListMedida(){
        ArrayList<String> data = new ArrayList<>();
        ArrayList<Medida> list = new ArrayList<>();
        String sql = "{call SP_GET_MEDIDA(?)}";

        // We passed the array without values 'cuz we just have the
        // cursor as parameter
        CachedRowSet rs = db.getProceduresWithCursor(sql, data);
        String json = "";
        try {
            while (rs.next()) {
                int id_medida = Integer.parseInt(rs.getString("id_medida"));
                String unidad = rs.getString("unidad");

                Medida medida = new Medida(id_medida, unidad);

                list.add(medida);
            }
            Gson gson = new Gson();
            json = gson.toJson(list);
        }catch (Exception e){
            System.out.println(e);
        }
        return json;
    }

    //Update medida
    @SneakyThrows
    public static String updateMedida(String id_medida, String unidad){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_UPDATE_MEDIDA(?,?,?)}";// IN,OUT(IN, IN, OUT)

        data.add(id_medida);
        data.add(unidad);

        String result = db.getProceduresWithInt(sql, data);

        JSONObject resp = new JSONObject();
        resp.put("result",result);

        return resp.toString();
    }

    //Delete medida
    @SneakyThrows
    public static String deleteMedida(String id_medida){
        ArrayList<String> data = new ArrayList<>();
        String sql = "{call SP_DELETE_MEDIDA(?, ?)}";
        data.add(id_medida);
        String result = db.getProceduresWithInt(sql,data);
        JSONObject resp = new JSONObject();
        resp.put("result", result);
        return resp.toString();
    }
}
