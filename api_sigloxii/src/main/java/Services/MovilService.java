package Services;

import Controllers.Negocio.ComensalController;
import Controllers.Negocio.MesaController;
import Controllers.Negocio.RecetaController;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static Controllers.Negocio.PedidoController.createPreOrden;
import static spark.Spark.get;
import static spark.Spark.post;

/*
 *   APP Móvil
 * */
public class MovilService {
    public void init() {
        // Obtiene lista de mesas
        get("/app/listarmesas", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "GET");

            return MesaController.getList();
        });

        //Obtiene lista de recetas
        get("/app/listarrecetas", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "GET");

            return RecetaController.getRecetas();
        });

        //Marca la mesa como no disponible.
        post("/app/cambiarDisponibilidadMesa", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            String id = obj.getString("id_mesa"); // This should be a String, otherwise will fail

            JSONObject resp = new JSONObject();
            resp.put("status", MesaController.cambiarDisponibilidad(id));

            return resp.toString();
        });

        // Crea un comensal
        post("/app/createComensal", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String cantidadPersonas = obj.getString("cantidadPersonas");
            String id_mesa = obj.getString("id_mesa");
            String id_trabajador = obj.getString("id_trabajador"); //Garzón

            JSONObject resp = new JSONObject();
            resp.put("status", ComensalController.create(cantidadPersonas, id_mesa, id_trabajador));

            return resp.toString();
        });

        // Crea la preorden
        post("/app/crearPreorden", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            /*
            *   {
            *       "id_comensal" : "2",
            *       "platos" : ["5","2","3"],
            *       "cantidad" : ["1","2","1"]
            *   }
            * */

            String id_comensal = obj.getString("id_comensal");
            JSONArray platos_array = obj.getJSONArray("platos");
            JSONArray cantidad_array = obj.getJSONArray("cantidad");

            // Verifica que tengan la misma cantidad de dato ambos array,
            // ya que ambos están relacionados
            if(platos_array.length() != cantidad_array.length()){
                // Verifica que los array vengan con datos
                if(platos_array.length() < 1 || cantidad_array.length() <1){
                    JSONObject resp = new JSONObject();
                    resp.put("status", "-1");
                    return  resp.toString();
                }
            }

            ArrayList<String> cantidad = new ArrayList<>();
            ArrayList<String> platos = new ArrayList<>();

            for (int i = 0; i < platos_array.length(); i++){
                platos.add(platos_array.getString(i));
                cantidad.add(cantidad_array.getString(i));
            }
            return createPreOrden(id_comensal, platos, cantidad);//RecetaController.getRecetas();
        });

        // Obtiene el id del comensal a base de la id_mesa
        post("/app/getComensal", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            String id = obj.getString("id_mesa"); // This should be a String, otherwise will fail

            JSONObject resp = new JSONObject();
            resp.put("result", MesaController.cambiarDisponibilidad(id));

            return resp.toString();
        });
    }
}
