package Services;

import Controllers.Negocio.*;
import Models.Negocio.Comensal;
import org.json.JSONObject;

import static spark.Spark.post;

/*
*   Todos las URLs relacionados a la gestión del proceso principal de negocio
* */
public class BusinessService {
    public void init(){

        // Obtiene lista de mesas
        post("/mesas/list", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return MesaController.getList();
        });

        //Obtiene lista de recetas
        post("/recetas/list", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return RecetaController.getRecetas();
        });

        //Obtiene lista de insumos
        post("/insumos/list", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return InsumoController.getInsumos();
        });

        //Obtiene lista de proveedores
        post("/proveedor/list", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return ProveedorController.getProveedores();
        });



        //Elimina mesa
        post("/mesa/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_mesa = obj.getString("id_mesa");

            return MesaController.deleteMesa(id_mesa);
        });

        //Elimina receta
        post("/receta/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_receta = obj.getString("id_receta");

            return RecetaController.deleteReceta(id_receta);
        });

        //Elimina insumo
        post("/insumo/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_insumo = obj.getString("id_insumo");

            return InsumoController.deleteInsumo(id_insumo);
        });

        //Elimina proveedor
        post("/proveedor/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_proveedor = obj.getString("id_proveedor");

            return ProveedorController.deleteProveedor(id_proveedor);
        });

        //Elimina comensal
        post("/comensal/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_comensal = obj.getString("id_comensal");

            return ComensalController.deleteComensal(id_comensal);
        });

        //Elimina orden compra
        post("/ordenCompra/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_orden_compra = obj.getString("id_orden_compra");

            return CompraController.deleteOrdenCompra(id_orden_compra);
        });

        //Insertar receta
        post("/receta/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_receta = obj.getString("id_receta");
            String nombre = obj.getString("nombre");
            String descripcion = obj.getString("descripcion");
            String precio = obj.getString("precio");
            String disponibilidad = obj.getString("disponibilidad");
            String url = obj.getString("url");
            String origen_id_origen = obj.getString("id_origen");
            String eliminado = obj.getString("eliminado");

            return RecetaController.insertReceta(id_receta,nombre,descripcion,precio,disponibilidad,url,origen_id_origen,eliminado);
        });

        //Insertar insumo
        post("/insumo/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_insumo = obj.getString("id_insumo");
            String nombre = obj.getString("nombre");
            String stock = obj.getString("stock");
            String proveedor_id_proveedor = obj.getString("id_proveedor");
            String medida_id_medida = obj.getString("id_medida");

            return InsumoController.insertInsumo(id_insumo,nombre,stock,proveedor_id_proveedor,medida_id_medida);
        });

        //Insertar comensal
        post("/comensal/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_comensal = obj.getString("id_comensal");
            String cantidadpersonas = obj.getString("cantidadpersonas");
            String mesa_id_mesa = obj.getString("id_mesa");
            String persona_id_persona = obj.getString("id_persona");
            String trabajador_id_trabajador = obj.getString("id_trabajador");
            String eliminado = obj.getString("eliminado");
            String finalizado = obj.getString("finalizado");

            return ComensalController.insertComensal(id_comensal,cantidadpersonas,mesa_id_mesa,persona_id_persona,trabajador_id_trabajador,eliminado,finalizado);
        });

        //Insertar mesa
        post("/mesa/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_mesa = obj.getString("id_mesa");
            String numero = obj.getString("numero");
            String capacidad = obj.getString("capacidad");
            String disponibilidad = obj.getString("disponibilidad");
            String eliminado = obj.getString("eliminado");

            return MesaController.insertMesa(id_mesa,numero,capacidad,disponibilidad,eliminado);
        });

        //Insertar proveedor
        post("/proveedor/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_proveedor = obj.getString("id_proveedor");
            String nombre = obj.getString("nombre");
            String fono = obj.getString("fono");
            String email = obj.getString("email");
            String direccion = obj.getString("direccion");

            return ProveedorController.insertProveedor(id_proveedor,nombre,fono,email,direccion);
        });

        //Update insumo
        post("/insumo/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_insumo = obj.getString("id_insumo");
            String nombre = obj.getString("nombre");
            String stock = obj.getString("stock");
            String proveedor_id_proveedor = obj.getString("id_proveedor");
            String medida_id_medida = obj.getString("id_medida");

            return InsumoController.updateInsumo(id_insumo, nombre, stock, proveedor_id_proveedor, medida_id_medida);
        });

        //Update mesa
        post("/mesa/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_mesa = obj.getString("id_mesa");
            String numero = obj.getString("numero");
            String capacidad = obj.getString("capacidad");
            String disponibilidad = obj.getString("disponibilidad");
            String eliminado = obj.getString("eliminado");

            return MesaController.updateMesa(id_mesa, numero, capacidad, disponibilidad, eliminado);
        });

        //Update proveedor
        post("/proveedor/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_proveedor = obj.getString("id_proveedor");
            String nombre = obj.getString("nombre");
            String fono = obj.getString("fono");
            String email = obj.getString("email");
            String direccion = obj.getString("direccion");

            return ProveedorController.updateProveedor(id_proveedor, nombre, fono, email, direccion);
        });

        //Update comensal
        post("/comensal/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_comensal = obj.getString("id_comensal");
            String cantidadpersonas = obj.getString("cantidadpersonas");
            String mesa_id_mesa = obj.getString("id_mesa");
            String persona_id_persona = obj.getString("id_persona");
            String trabajador_id_trabajador = obj.getString("id_trabajador");
            String eliminado = obj.getString("eliminado");
            String finalizado = obj.getString("finalizado");

            return ComensalController.updateComensal(id_comensal, cantidadpersonas, mesa_id_mesa, persona_id_persona, trabajador_id_trabajador, eliminado, finalizado);
        });

        //Insertar boleta
        post("/boleta/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_boleta = obj.getString("id_boleta");
            String fecha = obj.getString("fecha");
            String propina = obj.getString("propina");
            String totalconsumo = obj.getString("totalconsumo");
            String total = obj.getString("total");
            String comensal_id_comensal = obj.getString("id_comensal");
            String tipo_pago_id_tipo_pago = obj.getString("id_tipo_pago");

            return BoletaController.insertBoleta(id_boleta,fecha,propina,totalconsumo,total,comensal_id_comensal,
                    tipo_pago_id_tipo_pago);
        });

        //Listar boleta
        post("/boletas/list", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");

            return BoletaController.getListBoleta();
        });

        //Update boleta
        post("/boleta/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_boleta = obj.getString("id_boleta");
            String fecha = obj.getString("fecha");
            String propina = obj.getString("propina");
            String totalconsumo = obj.getString("totalconsumo");
            String total = obj.getString("total");
            String comensal_id_comensal = obj.getString("id_comensal");
            String tipo_pago_id_tipo_pago = obj.getString("id_tipo_pago");

            return BoletaController.updateBoleta(id_boleta,fecha,propina,totalconsumo,total,comensal_id_comensal, tipo_pago_id_tipo_pago);
        });

        //Delete boleta
        post("/boleta/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_boleta = obj.getString("id_boleta");

            return BoletaController.deleteBoleta(id_boleta);
        });

        //Insertar orden compra
        post("/ordenCompra/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_orden_compra = obj.getString("id_orden_compra");
            String fechapedido = obj.getString("fechapedido");
            String fechallegada = obj.getString("fechallegada");
            String eliminado = obj.getString("eliminado");

            return CompraController.insertOrdenCompra(id_orden_compra,fechapedido,fechallegada,eliminado);
        });

        //Update orden compra
        post("/ordenCompra/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_orden_compra = obj.getString("id_orden_compra");
            String fechapedido = obj.getString("fechapedido");
            String fechallegada = obj.getString("fechallegada");
            String eliminado = obj.getString("eliminado");

            return CompraController.updateOrdenCompra(id_orden_compra,fechapedido,fechallegada,eliminado);
        });

        //Insertar orden compra insumo
        post("/ordenCompraInsumo/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String cantidad = obj.getString("cantidad");
            String orden_compra_id_orden_compra = obj.getString("id_orden_compra");
            String insumo_id_insumo = obj.getString("id_insumo");

            return CompraController.insertOrdenCompraInsumo(cantidad,orden_compra_id_orden_compra,insumo_id_insumo);
        });

        //Listar orden compra
        post("/ordenCompra/list", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");

            return CompraController.getOrdenesCompras();
        });

        //Listar orden compra insumos
        post("/ordenCompraInsumo/list", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");

            return CompraController.getListOrdenCompraInsumo();
        });

        //Update orden compra insumo
        post("/ordenCompraInsumo/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String cantidad = obj.getString("cantidad");
            String orden_compra_id_orden_compra = obj.getString("id_orden_compra");
            String insumo_id_insumo = obj.getString("id_insumo");

            return CompraController.updateOrdenCompraInsumo(cantidad,orden_compra_id_orden_compra,insumo_id_insumo);
        });

        //Delete orden compra insumo
        post("/ordenCompraInsumo/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String orden_compra_id_orden_compra = obj.getString("id_orden_compra");
            String insumo_id_insumo = obj.getString("id_insumo");

            return CompraController.deleteOrdenCompraInsumo(orden_compra_id_orden_compra,insumo_id_insumo);
        });

        //Listar pedido
        post("/pedidos/list", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");

            return PedidoController.getListPedido();
        });

        //Insertar pedido
        post("/pedido/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_pedido = obj.getString("id_pedido");
            String estado = obj.getString("estado");
            String comensal_id_comensal = obj.getString("id_comensal");
            String validado = obj.getString("validado");

            return PedidoController.insertPedido(id_pedido,estado,comensal_id_comensal,validado);
        });

        //Update pedido
        post("/pedido/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_pedido = obj.getString("id_pedido");
            String estado = obj.getString("estado");
            String comensal_id_comensal = obj.getString("id_comensal");
            String validado = obj.getString("validado");

            return PedidoController.updatePedido(id_pedido,estado,comensal_id_comensal,validado);
        });

        //Delete pedido
        post("/pedido/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_pedido = obj.getString("id_pedido");

            return PedidoController.deletePedido(id_pedido);
        });

        //Update receta
        post("/receta/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_receta = obj.getString("id_receta");
            String nombre = obj.getString("nombre");
            String descripcion = obj.getString("descripcion");
            String precio = obj.getString("precio");
            String disponibilidad = obj.getString("disponibilidad");
            String url = obj.getString("url");
            String origen_id_origen = obj.getString("id_origen");
            String eliminado = obj.getString("eliminado");

            return RecetaController.updateReceta(id_receta,nombre,descripcion,precio,disponibilidad,url,origen_id_origen,
                    eliminado);
        });

        //Insertar receta insumo
        post("/recetaInsumo/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String cantidad = obj.getString("cantidad");
            String insumo_id_insumo = obj.getString("id_insumo");
            String receta_id_receta = obj.getString("id_receta");
            String medida_id_medida = obj.getString("id_medida");

            return RecetaController.insertRecetaInsumo(cantidad,insumo_id_insumo, receta_id_receta,medida_id_medida);
        });

        //Update receta insumo
        post("/recetaInsumo/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String cantidad = obj.getString("cantidad");
            String insumo_id_insumo = obj.getString("id_insumo");
            String receta_id_receta = obj.getString("id_receta");
            String medida_id_medida = obj.getString("id_medida");

            return RecetaController.updateRecetaInsumo(cantidad,insumo_id_insumo,receta_id_receta,medida_id_medida);
        });

        //Delete receta insumo
        post("/recetaInsumo/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String insumo_id_insumo = obj.getString("id_insumo");
            String receta_id_receta = obj.getString("id_receta");
            String medida_id_medida = obj.getString("id_medida");

            return RecetaController.deleteRecetaInsumo(insumo_id_insumo,receta_id_receta,medida_id_medida);
        });

        //Listar receta insumo
        post("/recetaInsumo/list", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");

            return RecetaController.getListRecetasInsumos();
        });

        //Insertar medida
        post("/medida/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_medida = obj.getString("id_medida");
            String unidad = obj.getString("unidad");

            return MedidaController.insertMedida(id_medida, unidad);
        });

        //Listar medida
        post("/medida/list", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");

            return MedidaController.getListMedida();
        });

        //Update medida
        post("/medida/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());

            String id_medida = obj.getString("id_medida");
            String unidad = obj.getString("unidad");

            return MedidaController.updateMedida(id_medida, unidad);
        });

        //Delete medida
        post("/medida/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_medida = obj.getString("id_medida");

            return MedidaController.deleteMedida(id_medida);
        });

        //Genera Ganancia
        post("/calcularGanancia/generar", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String fecha_inicio = obj.getString("fecha_inicio");
            String fecha_fin = obj.getString("fecha_fin");

            return CompraController.calcularGanancia(fecha_inicio,fecha_fin);
        });


        // Obtiene comensales activos
        post("/comensales/activos", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return PedidoController.getComensalesActivos();
        });

        // Obtiene detalles de orden de pedido por comensal
        post("/comensales/detallesOrden", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_comensal = obj.getString("id_comensal");

            return PedidoController.getComensalesDetalles(id_comensal);
        });

        // Obtiene detalle de ordenes validadas para cocina
        post("/cocina/ordenes", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return PedidoController.getPedidosValidados();
        });

        // Marca pedido como terminado y listo para servir
        post("/cocina/terminarPedido", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_pedido = obj.getString("id_pedido");

            return PedidoController.terminarPedido(id_pedido);
        });

        // Valida Pedido
        post("/pedido/validar", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_pedido = obj.getString("id_pedido");

            return PedidoController.validarPedido(id_pedido);
        });

        // Elimino Pedido
        post("/pedido/eliminar", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_pedido = obj.getString("id_pedido");

            return PedidoController.eliminarPedido(id_pedido);
        });

        // Finaliza comensal
        post("/comensal/finalizar", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_comensal = obj.getString("id_comensal");

            return ComensalController.cerrarComensal(id_comensal);
        });

    }
}
