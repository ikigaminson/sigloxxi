package Services;

import Controllers.Personas.ClienteController;
import Controllers.Personas.PersonaController;
import Controllers.Personas.TipoPersonaController;
import Controllers.Personas.TrabajadorController;
import org.json.JSONObject;

import static spark.Spark.options;
import static spark.Spark.post;

/*
*   Gestión masiva de personas
* */
public class UsuarioService {
    public void init(){

        //I'm not sure how this works 🙃
        options("/*", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");
            System.out.println(request.headers());

            return "post";
        });

        // Login from user to access at the system
        post("/usuario/login", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            //Parse body, in JSON, to json object
            JSONObject obj = new JSONObject(request.body());

            //Gets JSON values from the keys
            /*
            * {
            *   "user": "username",
            *   "pass": "password"
            * }
            * */
            String user = obj.getString("user");
            String pass = obj.getString("pass");

            // Convert the response in a JSON object
            JSONObject resp = new JSONObject();
            resp.put("id",TrabajadorController.login(user, pass));

            return resp.toString();
        });

        // Gets nombre, paterno, materno and tipoPersona about persona
        post("/usuario/get", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            //Gets data from URL
            //  http://localhost/usuario/get?id=1
            String id = request.queryParams("id");

            return TrabajadorController.getTrabajador(id);
        });

        // Gets tiposPersona from DB
        post("/usuario/getTipos", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return TipoPersonaController.getTipos();
        });

        // Insert a persona in the DB
        post("/usuario/insert", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            /*
            * {
            *   "rut" : "1111111",
            *   "dv" : "1",
            *   "nombre" : "Juanito",
            *   "paterno" : "Perez",
            *   "materno" : "Paredes",
            *   "mail" : "mail@mail.cl",
            *   "idTipo" : "1",
            * }
            * */

            String rut = obj.getString("rut");
            String dv = obj.getString("dv");
            String nombre = obj.getString("nombre");
            String paterno = obj.getString("paterno");
            String materno = obj.getString("materno");
            String mail = obj.getString("mail");
            String idTipo = obj.getString("idTipo");

            JSONObject resp = new JSONObject();
            int id = PersonaController.insertPersona(rut, dv, nombre, paterno, materno, mail, idTipo);
            resp.put("last_id", id);

            return resp.toString();
        });

        // Gets tiposPersona from DB
        post("/cliente/list", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            return ClienteController.getClients();
        });

        // update a persona in the DB
        post("/usuario/update", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            JSONObject obj = new JSONObject(request.body());
            /*
             * {
             *   "id_persona" : 1,
             *   "rut" : "1111111",
             *   "dv" : "1",
             *   "nombre" : "Juanito",
             *   "paterno" : "Perez",
             *   "materno" : "Paredes",
             *   "mail" : "mail@mail.cl",
             *   "idTipo" : "1",
             * }
             * */

            String id_persona = obj.getString("id_persona");
            String rut = obj.getString("rut");
            String dv = obj.getString("dv");
            String nombre = obj.getString("nombre");
            String paterno = obj.getString("paterno");
            String materno = obj.getString("materno");
            String mail = obj.getString("mail");
            String idTipo = obj.getString("idTipo");

            return PersonaController.updatePersona(id_persona, rut, dv, nombre, paterno, materno, mail, idTipo);
        });

        //Elimina persona
        post("/persona/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_persona = obj.getString("id_persona");

            return PersonaController.deletePersona(id_persona);
        });
    }
}
