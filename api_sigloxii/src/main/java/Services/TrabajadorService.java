package Services;

import Controllers.Personas.TrabajadorController;
import org.json.JSONObject;

import static spark.Spark.options;
import static spark.Spark.post;

/*
*   Gestión de trabajadores
* */
public class TrabajadorService {
    public void init(){
        //I'm not sure how this works 🙃
        options("/*", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");
            System.out.println(request.headers());

            return "post";
        });

        post("/trabajador/insert", (request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");

            //Parse body, in JSON, to json object
            JSONObject obj = new JSONObject(request.body());

            //Gets JSON values from the keys
            /*
             * {
             *   "user": "username",
             *   "pass": "password",
             *   "id_persona": 0
             * }
             * */
            //Rescata los valores enviados por el json, que se encuentran en el objeto json
            String id_trabajador = obj.getString("id_trabajador");
            String usuario = obj.getString("user");
            String clave = obj.getString("pass");
            String persona_id_persona = obj.getString("id_persona");
            String eliminado = obj.getString("eliminado");
            //Utiliza el metodo de trabajador controller para consultar en la base de datos
            return TrabajadorController.insertTrabajador(id_trabajador,usuario,clave,persona_id_persona,eliminado);

        });

		//Listar trabajador
        post("/trabajador/list", (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            response.header("Access-Control-Allow-Methods", "POST");

            return TrabajadorController.listTrabajadores();
        });

        //Elimina trabajador
        post("/trabajador/delete", (request, response)->{
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            //Parse body, in JSON to json object
            JSONObject obj = new JSONObject(request.body());
            //Gets JSON values from keys
            String id_trabajador = obj.getString("id_trabajador");

            return TrabajadorController.deleteTrabajador(id_trabajador);
        });
    }
}
