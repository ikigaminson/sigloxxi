package Models.Negocio;

import lombok.Data;

import java.sql.Date;

@Data
public class Boleta {
    private int id_boleta;
    private Date fecha;
    private int propina;
    private int totalconsumo;
    private int total;
    private Comensal comensal;
    private Tipo_pago tipo_pago;

    public Boleta(int id_boleta, Date fecha, int propina, int totalconsumo, int total, Comensal comensal, Tipo_pago tipo_pago){
        this.id_boleta = id_boleta;
        this.fecha = fecha;
        this.propina = propina;
        this.totalconsumo = totalconsumo;
        this.total = total;
        this.comensal = comensal;
        this.tipo_pago = tipo_pago;
    }

    public Boleta(){
    }
}
