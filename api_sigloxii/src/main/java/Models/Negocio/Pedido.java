package Models.Negocio;

import lombok.Data;

@Data
public class Pedido {
    private int id_pedido;
    private String estado;
    private Comensal comensal;
    private String validado;

    public Pedido(int id_pedido, String estado, Comensal comensal, String validado) {
        this.id_pedido = id_pedido;
        this.estado = estado;
        this.comensal = comensal;
        this.validado = validado;
    }

    public Pedido(){

    }
}
