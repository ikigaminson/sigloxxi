package Models.Negocio;

import lombok.Data;

@Data
public class Receta_insumo {
    private float cantidad;
    private Insumo insumo;
    private Receta receta;
    private Medida medida;

    public Receta_insumo(float cantidad, Insumo insumo, Receta receta, Medida medida){
        this.cantidad = cantidad;
        this.insumo = insumo;
        this.receta = receta;
        this.medida = medida;
    }

    public Receta_insumo(){

    }
}
