package Models.Negocio;

import lombok.Data;

@Data
public class Insumo {
    private int id_insumo;
    private String nombre;
    private int stock;
    private Proveedor proveedor;
    private Medida medida;

    public Insumo(int id_insumo, String nombre, int stock, Proveedor proveedor, Medida medida) {
        this.id_insumo = id_insumo;
        this.nombre = nombre;
        this.stock = stock;
        this.proveedor = proveedor;
        this.medida = medida;
    }

    public Insumo(){

    }
}
