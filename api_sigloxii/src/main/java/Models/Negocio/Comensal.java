package Models.Negocio;

import Models.Personas.Persona;
import Models.Personas.Trabajador;
import lombok.Data;

@Data
public class Comensal {
    private int id_comensal;
    private int cantidadPersonas;
    private Mesa mesa;
    private Persona cliente;
    private Trabajador garzon;
    private int eliminado;
    private int finalizado;

    public Comensal(int id_comensal, int cantidadPersonas, Mesa mesa, Persona persona, Trabajador garzon, int eliminado, int finalizado) {
        this.id_comensal = id_comensal;
        this.cantidadPersonas = cantidadPersonas;
        this.mesa = mesa;
        this.cliente = persona;
        this.garzon = garzon;
        this.eliminado = eliminado;
        this.finalizado = finalizado;
    }

    public Comensal(int id_comensal, int cantidadPersonas, Mesa mesa, Persona persona, Trabajador garzon) {
        this.id_comensal = id_comensal;
        this.cantidadPersonas = cantidadPersonas;
        this.mesa = mesa;
        this.cliente = persona;
        this.garzon = garzon;
    }

    public Comensal(){
    }
}
