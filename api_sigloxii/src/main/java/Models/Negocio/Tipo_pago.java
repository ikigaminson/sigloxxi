package Models.Negocio;

import lombok.Data;

@Data
public class Tipo_pago {
    private int id_tipo_pago;
    private String modopago;

    public Tipo_pago(int id_tipo_pago, String modopago){
        this.id_tipo_pago = id_tipo_pago;
        this.modopago = modopago;
    }

    public Tipo_pago(){
    }
}
