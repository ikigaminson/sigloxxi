package Models.Negocio;

import lombok.Data;

@Data
public class Medida {
    private int id_medida;
    private String unidad;

    public Medida(int id_medida, String unidad) {
        this.id_medida = id_medida;
        this.unidad = unidad;
    }

    public Medida(){

    }
}
