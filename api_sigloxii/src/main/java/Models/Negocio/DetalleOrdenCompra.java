package Models.Negocio;

import Models.Personas.Persona;
import Models.Personas.Trabajador;
import lombok.Data;

@Data
public class DetalleOrdenCompra {
    private int cantidad;
    private OrdenCompra ordenCompra;
    private Insumo insumo;

    public DetalleOrdenCompra(int cantidad, OrdenCompra ordenCompra, Insumo insumo) {
        this.cantidad = cantidad;
        this.ordenCompra = ordenCompra;
        this.insumo = insumo;
    }
}
