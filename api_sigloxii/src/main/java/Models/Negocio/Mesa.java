package Models.Negocio;

import lombok.Data;

@Data
public class Mesa {
    private int id_mesa;
    private int numero;
    private int capacidad;
    private String disponibilidad;

    public Mesa(int id_mesa, int numero, int cantidad, String disponibilad) {
        this.setId_mesa(id_mesa);
        this.setNumero(numero);
        this.setCapacidad(cantidad);
        this.setDisponibilidad(disponibilad);
    }
}

