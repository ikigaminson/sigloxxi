package Models.Negocio;

import lombok.Data;

import java.sql.Date;

@Data
public class OrdenCompra {
    private int id_ordenCompra;
    private Date fechaPedido;
    private Date fechaLlegada;
    private int eliminado;

    public OrdenCompra(int id_ordenCompra, Date fechaPedido, Date fechaLlegada, int eliminado) {
        this.id_ordenCompra = id_ordenCompra;
        this.fechaPedido = fechaPedido;
        this.fechaLlegada = fechaLlegada;
        this.eliminado = eliminado;
    }

    public OrdenCompra(int id_ordenCompra, Date fechaPedido, int eliminado) {
        this.id_ordenCompra = id_ordenCompra;
        this.fechaPedido = fechaPedido;
        this.eliminado = eliminado;
    }

    public OrdenCompra(){

    }

}
