package Models.Negocio;

import lombok.Data;

@Data
public class Proveedor {
    private int id_proveedor;
    private String nombre;
    private long fono; //Por limitación de número, se ocupa long para teléfonos
    private String email;
    private String direccion;

    public Proveedor(int id_proveedor, String nombre, long fono, String email, String direccion) {
        this.id_proveedor = id_proveedor;
        this.nombre = nombre;
        this.fono = fono;
        this.email = email;
        this.direccion = direccion;
    }

    public Proveedor(int id_proveedor, String nombre, long fono) {
        this.id_proveedor = id_proveedor;
        this.nombre = nombre;
        this.fono = fono;
    }

    public Proveedor(String nombre) {
        this.nombre = nombre;
    }
}
