package Models.Negocio;

import lombok.Data;

@Data
public class Origen {
    private int id_origen;
    private String descripcion;

    public Origen(int id_origen, String descripcion) {
        this.id_origen = id_origen;
        this.descripcion = descripcion;
    }

    public Origen() {
    }
}
