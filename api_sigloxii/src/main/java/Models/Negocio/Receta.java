package Models.Negocio;

import lombok.Data;

@Data
public class Receta {
    private int id_receta;
    private String nombre;
    private String descripcion;
    private int precio;
    private String disponibilidad;
    private String ImageUrl;
    private Origen origen;
    private int eleminado;

    public Receta(int id_receta, String nombre, String descripcion, int precio, String disponibilidad, String url, Origen origen, int eleminado) {
        this.id_receta = id_receta;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.disponibilidad = disponibilidad;
        this.ImageUrl = url;
        this.origen = origen;
        this.eleminado = eleminado;
    }

    public Receta(int id_receta, String nombre, String descripcion, int precio, String disponibilidad, Origen origen, int eleminado) {
        this.id_receta = id_receta;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.disponibilidad = disponibilidad;
        this.origen = origen;
        this.eleminado = eleminado;
    }

    public Receta(){

    }


}
