package Models.Personas;

import lombok.Data;

@Data
public class TipoPersona {
    private int id_tipo_persona;
    private String descripcion;

    public TipoPersona(int id_tipo_persona, String descripcion) {
        this.id_tipo_persona = id_tipo_persona;
        this.descripcion = descripcion;
    }

    public TipoPersona(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoPersona() {
    }
}
