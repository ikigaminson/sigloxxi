package Models.Personas;

import lombok.Data;

@Data
public class Trabajador {
    private int id_trabajador;
    private String usuario;
    private String clave;
    private Persona persona;
}
