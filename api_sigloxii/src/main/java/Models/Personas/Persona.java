package Models.Personas;

import lombok.Data;

@Data
public class Persona {
    private int id_persona;
    private int rut;
    private String dv;
    private String nombre;
    private String paterno;
    private String materno;
    private String email;
    private TipoPersona tipoPersona;

    public Persona() {
    }

    public Persona(int id_persona, int rut, String dv, String nombre, String paterno, String materno, String email, TipoPersona tipoPersona) {
        this.id_persona = id_persona;
        this.rut = rut;
        this.dv = dv;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.email = email;
        this.tipoPersona = tipoPersona;
    }
}
