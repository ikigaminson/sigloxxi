/*
 *  SCRIPT CREACION DB
 *  Ejecutar como SYSADM y despues crea una nueva conexion
 *  con usuario SIGLOXXI_ADMIN en ORCL
 *
 */

CREATE USER sigloxxi_admin IDENTIFIED BY pass123;

GRANT CONNECT TO sigloxxi_admin;

GRANT CONNECT, RESOURCE, DBA TO sigloxxi_admin;

/*GRANT ALL PRIVILEGES TO sigloxxi_admin;*/

GRANT UNLIMITED TABLESPACE TO sigloxxi_admin;