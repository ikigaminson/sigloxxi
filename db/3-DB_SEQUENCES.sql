/*
 *  SCRIPT PARA GENERAR AUTO INCREMENT
 *  Se crea una secuencia y luego se crea un trigger que se ejecuta
 *  cada vez que se hace una inserción
 *
 *  <tipo>_<accion>_<tabla>
 *  seq_ = sequence || trig_ = trigger || _ai_ = auto increment
 */

 -- MESA SEQ
CREATE SEQUENCE seq_ai_mesa START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_mesa
BEFORE INSERT ON mesa
FOR EACH ROW
BEGIN
  SELECT seq_ai_mesa.nextval
  INTO :new.id_mesa
  FROM dual;
END;
/

-- BOLETA SEQ
CREATE SEQUENCE seq_ai_boleta START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_boleta
BEFORE INSERT ON boleta
FOR EACH ROW
BEGIN
  SELECT seq_ai_boleta.nextval
  INTO :new.id_boleta
  FROM dual;
END;
/

-- TIPO_PERSONA SEQ
CREATE SEQUENCE seq_ai_tipo_persona START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_tipo_persona
BEFORE INSERT ON tipo_persona
FOR EACH ROW
BEGIN
  SELECT seq_ai_tipo_persona.nextval
  INTO :new.id_tipo_persona
  FROM dual;
END;
/

-- TRABAJADOR SEQ
CREATE SEQUENCE seq_ai_trabajador START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_trabajador
BEFORE INSERT ON trabajador
FOR EACH ROW
BEGIN
  SELECT seq_ai_trabajador.nextval
  INTO :new.id_trabajador
  FROM dual;
END;
/

-- COMENSAL SEQ
CREATE SEQUENCE seq_ai_comensal START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_comensal
BEFORE INSERT ON comensal
FOR EACH ROW
BEGIN
  SELECT seq_ai_comensal.nextval
  INTO :new.id_comensal
  FROM dual;
END;
/

-- TIPO_REGISTRO SEQ
CREATE SEQUENCE seq_ai_tipo_registro START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_tipo_registro
BEFORE INSERT ON tipo_registro
FOR EACH ROW
BEGIN
  SELECT seq_ai_tipo_registro.nextval
  INTO :new.id_tipo
  FROM dual;
END;
/

-- REGISTRO SEQ
CREATE SEQUENCE seq_ai_registro START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_registro
BEFORE INSERT ON registro
FOR EACH ROW
BEGIN
  SELECT seq_ai_registro.nextval
  INTO :new.id_registro
  FROM dual;
END;
/

-- TIPO_PAGO SEQ
CREATE SEQUENCE seq_ai_tipo_pago START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_tipo_pago
BEFORE INSERT ON tipo_pago
FOR EACH ROW
BEGIN
  SELECT seq_ai_tipo_pago.nextval
  INTO :new.id_tipo_pago
  FROM dual;
END;
/

-- MEDIDA SEQ
CREATE SEQUENCE seq_ai_medida START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_medida
BEFORE INSERT ON medida
FOR EACH ROW
BEGIN
  SELECT seq_ai_medida.nextval
  INTO :new.id_medida
  FROM dual;
END;
/

-- PROVEEDOR SEQ
CREATE SEQUENCE seq_ai_proveedor START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_proveedor
BEFORE INSERT ON proveedor
FOR EACH ROW
BEGIN
  SELECT seq_ai_proveedor.nextval
  INTO :new.id_proveedor
  FROM dual;
END;
/

-- ORDEN_COMPRA SEQ
CREATE SEQUENCE seq_ai_orden_compra START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_orden_compra
BEFORE INSERT ON orden_compra
FOR EACH ROW
BEGIN
  SELECT seq_ai_orden_compra.nextval
  INTO :new.id_orden_compra
  FROM dual;
END;
/

-- INSUMO SEQ
CREATE SEQUENCE seq_ai_insumo START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_insumo
BEFORE INSERT ON insumo
FOR EACH ROW
BEGIN
  SELECT seq_ai_insumo.nextval
  INTO :new.id_insumo
  FROM dual;
END;
/

-- RECETA SEQ
CREATE SEQUENCE seq_ai_receta START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_receta
BEFORE INSERT ON receta
FOR EACH ROW
BEGIN
  SELECT seq_ai_receta.nextval
  INTO :new.id_receta
  FROM dual;
END;
/

-- ORIGEN SEQ
CREATE SEQUENCE seq_ai_origen START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_origen
BEFORE INSERT ON origen
FOR EACH ROW
BEGIN
  SELECT seq_ai_origen.nextval
  INTO :new.id_origen
  FROM dual;
END;
/

-- PEDIDO SEQ
CREATE SEQUENCE seq_ai_pedido START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_pedido
BEFORE INSERT ON pedido
FOR EACH ROW
BEGIN
  SELECT seq_ai_pedido.nextval
  INTO :new.id_pedido
  FROM dual;
END;
/

-- PERSONA SEQ
CREATE SEQUENCE seq_ai_persona START WITH 1 NOCACHE;

CREATE OR REPLACE TRIGGER trig_ai_persona
BEFORE INSERT ON persona
FOR EACH ROW
BEGIN
  SELECT seq_ai_persona.nextval
  INTO :new.id_persona
  FROM dual;
END;
/