CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_CALCULAR_GANANCIAS
(
	p_fecha_inicio 	IN VARCHAR,
	p_fecha_fin		IN VARCHAR,
	p_rows			OUT SYS_REFCURSOR
)
IS
BEGIN
	OPEN p_rows FOR
	SELECT 
		SUM(b.TOTALCONSUMO) totalConsumo
		FROM BOLETA b 
		WHERE b.FECHA BETWEEN TO_DATE (p_fecha_inicio, 'dd/mm/yyyy')
		AND TO_DATE (p_fecha_fin, 'dd/mm/yyyy');
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETEPERSONA
(  p_id_persona in persona.id_persona%type,
   p_result out number

 )
IS
BEGIN
    UPDATE persona p
    SET
       p.eliminado = 1
    WHERE id_persona = p_id_persona AND eliminado = 0;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_BOLETA
(  b_id_boleta in boleta.id_boleta%type,
   p_result out number

 )
IS
BEGIN
    DELETE FROM boleta b

    WHERE id_boleta = b_id_boleta;

   IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_CLIENTE
(  p_id_persona in persona.id_persona%type,
   p_result out number

 )
IS
BEGIN
    UPDATE persona p
    SET
       p.eliminado = 1
    WHERE id_persona = p_id_persona AND tipo_persona_id_tipo_persona = 7;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_COMENSAL
( p_id_comensal in comensal.id_comensal%type,
  p_result out number
)
IS
BEGIN
    UPDATE comensal p       
    SET 
     p.eliminado = 1
    WHERE id_comensal = p_id_comensal;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_INSUMO
( p_id_insumo in insumo.id_insumo%type,
  p_result out number
)
IS
BEGIN
    DELETE FROM insumo
    WHERE id_insumo = p_id_insumo;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_DELETE_MEDIDA
( m_id_medida in medida.id_medida%type,
  m_result out number
)
IS
BEGIN
    DELETE FROM medida m
    WHERE id_medida = m_id_medida;

    IF SQL%rowcount = 1 THEN
        m_result := 1;
    ELSE
        m_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_MESA
( p_id_mesa in mesa.id_mesa%type,
  p_result out number
)
IS
BEGIN
    UPDATE mesa p 
    SET 
     p.eliminado = 1
    WHERE id_mesa = p_id_mesa;

     IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_ORDEN
(  b_cantidad in orden.cantidad%type,
   p_result out number

 )
IS
BEGIN
    DELETE FROM orden b

    WHERE cantidad = b_cantidad;

   IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_DELETE_ORDEN_COMPRA
( p_id_orden_compra in orden_compra.id_orden_compra%type,
  p_result out number
)
IS
BEGIN
    UPDATE orden_compra p 
    SET 
     p.eliminado = 1
    WHERE id_orden_compra = p_id_orden_compra;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    
    COMMIT;
    
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_ORDEN_COMPRA_INSUMOS
( p_orden_compra_id_orden_compra in orden_compra_insumos.orden_compra_id_orden_compra%type,
  p_insumo_id_insumo in orden_compra_insumos.insumo_id_insumo%type,
  p_result out number
)
IS
BEGIN
    DELETE FROM orden_compra_insumos p
    WHERE orden_compra_id_orden_compra = p_orden_compra_id_orden_compra and insumo_id_insumo = p_insumo_id_insumo;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_PEDIDO
( p_id_pedido in pedido.id_pedido%type,
  p_result out number
)
IS
BEGIN
    DELETE FROM pedido
    WHERE id_pedido = p_id_pedido;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_PROVEEDOR
( p_id_proveedor in proveedor.id_proveedor%type,
  p_result out number
)
IS
BEGIN
    DELETE FROM proveedor
    WHERE id_proveedor = p_id_proveedor;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_RECETA
( p_id_receta in receta.id_receta%type,
  p_result out number
)
IS
BEGIN
    UPDATE receta p 
    SET 
     p.eliminado = 1
    WHERE id_receta = p_id_receta;

     IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_RECETA_INSUMO 
( p_insumo_id_insumo in receta_insumo.insumo_id_insumo%type,
  p_receta_id_receta in receta_insumo.receta_id_receta%type,
  p_medida_id_medida in receta_insumo.medida_id_medida%type,
  p_result out number
)
IS
BEGIN
    DELETE FROM receta_insumo
    WHERE insumo_id_insumo = p_insumo_id_insumo and receta_id_receta = p_receta_id_receta and medida_id_medida = p_medida_id_medida;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_REGISTRO
( p_id_registro in registro.id_registro%type,
  p_result out number
)
IS
BEGIN
    DELETE FROM registro
    WHERE id_registro = p_id_registro;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_DELETE_TRABAJADOR
( p_id_trabajador in trabajador.id_trabajador%type,
  p_result out number
)
IS
BEGIN    
    UPDATE trabajador p 
    SET  
     p.eliminado = 1
    WHERE id_trabajador = p_id_trabajador AND eliminado = 0;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_deshabilitarMesa
( p_id_mesa in mesa.id_mesa%TYPE,
  p_out out NUMBER)
IS
BEGIN
    UPDATE mesa
    SET disponibilidad = 'No Disponible'
    WHERE id_mesa = p_id_mesa;

    IF SQL%rowcount = 1 THEN
        p_out := 1;
    ELSE
        p_out := -1;
    END IF;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_getClients
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        p.id_persona id_persona,
        p.nombre name,
        p.apelpat paterno,
        p.apelmat materno,
        p.rut rut,
        p.digitoverificador dv,
        p.email email,
        tp.id_tipo_persona id_tipoPersona,
        tp.descripcion tipoNombre
    FROM persona p
    JOIN tipo_persona tp ON p.tipo_persona_id_tipo_persona = tp.id_tipo_persona
    WHERE tp.id_tipo_persona = 7;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_GETIPOPERSONA
(p_idtipoPersona in NUMBER, p_rows out SYS_REFCURSOR)
IS
BEGIN
OPEN p_rows FOR
SELECT PERSONA.RUT, PERSONA.digitoverificador, PERSONA.NOMBRE
FROM TIPO_PERSONA 
INNER JOIN PERSONA ON TIPO_PERSONA.ID_TIPO_PERSONA=PERSONA.TIPO_PERSONA_ID_TIPO_PERSONA
WHERE TIPO_PERSONA.ID_TIPO_PERSONA=p_idtipoPersona;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_getMesas
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        m.id_mesa id_mesa,
        m.numero numero,
        m.capacidad capacidad,
        m.disponibilidad disponibilidad
    FROM mesa m
    ORDER BY m.numero;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_GETORDEN
(p_id_mesa in NUMBER,  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT RECETA.NOMBRE, ORDEN.CANTIDAD,
           RECETA.PRECIO AS "VALOR POR PLATO",
           (RECETA.PRECIO*ORDEN.CANTIDAD) AS TOTAL  
           FROM MESA
           INNER JOIN COMENSAL ON MESA.id_mesa=COMENSAL.MESA_ID_MESA
           INNER JOIN PEDIDO ON COMENSAL.ID_COMENSAL=PEDIDO.COMENSAL_ID_COMENSAL
           INNER JOIN ORDEN ON PEDIDO.ID_PEDIDO=ORDEN.PEDIDO_ID_PEDIDO
           INNER JOIN RECETA ON ORDEN.RECETA_ID_RECETA=RECETA.ID_RECETA
            WHERE MESA.id_mesa = p_id_mesa;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_getOrdenCompras
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       op.id_orden_compra id_orden_compra,
       op.fechapedido fechapedido,
       op.fechallegada fechallegada,
       op.eliminado eliminado
    FROM orden_compra op;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_getPedido
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       p.id_pedido id_pedido,
       p.estado estado,
       p.comensal_id_comensal comensal_id_comensal,
       p.validado validado
    FROM pedido p;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_getProveedores
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       p.id_proveedor id_proveedor,
       p.nombre nombre,
       p.fono fono,
       p.email email,
       p.direccion direccion
    FROM proveedor p;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_getRecetas
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        r.id_receta id_receta,
        r.nombre nombre,
        r.descripcion descripcion,
        r.precio precio,
        r.url url,
        r.disponibilidad disponibilidad,
        o.descripcion origen
    FROM receta r
    JOIN origen o ON (r.origen_id_origen = o.id_origen);
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_getTrabajadores
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        t.id_trabajador id_trabajador,
        p.id_persona id_persona,
        p.nombre name,
        p.apelpat paterno,
        p.apelmat materno,
        p.rut rut,
        p.digitoverificador dv,
        p.email email,
        tp.id_tipo_persona id_tipoPersona,
        tp.descripcion tipoNombre
    FROM trabajador t
    JOIN persona p ON (t.persona_id_persona = p.id_persona)
    JOIN tipo_persona tp ON p.tipo_persona_id_tipo_persona = tp.id_tipo_persona
    WHERE p.eliminado = 0 AND t.eliminado = 0 ;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_get_Boleta
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       p.id_boleta id_boleta,
       p.fecha fecha,
       p.propina propina,
       p.totalconsumo totalconsumo,
       p.total total,
       p.comensal_id_comensal comensal_id_comensal,
       p.tipo_pago_id_tipo_pago tipo_pago_id_tipo_pago

 FROM boleta p;
 COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_get_Comensal
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        c.id_comensal id_comensal,
        c.cantidadpersonas cantidadpersonas,
        c.mesa_id_mesa mesa_id_mesa,
        c.persona_id_persona personaidpersona,
        c.trabajador_id_trabajador trabajadoridtrabajador,
        c.eliminado eliminado,
        c.finalizado finalizado
    FROM comensal c
    WHERE c.eliminado = 0;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_get_Insumos
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       i.id_insumo id_insumo,
       i.nombre nombre,
       i.stock stock,
       i.proveedor_id_proveedor proveedor_id_proveedor,
       pr.nombre nombre_proveedor,
       pr.fono fono_proveedor,
       i.medida_id_medida medida_id_medida,
       m.unidad unidad
    FROM insumo i 
    JOIN Medida m ON (i.medida_id_medida = m.id_medida)
    JOIN Proveedor pr ON (i.proveedor_id_proveedor = pr.id_proveedor);
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_GET_MEDIDA
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       m.id_medida id_medida,
       m.unidad unidad
    FROM medida m;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_get_Orden_CompraInsumos
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       p.cantidad cantidad,
       p.orden_compra_id_orden_compra orden_compra_id_orden_compra,
       p.insumo_id_insumo insumo_id_insumo

 FROM orden_compra_insumos p;
 COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_get_Receta_Insumo
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        r.cantidad cantidad,
        r.insumo_id_insumo  insumo_id_insumo,
        r.receta_id_receta receta_id_receta,
        r.medida_id_medida medida_id_medida

    FROM receta_insumo r;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_GET_REGISTRO
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        r.id_registro id_registro,
        r.fecha  fecha,
        r.comensal_id_comensal comensal_id_comensal,
        r.tipo_registro_id_tipo tipo_registro_id_tipo

    FROM registro r;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_habilitarMesa
( p_id_mesa in mesa.id_mesa%TYPE,
  p_out out NUMBER)
IS
BEGIN
    UPDATE mesa
    SET disponibilidad = 'Disponible'
    WHERE id_mesa = p_id_mesa;

    IF SQL%rowcount = 1 THEN
        p_out := 1;
    ELSE
        p_out := -1;
    END IF;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_BOLETA
( b_id_boleta in boleta.id_boleta%type,
  b_fecha in boleta.fecha%type, b_propina in boleta.propina%type,
  b_totalconsumo in boleta.totalconsumo%type, b_total in boleta.total%type,
  b_comensal_id_comensal in boleta.comensal_id_comensal%type, b_tipo_pago_id_tipo_pago in boleta.tipo_pago_id_tipo_pago%type,
  b_result out number
)
IS

BEGIN
   INSERT INTO boleta 

   VALUES (b_id_boleta,b_fecha, b_propina, b_totalconsumo, b_total, b_comensal_id_comensal, b_tipo_pago_id_tipo_pago);

    IF SQL%rowcount = 1 THEN
        b_result := 1;
    ELSE
        b_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_COMENSAL
( p_id_comensal in comensal.id_comensal%type,
  p_cantidadpersonas in comensal.cantidadpersonas%type, p_mesa_id_mesa in comensal.mesa_id_mesa%type,
  p_persona_id_persona in comensal.persona_id_persona%type, p_trabajador_id_trabajador in comensal.trabajador_id_trabajador%type,
  p_eliminado in comensal.eliminado%type, p_finalizado in comensal.finalizado%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO comensal 
   VALUES (p_id_comensal, p_cantidadpersonas, p_mesa_id_mesa, p_persona_id_persona, p_trabajador_id_trabajador,p_eliminado,p_finalizado);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_INSUMO
( p_id_insumo in insumo.id_insumo%type,
  p_nombre in insumo.nombre%type, p_stock in insumo.stock%type,
  p_proveedor_id_proveedor in insumo.proveedor_id_proveedor%type, p_medida_id_medida in insumo.medida_id_medida%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO insumo 
   VALUES (p_id_insumo, p_nombre, p_stock, p_proveedor_id_proveedor, p_medida_id_medida);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_MEDIDA
( m_id_medida in medida.id_medida%type,
  m_unidad in medida.unidad%type,
  m_result out number
)
IS
BEGIN
   INSERT INTO medida m 
   VALUES (m_id_medida, m_unidad);
    IF SQL%rowcount = 1 THEN
        m_result := 1;
    ELSE
        m_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_MESA
( p_id_mesa in mesa.id_mesa%type,
  p_numero in mesa.numero%type, p_capacidad in mesa.capacidad%type,
  p_disponibilidad in mesa.disponibilidad%type, p_eliminado in mesa.eliminado%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO mesa p
   VALUES (p_id_mesa, p_numero, p_capacidad, p_disponibilidad, p_eliminado);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_ORDEN
( p_cantidad in orden.cantidad%type,
  p_pedido_id_pedido in orden.pedido_id_pedido%type, p_receta_id_receta in orden.receta_id_receta%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO orden p 
   VALUES (p_cantidad, p_pedido_id_pedido, p_receta_id_receta);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_ORDEN_COMPRA
( p_id_orden_compra in orden_compra.id_orden_compra%type,
  p_fechapedido in orden_compra.fechapedido%type, p_fechallegada in orden_compra.fechallegada%type,
  p_eliminado in orden_compra.eliminado%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO orden_compra p 
   VALUES (p_id_orden_compra, p_fechapedido, p_fechallegada, p_eliminado);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_ORDEN_COMPRA_INSUMOS
( p_cantidad in orden_compra_insumos.cantidad%type,
  p_orden_compra_id_orden_compra in orden_compra_insumos.orden_compra_id_orden_compra%type,
  p_insumo_id_insumo in orden_compra_insumos.insumo_id_insumo%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO orden_compra_insumos
   VALUES (p_cantidad, p_orden_compra_id_orden_compra, p_insumo_id_insumo);

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_PEDIDO
( p_id_pedido in pedido.id_pedido%type,
  p_estado in pedido.estado%type, p_comensal_id_comensal in pedido.comensal_id_comensal%type,
  p_validado in pedido.validado%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO pedido p
   VALUES (p_id_pedido, p_estado, p_comensal_id_comensal, p_validado);

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_PROVEEDOR
( p_id_proveedor in proveedor.id_proveedor%type,
  p_nombre in proveedor.nombre%type, p_fono in proveedor.fono%type,
  p_email in proveedor.email%type, p_direccion in proveedor.direccion%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO proveedor 
   VALUES (p_id_proveedor, p_nombre, p_fono, p_email, p_direccion);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_RECETA
( p_id_receta in receta.id_receta%type,
  p_nombre in receta.nombre%type, p_descripcion in receta.descripcion%type,
  p_precio in receta.precio%type, p_disponibilidad in receta.disponibilidad%type,
  p_url in receta.url%type, p_origen_id_origen in receta.origen_id_origen%type,
  p_eliminado in receta.eliminado%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO receta 
   VALUES (p_id_receta, p_nombre, p_descripcion, p_precio, p_disponibilidad, p_url, p_origen_id_origen, p_eliminado);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_RECETA_INSUMO
( p_cantidad in receta_insumo.cantidad%type,
  p_insumo_id_insumo in receta_insumo.insumo_id_insumo%type, p_receta_id_receta in receta_insumo.receta_id_receta%type,
  p_medida_id_medida in receta_insumo.medida_id_medida%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO receta_insumo p
   VALUES (p_cantidad, p_insumo_id_insumo, p_receta_id_receta, p_medida_id_medida);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_REGISTRO
( p_id_registro in registro.id_registro%type,
  p_fecha in registro.fecha%type, p_comensal_id_comensal in registro.comensal_id_comensal%type,
  p_tipo_registro_id_tipo in registro.tipo_registro_id_tipo%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO registro p 
   VALUES (p_id_registro, p_fecha, p_comensal_id_comensal, p_tipo_registro_id_tipo);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_INSERT_TRABAJADOR
( p_id_trabajador in trabajador.id_trabajador%type,
  p_usuario in trabajador.usuario%type,
  p_clave in trabajador.clave%type,
  p_persona_id_persona in trabajador.persona_id_persona%type,
  p_eliminado in trabajador.eliminado%type,
  p_result out number
)
IS
BEGIN
   INSERT INTO TRABAJADOR
   VALUES (p_id_trabajador, p_usuario, p_clave, p_persona_id_persona, p_eliminado);
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_login
( p_user     in  trabajador.usuario%type,
  p_password in  trabajador.usuario%type,
  p_row      out SYS_REFCURSOR
)
IS
BEGIN
    OPEN p_row FOR
    SELECT
        id_trabajador "id" 
    FROM trabajador
    WHERE usuario = p_user
    AND clave = p_password;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_updateInsumo
(   p_id_insumo in insumo.id_insumo%TYPE,
    p_nombre in insumo.nombre%TYPE,
    p_stock in insumo.stock%TYPE,
    p_proveedor_id_proveedor in insumo.proveedor_id_proveedor%TYPE,
    p_medida_id_medida in insumo.medida_id_medida%TYPE,
    p_result out number
 )
IS
BEGIN
    UPDATE insumo p
    SET
        p.id_insumo = p_id_insumo,
        p.nombre = p_nombre,
        p.stock = p_stock,
        p.proveedor_id_proveedor = p_proveedor_id_proveedor,
        p.medida_id_medida = p_medida_id_medida
    WHERE id_insumo = p_id_insumo;
       IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_updateMesa
(   p_id_mesa in mesa.id_mesa%TYPE,
    p_numero in mesa.numero%TYPE,
    p_capacidad in mesa.capacidad%TYPE,
    p_disponibilidad in mesa.disponibilidad%TYPE,
    p_eliminado in mesa.eliminado%type,
    p_result out number
  )
IS
BEGIN
    UPDATE mesa p
    SET
        p.id_mesa = p_id_mesa,
        p.numero = p_numero,
        p.capacidad = p_capacidad,
        p.disponibilidad = p_disponibilidad,
        p.eliminado = p_eliminado
    WHERE id_mesa = p_id_mesa;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_updatePassword
( p_id       in  trabajador.id_trabajador%type,
  p_password in  trabajador.clave%type,
  p_out      out NUMBER
)
IS
BEGIN
    UPDATE trabajador
    SET clave = p_password
    WHERE id_trabajador = p_id;
    IF SQL%rowcount = 1 THEN
        p_out := 1;
    ELSE
        p_out := -1;
    END IF;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_updatePersona
(   p_id_persona in persona.id_persona%TYPE,
    p_rut in persona.rut%TYPE,
    p_dv in persona.digitoverificador%TYPE,
    p_nombre in persona.nombre%TYPE,
    p_paterno in persona.apelpat%TYPE,
    p_materno in persona.apelmat%TYPE,
    p_email in persona.email%TYPE,
    p_tipo_persona_id_tipo_persona in persona.tipo_persona_id_tipo_persona%TYPE,
    p_result out NUMBER)
IS
BEGIN
    UPDATE persona p
    SET
        p.rut = p_rut,
        p.digitoverificador = p_dv,
        p.nombre = p_nombre,
        p.apelpat = p_paterno,
        p.apelmat = p_materno,
        p.email = p_email,
        p.tipo_persona_id_tipo_persona = p_tipo_persona_id_tipo_persona

    WHERE id_persona = p_id_persona;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Boleta
(   b_id_boleta in boleta.id_boleta%TYPE,
    b_fecha in boleta.fecha%TYPE,
    b_propina in boleta.propina%TYPE,
    b_totalconsumo in boleta.totalconsumo%TYPE,
    b_total in boleta.total%TYPE,
    b_comensal_id_comensal in boleta.comensal_id_comensal%TYPE,
    b_tipo_pago_id_tipo_pago in boleta.tipo_pago_id_tipo_pago%TYPE,
    p_result out NUMBER)
IS
BEGIN
    UPDATE boleta b
    SET
     b.id_boleta = b_id_boleta,
     b.fecha = b_fecha,
     b.propina = b_propina,
     b.totalconsumo = b_totalconsumo,
     b.total = b_total,
     b.comensal_id_comensal = b_comensal_id_comensal,
     b.tipo_pago_id_tipo_pago = b_tipo_pago_id_tipo_pago

    WHERE id_boleta = b_id_boleta;
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Comensal
(   p_id_comensal in comensal.id_comensal%TYPE,
    p_cantidadpersonas in comensal.cantidadpersonas%type,
    p_mesa_id_mesa in comensal.mesa_id_mesa%TYPE,
    p_persona_id_persona in comensal.persona_id_persona%TYPE,
    p_trabajador_id_trabajador in comensal.trabajador_id_trabajador%TYPE,
    p_eliminado in comensal.eliminado%type,
    p_finalizado in comensal.finalizado%type,
    p_result out number
 )
IS
BEGIN
    UPDATE comensal p
    SET
        p.id_comensal = p_id_comensal,
        p.cantidadpersonas = p_cantidadpersonas,
        p.mesa_id_mesa = p_mesa_id_mesa,
        p.persona_id_persona = p_persona_id_persona,
        p.trabajador_id_trabajador = p_trabajador_id_trabajador,
        p.eliminado = p_eliminado,
        p.finalizado = p_finalizado
    WHERE id_comensal = p_id_comensal;
       IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.SP_UPDATE_MEDIDA
(   m_id_medida in medida.id_medida%type,
    m_unidad in medida.unidad%type,
    m_result out number
 )
IS
BEGIN
    UPDATE medida m
    SET
        m.id_medida = m_id_medida,
        m.unidad = m_unidad
    WHERE id_medida = m_id_medida;
       IF SQL%rowcount = 1 THEN
        m_result := 1;
    ELSE
        m_result := -1;
    END IF;

    COMMIT;

END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Orden
(   o_cantidad in orden.cantidad%TYPE,
    o_pedido_id_pedido in orden.pedido_id_pedido%TYPE,
    o_receta_id_receta in orden.receta_id_receta%TYPE,
    p_result out NUMBER)
IS
BEGIN
    UPDATE orden o
    SET
     o.cantidad = o_cantidad,
     o.pedido_id_pedido = o_pedido_id_pedido,
     o.receta_id_receta = o_receta_id_receta

    WHERE pedido_id_pedido = o_pedido_id_pedido;
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Orden_Compra
(   p_id_orden_compra in orden_compra.id_orden_compra%TYPE,
    p_fechapedido in orden_compra.fechapedido%TYPE,
    p_fechallegada in orden_compra.fechallegada%TYPE,
    p_eliminado in orden_compra.eliminado%type,
    p_result out NUMBER)
IS
BEGIN
    UPDATE orden_compra p
    SET
        p.id_orden_compra = p_id_orden_compra,
        p.fechapedido = p_fechapedido,
        p.fechallegada = p_fechallegada,
        p.eliminado = p_eliminado

    WHERE id_orden_compra = p_id_orden_compra;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Orden_CompraInsumos
(   p_cantidad in orden_compra_insumos.cantidad%TYPE,
    p_orden_compra_id_orden_compra in orden_compra_insumos.orden_compra_id_orden_compra%TYPE,
    p_insumo_id_insumo in orden_compra_insumos.insumo_id_insumo%TYPE,
    p_result out NUMBER)
IS
BEGIN
    UPDATE orden_compra_insumos p
    SET
        p.cantidad = p_cantidad,
        p.orden_compra_id_orden_compra = p_orden_compra_id_orden_compra,
        p.insumo_id_insumo = p_insumo_id_insumo

    WHERE orden_compra_id_orden_compra = p_orden_compra_id_orden_compra and insumo_id_insumo = p_insumo_id_insumo;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Pedido
(   p_id_pedido in pedido.id_pedido%TYPE,
    p_estado in pedido.estado%TYPE,
    p_comensal_id_comensal in pedido.comensal_id_comensal%TYPE,
    p_validado in pedido.validado%TYPE,
    p_result out NUMBER)
IS
BEGIN
    UPDATE pedido p
    SET
        p.id_pedido = p_id_pedido,
        p.estado = p_estado,
        p.comensal_id_comensal = p_comensal_id_comensal,
        p.validado = p_validado

    WHERE id_pedido = p_id_pedido;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
  COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Proveedor
(   p_id_proveedor in proveedor.id_proveedor%TYPE,
    p_nombre in proveedor.nombre%TYPE,
    p_fono in proveedor.fono%TYPE,
    p_email in proveedor.email%TYPE,
    p_direccion in proveedor.direccion%TYPE,
    p_result out number
 )
IS

BEGIN
    UPDATE proveedor p
    SET
        p.id_proveedor = p_id_proveedor,
        p.nombre = p_nombre,
        p.fono = p_fono,
        p.email = p_email,
        p.direccion = p_direccion
    WHERE id_proveedor = p_id_proveedor;

    IF SQL%rowcount = 1 THEN

        p_result := 1;
    ELSE
        p_result := -1;

    END IF;

   COMMIT;

    END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Receta
(   p_id_receta in receta.id_receta%TYPE,
    p_nombre in receta.nombre%TYPE,
    p_descripcion in receta.descripcion%TYPE,
    p_precio in receta.precio%TYPE,
    p_disponibilidad in receta.disponibilidad%TYPE,
    p_url in receta.url%TYPE,
    p_origen_id_origen in receta.origen_id_origen%TYPE,
    p_eliminado in receta.eliminado%type,
    p_result out NUMBER)
IS
BEGIN
    UPDATE receta p
    SET
        p.id_receta = p_id_receta,
        p.nombre = p_nombre,
        p.descripcion = p_descripcion,
        p.precio = p_precio,
        p.disponibilidad = p_disponibilidad,
        p.url  = p_url ,
        p.origen_id_origen = p_origen_id_origen,
        p.eliminado = p_eliminado

    WHERE id_receta = p_id_receta;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Receta_Insumo
(   p_cantidad in receta_insumo.cantidad%TYPE,
    p_insumo_id_insumo in receta_insumo.insumo_id_insumo%TYPE,
    p_receta_id_receta in receta_insumo.receta_id_receta%TYPE,
    p_medida_id_medida in receta_insumo.medida_id_medida%type,
    p_result out NUMBER)
IS
BEGIN
    UPDATE receta_insumo p
    SET
        p.cantidad = p_cantidad,
        p.insumo_id_insumo = p_insumo_id_insumo,
        p.receta_id_receta = p_receta_id_receta,
        p.medida_id_medida = p_medida_id_medida

    WHERE insumo_id_insumo = p_insumo_id_insumo and receta_id_receta = p_receta_id_receta and medida_id_medida = p_medida_id_medida;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;

CREATE OR REPLACE PROCEDURE SIGLOXXI_ADMIN.sp_update_Registro
(   p_id_registro in registro.id_registro%TYPE,
    p_fecha in registro.fecha%TYPE,
    p_comensal_id_comensal in registro.comensal_id_comensal%TYPE,
    p_tipo_registro_id_tipo in registro.tipo_registro_id_tipo%type,
    p_result out NUMBER)
IS
BEGIN
    UPDATE registro p
    SET
        p.id_registro = p_id_registro,
        p.fecha = p_fecha,
        p.comensal_id_comensal = p_comensal_id_comensal,
        p.tipo_registro_id_tipo = p_tipo_registro_id_tipo

    WHERE id_registro = p_id_registro;

    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    COMMIT;
END;
