/********

** SCRIPT ***

***/ ORIGEN 

INSERT INTO origen(descripcion)
WITH types AS(
    SELECT 'Finanzas'  FROM dual UNION ALL
    SELECT 'Caja'  FROM dual 
)
SELECT * FROM types;

**** *********** /

INSERT INTO origen(descripcion)
WITH types AS(
    SELECT 'Adm'  FROM dual UNION ALL
    SELECT 'Garzon'  FROM dual
)

SELECT * FROM types;


******************* / 
---INSERT INTO MESAS 

INSERT INTO mesa(numero, capacidad, disponibilidad)
WITH mesa AS(
    SELECT 1, 2, 'Disponible' FROM dual UNION ALL
    SELECT 3, 4, 'Disponible' FROM dual UNION ALL
    SELECT 5, 6, 'Disponible' FROM dual UNION ALL
    SELECT 7, 8,  'No Disponible'FROM dual 
)
SELECT * FROM mesa;

************************** /
--INSERT ORDEN_COMPRA 

INSERT INTO orden_compra(fechaPedido, FechaLlegada)
WITH orders AS(
    SELECT CURRENT_DATE - 2, CURRENT_DATE  FROM dual UNION ALL
    SELECT CURRENT_DATE - 3, CURRENT_DATE - 4 FROM dual UNION ALL
    SELECT CURRENT_DATE, to_date('') FROM dual 
)
SELECT * FROM orders;


******************//
-INSERT PROVEEDOR


INSERT INTO proveedor(nombre, fono, email, direccion)
WITH provider AS(
    SELECT 'Carnes Bilbao', 223521400 , 'ventas@carnesbilbao.cl', 'Arauco #953 Santiago' FROM dual UNION ALL
    SELECT 'Bidfood', 56225994444 , 'contacto@bidfood.cl', 'Lo Echevers 550 Quilicura Santiago' FROM dual UNION ALL
    SELECT 'AGQ Labs', 56227544000, 'atencionalcliente@agqlabs.com', 'Industriales Huechuraba Santiago' FROM dual UNION ALL
    SELECT 'DistribuidoresAceitesOliva', 56222622581 , 'contacto@dayratiyah.cl', 'RíoClaro Region Maule' FROM dual 
)
SELECT * FROM provider;

********************/
-INSERT RECETAS 


 insert into receta values (1,'Charquican','Porotos verdes con papas y verduras',5000,'D', '', 1);
 insert into receta values (2,'Tallarin Saltado','Fideos con verduras y papas',3500,'D', '',1);
 insert into receta values (3,'Porotos Con tallarines','Espectaculares porotos con verduras',3500, 'D', '',1);
 insert into receta values (4,'Tomates Rellenos','Delicioso tomate con atun y verduras',4500, 'N', '',1);
 insert into receta values (5,'Tallarines con Camarones','Camarones salteados con veduras',5000, 'D', '',1);
 insert into receta values (6,'Lasaña de zapallo italiano','Lasaña directamente de rusia',4000, 'D', '',1);
 insert into receta values (7,'Pastel Choclo ','Pastel de choclo de la magia del sur',5500, 'N', '',1);



*********************/
-INSERT PERSONA;


insert into persona values (1, 14741294,'K','Jose','Escobar','Grandon','Jjescobargrandon@mail.cl',7);
insert into persona values (1, 14235849,'9','Diego','Fuentealba','Sanchez','Diegofsanchez@mail.cl',7);
insert into persona values (1, 25879435,'4','Ibis','Balboa','Grandon','Ibisgrandonbalboa@mail.cl',7);
insert into persona values (1, 13284971,'8','Miguel','Villanueva','Jorquera','MiguelVillajorquera@mail.cl',7);
   
***********************/

-INSERT TRABAJADOR;

insert into trabajador values (2,'ibisbalboa', 'pass1212', 4);
insert into trabajador values (8,'miguelvilla', 'pass123', 5);
insert into trabajador values (4,'joseesgrandon', 'pass123', 9);

********************************************************/
