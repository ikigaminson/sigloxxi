/*
 *   SCRIPT PARA POBLAR DB
 *   Ingresarlo en orden o todo se ira muy lejos
 */

--INSERT tipo_persona 
INSERT INTO tipo_persona(descripcion)
WITH types AS(
    SELECT 'Administrador'  FROM dual UNION ALL
    SELECT 'Bodeguero'  FROM dual UNION ALL
    SELECT 'Cajero'  FROM dual UNION ALL
    SELECT 'Cocinero'  FROM dual UNION ALL
    SELECT 'Barista'  FROM dual UNION ALL
    SELECT 'Garzón'  FROM dual  UNION ALL
    SELECT 'Cliente'  FROM dual
)
SELECT * FROM types;

--INSERT tipo_registro 
INSERT INTO tipo_registro(descripcion)
WITH types AS(
    SELECT 'Entrada comensal'  FROM dual UNION ALL
    SELECT 'Preorden'  FROM dual UNION ALL
    SELECT 'Confirmación de pedido'  FROM dual UNION ALL
    SELECT 'Pedido sale de cocina'  FROM dual UNION ALL
    SELECT 'Pedido entregado a mesa'  FROM dual UNION ALL
    SELECT 'Comensal paga'  FROM dual 
)
SELECT * FROM types;

--INSERT tipo_pago 
INSERT INTO tipo_pago(modoPago)
WITH types AS(
    SELECT 'Efectivo'  FROM dual UNION ALL
    SELECT 'PagoRUT'  FROM dual UNION ALL
    SELECT 'Cheque'  FROM dual UNION ALL
    SELECT 'Crédito'  FROM dual UNION ALL
    SELECT 'Débito'  FROM dual 
)
SELECT * FROM types;

--INSERT Origen
INSERT INTO origen(descripcion)
WITH types AS(
    SELECT 'Cocina'  FROM dual UNION ALL
    SELECT 'Bar'  FROM dual 
)
SELECT * FROM types;

--INSERT mesa 
INSERT INTO mesa(numero, capacidad, disponibilidad, eliminado)
WITH mesa AS(
    SELECT 1, 2, 'Disponible', 0 FROM dual UNION ALL
    SELECT 2, 2, 'Disponible', 0 FROM dual UNION ALL
    SELECT 3, 4, 'Disponible', 0 FROM dual UNION ALL
    SELECT 4, 6,  'No Disponible', 0 FROM dual 
)
SELECT * FROM mesa;

--INSERT orden_compra 
INSERT INTO orden_compra(fechaPedido, FechaLlegada, eliminado)
WITH orders AS(
    SELECT CURRENT_DATE - 5, CURRENT_DATE, 0  FROM dual UNION ALL
    SELECT CURRENT_DATE - 15, CURRENT_DATE - 8, 0 FROM dual UNION ALL
    SELECT CURRENT_DATE, to_date(''), 0 FROM dual 
)
SELECT * FROM orders;

--INSERT medida 
INSERT INTO medida(unidad)
WITH measure AS(
    SELECT 'ml' FROM dual UNION ALL
    SELECT 'lt' FROM dual UNION ALL
    SELECT 'cc' FROM dual UNION ALL
    SELECT 'mg' FROM dual UNION ALL
    SELECT 'kg' FROM dual 
)
SELECT * FROM measure;

--INSERT proveedor 
INSERT INTO proveedor(nombre, fono, email, direccion)
WITH provider AS(
    SELECT 'San Pancrasio', 25553662, 'pancrasio@mail.cl', 'Av. libertad 512' FROM dual UNION ALL
    SELECT 'Arizona', 953966521, 'arizona@mail.cl', 'El olivar 3456' FROM dual UNION ALL
    SELECT 'CCU', 956327458, 'ccu@mail.cl', 'Camino internacional 1245' FROM dual UNION ALL
    SELECT 'Fidal', 256981302, 'fidal@hotmail.com', 'Italia 876' FROM dual 
)
SELECT * FROM provider;


--INSERT receta 
INSERT INTO receta(nombre, descripcion, precio, disponibilidad, url, ORIGEN_id_origen, eliminado)
WITH recipe AS(
    SELECT 'Arroz con huevo frito','Delicioso arroz con huevo frito de gallina feliz', 3500, 'D', '', 1, 0 FROM dual UNION ALL
    SELECT 'Cazuela de vacuno','Cazuela de vaca soltera', 4000, 'D', '', 1, 0 FROM dual UNION ALL
    SELECT 'Papas fritas', 'Deliciosas papas fritas traidas desde vietnam', 2500, 'N', '', 1, 0 FROM dual 
)
SELECT * FROM recipe;
  
------------------------------------------>

--INSERT persona 
INSERT INTO persona(rut, digitoVerificador, nombre, apelPat, apelMat, email, TIPO_PERSONA_id_tipo_persona, eliminado)
WITH person AS(
    SELECT 12695874, 'K', 'Manuel', 'Miranda', 'Ramirez', 'mmirandar@mail.cl', 1, 0 FROM dual UNION ALL
    SELECT 13654720, '8', 'Claudia', 'Hernandez', 'Perez', 'chernandezp@mail.cl', 3, 0 FROM dual UNION ALL
    SELECT 16458934, '3', 'Luciano', 'Riveras', 'Jimenez', 'lriverasj@mail.cl', 4, 0 FROM dual UNION ALL
    SELECT 8569721, '5', 'Roberto', 'Pereira', 'Gutierrez', 'rpereirag@mail.cl', 6, 0 FROM dual
)
SELECT * FROM person;

--INSERT trabajador (pass123)
INSERT INTO trabajador(usuario, clave, PERSONA_id_persona, eliminado)
WITH worker AS(
    SELECT 'mmirandar', '9b8769a4a742959a2d0298c36fb70623f2dfacda8436237df08d8dfd5b37374c', 1, 0 FROM dual UNION ALL
    SELECT 'chernandezp', '9b8769a4a742959a2d0298c36fb70623f2dfacda8436237df08d8dfd5b37374c', 2, 0 FROM dual UNION ALL
    SELECT 'lriverasj', '9b8769a4a742959a2d0298c36fb70623f2dfacda8436237df08d8dfd5b37374c', 3, 0 FROM dual
)
SELECT * FROM worker;