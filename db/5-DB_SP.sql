-- Login
CREATE OR REPLACE PROCEDURE sp_login
( p_user     in  trabajador.usuario%type,
  p_password in  trabajador.usuario%type,
  p_row      out SYS_REFCURSOR
)
IS
BEGIN
    OPEN p_row FOR
    SELECT
        persona_id_persona "id" 
    FROM trabajador
    WHERE usuario = p_user
    AND clave = p_password;
END;

-- Actualiza la contrasenia
CREATE OR REPLACE PROCEDURE sp_updatePassword
( p_id       in  trabajador.id_trabajador%type,
  p_password in  trabajador.clave%type,
  p_out      out NUMBER
)
IS
BEGIN
    UPDATE trabajador
    SET clave = p_password
    WHERE id_trabajador = p_id;
    IF SQL%rowcount = 1 THEN
        p_out := 1;
    ELSE
        p_out := -1;
    END IF;
END;

-- Obtiene todos los trabajadores
create or replace PROCEDURE sp_getTrabajadores
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        t.id_trabajador id_trabajador,
        p.nombre name,
        p.apelpat paterno,
        p.apelmat materno,
        p.rut rut,
        p.digitoverificador dv,
        p.email email,
        tp.id_tipo_persona id_tipoPersona,
        tp.descripcion tipoNombre
    FROM trabajador t
    JOIN persona p ON (t.persona_id_persona = p.id_persona)
    JOIN tipo_persona tp ON p.tipo_persona_id_tipo_persona = tp.id_tipo_persona;
END;

-- Obtiene todas las recetas del sistema 
CREATE OR REPLACE PROCEDURE sp_getRecetas
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        r.id_receta id_receta,
        r.nombre nombre,
        r.descripcion descripcion,
        r.precio precio,
        r.url url,
        r.disponibilidad disponibilidad,
        o.descripcion origen
    FROM receta r
    JOIN origen o ON (r.origen_id_origen = o.id_origen);
END;

-- Obtiene a todos los clientes
CREATE OR REPLACE PROCEDURE sp_getClients
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        p.id_persona id_persona,
        p.nombre name,
        p.apelpat paterno,
        p.apelmat materno,
        p.rut rut,
        p.digitoverificador dv,
        p.email email,
        tp.id_tipo_persona id_tipoPersona,
        tp.descripcion tipoNombre
    FROM persona p
    JOIN tipo_persona tp ON p.tipo_persona_id_tipo_persona = tp.id_tipo_persona
    WHERE tp.id_tipo_persona = 7;
END;

-- Obtiene las mesas
create or replace PROCEDURE sp_getMesas
(  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
        m.id_mesa id_mesa,
        m.numero numero,
        m.capacidad capacidad,
        m.disponibilidad disponibilidad
    FROM mesa m;
END;

-- Obtiene los insumos en sistema
CREATE OR REPLACE PROCEDURE sp_getInsumos
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       i.id_insumo id_insumo,
       i.nombre nombre,
       i.stock stock,
       p.id_proveedor id_proveedor,
       p.nombre nombre_proveedor,
       m.id_medida id_medida,
       m.unidad unidad
    FROM insumo i
    JOIN medida m ON (i.medida_id_medida = m.id_medida)
    JOIN proveedor p ON (i.proveedor_id_proveedor = p.id_proveedor);
END;

-- Obtiene proveedores
CREATE OR REPLACE PROCEDURE sp_getProveedores
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       p.id_proveedor id_proveedor,
       p.nombre nombre,
       p.fono fono,
       p.email email,
       p.direccion direccion
    FROM proveedor p;
END;

-- Muestra todas las ordenes de compra
CREATE OR REPLACE PROCEDURE sp_getOrdenCompras
( p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       op.id_orden_compra id_orden,
       op.fechapedido fechaPedido,
       op.fechallegada fechaLlegada
    FROM orden_compra op
    ORDER BY fechaPedido DESC;
END; 

-- Obtiene todas los pedidos de un comensal
CREATE OR REPLACE PROCEDURE sp_getPedido
( p_id_comensal in pedido.comensal_id_comensal%TYPE,
  p_rows out SYS_REFCURSOR)
IS
BEGIN
    OPEN p_rows FOR
    SELECT
       p.id_pedido id_pedido,
       p.estado estado,
       p.validado validado
    FROM pedido p
    WHERE p.comensal_id_comensal = p_id_comensal;
END; 


-- Actualiza datos de una persona
create or replace PROCEDURE sp_updatePersona
(   p_idPersona in persona.id_persona%TYPE,
    p_rut in persona.rut%TYPE,
    p_dv in persona.digitoverificador%TYPE,
    p_nombre in persona.nombre%TYPE,
    p_paterno in persona.apelpat%TYPE,
    p_materno in persona.apelmat%TYPE,
    p_email in persona.email%TYPE,
    p_tipoPersonaID in persona.tipo_persona_id_tipo_persona%TYPE,
    p_result out NUMBER)
IS
BEGIN
    UPDATE persona p
    SET
        p.rut = p_rut,
        p.digitoverificador = p_dv,
        p.nombre = p_nombre,
        p.apelpat = p_paterno,
        p.apelmat = p_materno,
        p.email = p_email,
        p.tipo_persona_id_tipo_persona = p_tipopersonaid
    WHERE id_persona = p_idpersona;
    
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    
    COMMIT;
        
END;

-- Marca persona como eliminado
create or replace PROCEDURE sp_deletePersona
(   p_idPersona in persona.id_persona%TYPE,
    p_result out NUMBER)
IS
BEGIN
    UPDATE persona p
    SET
        p.eliminado = 1
    WHERE id_persona = p_idpersona;
    
    IF SQL%rowcount = 1 THEN
        p_result := 1;
    ELSE
        p_result := -1;
    END IF;
    
    COMMIT;
        
END;